function getTicketDetails(id){
    var url = Routing.generate('app_user_ticket_get', { id: id, subdomain: 'devtig'});
    $.ajax({
        type: "GET",
        url: url,
        success: function(response) {
            if(response){
                return response;
            }
        }
    });
}