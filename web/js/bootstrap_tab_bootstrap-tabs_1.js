$(document).ready(function() {
// Bootstrap tab menu behavior
    var hash = window.location.hash;

    console.log('test');

    if (hash || location.hash.indexOf('?') >= 1) {
        if (location.hash.indexOf('?') >= 1) {
            hash = location.hash.split("?")[0];
            if (getUrlVars()['subject']) {
                $('input#ticket_subject').val(decodeURI(getUrlVars()["subject"].charAt(0).toUpperCase() + getUrlVars()["subject"].slice(1)));
            } else if (getUrlVars()['id']) {
                // TODO get ticket with ID
            }
        }
        $('a.tab-btn[href="' + hash + '"]').tab('show');
        window.location.hash = hash;
    } else {
        $('#index').tab('show');
    }

    $('a[href="#ticket"]').click(function (e) {
        getTicket($(this).attr('id'));
    });

    // navigate to a tab when the history changes
    window.addEventListener("popstate", function (e) {
        if (location.hash.indexOf('?') >= 1) {
            hash = location.hash.split("?")[0];
            if (getUrlVars()['subject']) {
                ticket = false;
                $('input#ticket_subject').val(decodeURI(getUrlVars()["subject"].charAt(0).toUpperCase() + getUrlVars()["subject"].slice(1)));
            }
        }else{
            hash = location.hash;
        }

        if (!location.hash) {
            var activeTab = $('[href="#index"]');
        }else
        {
            var activeTab = $('[href=' + hash + ']');
        }

        if (activeTab.length) {
            if (!activeTab.hasClass('active')) {
                if (activeTab.attr('href') == '#index') {
                    history.replaceState(null, null, window.location.href.slice(0, -window.location.hash.length));
                    showTab(activeTab);
                } else {
                    history.replaceState(null, null, $(this).attr('href'));
                    showTab(activeTab);
                }
            }
        } else {
            $('#index').tab('show');
        }
    });

    function showTab(activeTab) {
        activeTab.tab('show');
        $('a.tab-btn.active').removeClass('active');
        activeTab.addClass('active');
    }

    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
});