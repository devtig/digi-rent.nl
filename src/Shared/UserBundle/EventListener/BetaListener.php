<?php

namespace Shared\UserBundle\EventListener;

use App\CoreBundle\Event\DREvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Shared\EmailBundle\Mailer\MailerInterface;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Shared\UserBundle\Entity\ConfirmationState;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BetaListener implements EventSubscriberInterface
{
    private $userManager;
    private $mailer;
    private $tokenGenerator;
    private $router;
    private $session;

    /**
     * EmailConfirmationListener constructor.
     *
     * @param UserManagerInterface    $userManager
     * @param MailerInterface         $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UrlGeneratorInterface   $router
     * @param SessionInterface        $session
     */
    public function __construct(UserManagerInterface $userManager, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator, UrlGeneratorInterface $router, SessionInterface $session)
    {
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->router = $router;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return array(
            DREvents::BETA_REGISTRATION_SUCCESS => 'onBetaRegistrationSuccess',
            DREvents::BETA_REGISTRATION_CONFIRMED => 'onBetaConfirmationSuccess'
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onBetaRegistrationSuccess(FormEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getForm()->getData();

        $user->setEnabled(false);
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationState(ConfirmationState::CONF_STATE_BETA);
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->userManager->updateUser($user);

        $this->mailer->sendBetaRegisteredEmail($user);
        $this->mailer->sendAdminUpdateEmail($user, __FUNCTION__);

        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());
    }

    /**
     * @param UserEvent $event
     */
    public function onBetaConfirmationSuccess(UserEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getUser();

        $this->mailer->sendBetaConfirmationEmail($user);
        $this->mailer->sendAdminUpdateEmail($user, __FUNCTION__);

        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());
    }
}
