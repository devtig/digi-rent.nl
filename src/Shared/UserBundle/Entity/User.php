<?php

namespace Shared\UserBundle\Entity;

use App\CompanyBundle\Entity\Company;
use App\SharedBundle\Entity\Phone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Shared\UserBundle\Entity\Role;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\CoreBundle\Entity\Traits as Traits;
use Shared\UserBundle\Entity\Traits as UserTraits;
use Simpleweb\SaaSBundle\Entity\Traits as SaasBundleTraits;

/**
 * User
 * @ExclusionPolicy("All")
 * @ORM\Table(name="user")
 * @ORM\Entity
 * @Vich\Uploadable
 *
 */
class User extends BaseUser
{
    use Traits\TimeStampable,
        UserTraits\Referable;

    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    public function __construct()
    {
        $this->enabled = false;
        $this->phonenumbers = new ArrayCollection();
        $this->dayOfBirth = new \DateTime('now');
        parent::__construct();
    }

    public function __toString()
    {
        $firstname = ucfirst($this->firstname);
        $middleName = strtolower($this->middleName);
        $middleNames = explode(" ", $middleName);
        $lastname = ucfirst($this->lastname);

        return $firstname.' '.(!empty($middleNames[0]) ? $middleNames[0][0] : '' ).(!empty($middleNames[1]) ? '.'.$middleNames[1][0].'.' : '' ).' '.$lastname;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\CompanyBundle\Entity\Company", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true)
     */
    private $company;

    /**
    * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\SharedBundle\Entity\Phone", cascade={"persist"})
     * @ORM\JoinTable(name="users_phonenumbers",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="phonenumber_id", referencedColumnName="id", unique=true)}
     *  )
     */
    private $phonenumbers;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $imageName;

    /**
     * @var string
     * @Expose()
     * @Assert\NotBlank(message="Please enter your firstname.", groups={"Registration", "Profile"})
     * @ORM\Column(name="firstname", type="string", length=255)
     *
     */
    private $firstname;

    /**
     * @var string
     * @Expose()
     * @ORM\Column(name="middle_name", type="string", length=255, nullable=true)
     *
     */
    private $middleName;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please enter your lastname.", groups={"Registration", "Profile"})
     * @ORM\Column(name="lastname", type="string", length=255)
     *
     */
    private $lastname;

    /**
     * @var \DateTime $dayOfBirth
     *
     * @ORM\Column(name="day_of_birth", type="date", nullable=false)
     */
    private $dayOfBirth;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="previous_login", type="datetime", nullable=true)
     */
    private $previousLogin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="newsletter", type="boolean", nullable=true)
     */
    private $newsletter;

    /**
     * @var integer
     *
     * @ORM\Column(name="confirmationState", type="smallint", nullable=true)
     */
    protected $confirmationState;

    /**
     * Set email
     *
     * @param String $email
     * @return User
     */
    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }

    /**
     * Set company
     *
     * @param Company $company
     * @return User
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get phonenumbers
     *
     * @return ArrayCollection
     */
    public function getPhonenumbers()
    {
        return $this->phonenumbers;
    }

    /**
     * Add phonenumber
     *
     * @param Phone $phonenumber
     * @return User
     */
    public function addPhone(Phone $phonenumber)
    {
        if (!$this->phonenumbers->contains($phonenumber)) {
            $this->phonenumbers[] = $phonenumber;
        }
    }

    /**
     * Remove phonenumber
     *
     * @param  Phone $phonenumber
     * @return User
     */
    public function removePhone(Phone $phonenumber){
        if ($this->phonenumbers->contains($phonenumber)) {
            $this->phonenumbers->removeElement($phonenumber);
        }
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated_at = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get ImageFile
     *
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return User
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get ImageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set Firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get Firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return ucfirst($this->firstname);
    }

    /**
     * Set middlename
     *
     * @param string $middleName
     * @return User
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string
     */
    public function getMiddleName()
    {
        return strtolower($this->middleName);
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return ucfirst($this->lastname);
    }

    /**
     * Get dayOfBirth
     *
     * @return \DateTime
     */
    public function getDayOfBirth()
    {
        return $this->dayOfBirth;
    }

    /**
     * Set dayOfBirth
     *
     * @param \DateTime $dayOfBirth
     * @return User
     */
    public function setDayOfBirth($dayOfBirth)
    {
        $this->dayOfBirth = $dayOfBirth;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function setLastLogin(\DateTime $currentLogin = NULL)
    {
        if(!is_null($this->lastLogin)){
            $previousLogin = $this->lastLogin;
            $this->setPreviousLogin($previousLogin);
        }
        parent::setLastLogin($currentLogin);
    }

    /**
     * Get previous_login
     *
     * @return \DateTime
     */
    public function getPreviousLogin()
    {
        return $this->previousLogin;
    }

    /**
     * Set previous_login
     *
     * @param  \DateTime $previousLogin
     * @return User
     */
    private function setPreviousLogin(\DateTime $previousLogin)
    {
        $this->previousLogin = $previousLogin;
        return $this;
    }

    /**
     * Set newsletter
     *
     * @param boolean $newsletter
     * @return User
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get newsletter
     *
     * @return boolean
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Set confirmationState.
     *
     * @param int $confirmationState
     */
    public function setConfirmationState($confirmationState)
    {
        $this->confirmationState = $confirmationState;
    }
    /**
     * Set confirmationState string.
     *
     * @param string $confirmationState
     */
    public function setConfirmationStateString($confirmationState)
    {
        $confirmationState = \array_search(\strtolower($confirmationState), ConfirmationState::$states);
        if ($confirmationState > 0) {
            $this->setConfirmationState($confirmationState);
        }
    }
    /**
     * Get confirmationState.
     *
     * @return int
     */
    public function getConfirmationState()
    {
        return $this->confirmationState;
    }
    /**
     * Get confirmationState string.
     *
     * @return string
     */
    public function getConfirmationStateString()
    {
        if (isset(ConfirmationState::$states[$this->confirmationState])) {
            return ConfirmationState::$states[$this->confirmationState];
        }
        return ConfirmationState::$states[0];
    }
}