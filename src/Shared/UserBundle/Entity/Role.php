<?php

namespace  Shared\UserBundle\Entity;

class Role {

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const DR_ADMIN = 'DR_ADMIN';
    const DR_EMPLOYEE = 'DR_EMPLOYEE';
    const DR_ADMINISTRATOR = 'DR_ADMINISTRATOR';
    const DR_PLANNER = 'DR_PLANNER';

    static public $roles = array(
        self::ROLE_ADMIN => 'role.admin',
        self::DR_ADMIN => 'role.dr_admin',
        self::DR_EMPLOYEE => 'role.dr_employee',
        self::DR_ADMINISTRATOR => 'role.dr_administrator',
        self::DR_PLANNER => 'role.dr_planner',
    ); // From top to down, sorted on importance

}