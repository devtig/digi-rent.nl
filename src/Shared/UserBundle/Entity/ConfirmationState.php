<?php

namespace Shared\UserBundle\Entity;

class ConfirmationState {

    const CONF_STATE_BETA = 1;
    const CONF_STATE_BETA_CONVERSION = 2;
    const CONF_STATE_REGISTER = 3;
    const CONF_STATE_RESET = 4;

    static public $states = array(
        self::CONF_STATE_BETA => 'conf_state.beta',
        self::CONF_STATE_BETA_CONVERSION => 'conf_state.conversion',
        self::CONF_STATE_REGISTER => 'conf_state.register',
        self::CONF_STATE_RESET => 'conf_state.reset',
    );

}