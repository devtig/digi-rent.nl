<?php

namespace Shared\UserBundle\Form;

use App\CompanyBundle\Form\Type\CompanyRegistrationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if($options['type'] == 'beta'){
            //Translation domain for error messages
            $builder
                ->remove('username')
                ->remove('email')
                ->remove('plainPassword')
                ->add('firstname', 'text', array(
                    'attr' => array(
                        'placeholder' => 'Voornaam',
                        'pattern'     => '.{2,}' //minlength
                    )
                ))
                ->add('lastname', 'text', array(
                    'attr' => array(
                        'placeholder' => 'Achternaam',
                        'pattern'     => '.{2,}' //minlength
                    )
                ))
                ->add('email', 'email', array(
                    'attr' => array(
                        'placeholder' => 'E-mail'
                    )
                ));
            ;
        }else{
            //Translation domain for error messages
            $builder
                ->remove('username', HiddenType::class, array(
                    'error_bubbling' => false
                ))
                ->add('company', CompanyRegistrationType::class)
                ->add('firstname')
                ->add('middlename')
                ->add('lastname')
            ;
        }
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'type'
        ));

        $resolver->setDefaults(array(
            'data_class' => 'Shared\UserBundle\Entity\User',
            'cascade_validation' => true,
            'type'  => null,
        ));
    }
}