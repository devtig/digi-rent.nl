<?php

namespace Shared\UserBundle\Form;

use App\CompanyBundle\Form\Type\CompanyRegisteredType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountConfirmedType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('dayofbirth', 'mablae_datetime', array( 'pickerOptions' =>
                array('format' => 'mm/dd/yyyy',
                    'viewMode' => 'days', // days, month, years, decades

                )))
            ->add('company', CompanyRegisteredType::class);

    }

    public function getName()
    {
        return 'user';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shared\UserBundle\Entity\User',
        ));
    }
}