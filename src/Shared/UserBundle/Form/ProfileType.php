<?php

namespace Shared\UserBundle\Form;

use App\SharedBundle\Form\Type\PhoneType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //TODO uncomment when in prod environment
//        $builder
//            ->add(
//            'recaptcha', 'ewz_recaptcha', array(
//            'attr'          => array(
//                'options' => array(
//                    'theme' => 'clean'
//                )
//            ),
//            'mapped' => false,
//            'constraints'   => array(
//                new True()
//            )
//        ));
        $builder
            ->remove('username')
            ->remove('current_password')
            ->add('firstname', 'text', array(
                "label"=>"form.firstname",
            ))
            ->add('middlename', 'text', array(
                "label"=>"form.middlename",
            ))
            ->add('lastname', 'text', array(
                "label"=>"form.lastname",
            ))
            ->add('dayofbirth', 'mablae_datetime',
                array(
                    "label" => "form.birthday",
                    'pickerOptions' =>
                    array(
                        'format' => 'YYYY-M-D',
                        'viewMode' => 'years', // days, month, years, decades
                    )
                )
            )
            ->add('phonenumbers', CollectionType::class, array(
                'entry_type' => PhoneType::class,
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('submit', 'submit',
                array(
                    'label' => 'Opslaan',
                    'attr' => array(
                        'icon' => 'send'
                    )
                )
            );

        $builder->add('imageFile', 'vich_image', array(
            'required'      => false,
            'allow_delete'  => false, // not mandatory, default is true
            'download_link' => false, // not mandatory, default is true
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

}