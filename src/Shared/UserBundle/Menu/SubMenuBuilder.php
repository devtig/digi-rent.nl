<?php namespace Shared\UserBundle\Menu;

use App\CoreBundle\Services\CreateMenuHelper;

class SubMenuBuilder
{

    private $menuHelper;

    function __construct(CreateMenuHelper $menuHelper)
    {
        $this->menuHelper = $menuHelper;
    }

    public function createAccountMenu(array $options)
    {
        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
            array('Profiel' => 'tab',
                'attributes' => array(
                    'route' => 'app_account_profile',
                    'edit' => 'app_account_profile_edit',
                ),
            ),
            array('Abonnement' => 'tab',
                'attributes' => array(
                    'route' => 'app_account_subscription',
                ),
            ),
            array('Updates' => 'tab',
                'attributes' => array(
                    'route' => 'app_account_subscription',
                ),
            ),
            array('Support' => 'tab',
                'attributes' => array(
                    'route' => 'app_account_support',
                    'update' => 2,
                ),
            ),
        );
        return $this->menuHelper->generateMenu(array('class' => 'nav nav-tabs border-bottom'), $menuItems);
    }

    public function createCompanyMenu(array $options)
    {
        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
            array('Profiel' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_profile',
                ),
            ),
            array('Magazijn' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_warehouse',
                ),
            ),
            array('Personeel' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_employees'
                ),
            ),
            array('Wagenpark' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_vehicles',
                    'update' => 2,
                ),
            ),
        );
        return $this->menuHelper->generateMenu(array('class' => 'nav nav-tabs border-bottom'), $menuItems);
    }
}