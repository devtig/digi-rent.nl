<?php

namespace Shared\UserBundle\Controller;

use App\CoreBundle\Event\DREvents;
use App\SharedBundle\Entity\Address;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Model\UserInterface;
use Shared\UserBundle\Entity\ConfirmationState;
use Shared\UserBundle\Entity\Role;
use Symfony\Component\HttpFoundation\Request;
use Shared\UserBundle\Form\AccountConfirmedType;

//Use statements from extended controller
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RegistrationController extends BaseController
{
    /**
     * Let's the user register via a form
     */
    public function registerAction(Request $request, $token = null)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        // Check if there is a valid token, otherwise create user manually
        if($token){
            if(null === $userManager->findUserByConfirmationToken($token)){
                $url = $this->generateUrl('web_index');
                return new RedirectResponse($url);
            }else{
                $user = $userManager->findUserByConfirmationToken($token);
                if($user->getConfirmationState() != ConfirmationState::CONF_STATE_BETA_CONVERSION){
                    $url = $this->generateUrl('web_index');
                    return new RedirectResponse($url);
                }
            }
        }else{
            $user = $userManager->createUser();
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);


        if ($form->isValid()){

            $event = new FormEvent($form, $request);

            try {
                $userManager->updateUser($user);
            } catch (\Exception $e) {
                // Todo sent email to admin and user something went wrong...
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);
                throw new \Exception($e->getMessage());
            }

//            if(null === $user->getConfirmationToken(){
//                $url = $this->generateUrl('fos_user_registration_confirm', $user->getConfirmationToken());
//                return new RedirectResponse($url);
//            }else{
//                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
//
//            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('FOSUserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(),
            'token' => $token
        ));
    }


    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction()
    {
        if ($this->getUser() instanceof UserInterface) {
            return $this->redirectToRoute($this->getParameter('fos_user.user.default_route'));
        }

        $email = $this->get('session')->get('fos_user_send_confirmation_email/email');

        if (empty($email)) {
            return new RedirectResponse($this->get('router')->generate('web_index'));
        }

        $this->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

        return $this->render('FOSUserBundle:Registration:check_email.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * Receive the confirmation token from user email provider, login the user and redirect afterwards
     */
    public function confirmAction(Request $request, $token = null)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            $url = $this->generateUrl('web_index');
            return new RedirectResponse($url);
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $form = $this->createForm(AccountConfirmedType::class, $user);

        // TODO change address dummy data
        $address = new Address();
        $address->setStreetname('test');
        $address->setHousenumber('test');
        $address->addAddressType(10);
        $address->setCity('test');
        $address->setZipcode('test');
        $address->setCountry(1);
        $user->getCompany()->addAddress($address);

        $form->setData($user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //Enable the user
            $user->setEnabled(true)
                ->setConfirmationToken(null)
                ->addRole(Role::DR_ADMIN);

            $userManager->updateUser($user);

            $event = new GetResponseUserEvent($user, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);


            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            // Adds successflash to flashbag TODO maybe we can show a success flash
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('UserBundle:Registration:confirmed.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }


    /**
     * Receive the confirmation token from user email provider, login the user and redirect afterwards
     */
    public function betaConfirmAction(Request $request, $token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user || $user->getConfirmationState() != ConfirmationState::CONF_STATE_BETA) {
            $url = $this->generateUrl('web_index');
            return new RedirectResponse($url);
        }else{

           $user ->setConfirmationToken($this->container->get('fos_user.util.token_generator')->generateToken())
               ->addRole(Role::ROLE_ADMIN)
               ->setConfirmationState(ConfirmationState::CONF_STATE_BETA_CONVERSION);

            $userManager->updateUser($user);

            $event = new GetResponseUserEvent($user, $request);

            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(DREvents::BETA_REGISTRATION_CONFIRMED, $event);

        }

        return $this->render('UserBundle:Registration:betaConfirmed.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        //TODO redirect user to success page? and with subdomain + link or redirect directly to it
        // Possible to directly login user
        $url = $this->generateUrl('web_account_profile');
        $response = new RedirectResponse($url);

        return $response;

        return $this->render('UserBundle:Registration:confirmed.html.twig', array(
            'user' => $user,
            'targetUrl' => $this->getTargetUrlFromSession(),
        ));
    }

    private function getTargetUrlFromSession()
    {
        // Set the SecurityContext for Symfony <2.6
        if (interface_exists('Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface')) {
            $tokenStorage = $this->get('security.token_storage');
        } else {
            $tokenStorage = $this->get('security.context');
        }

        $key = sprintf('_security.%s.target_path', $tokenStorage->getToken()->getProviderKey());

        if ($this->get('session')->has($key)) {
            return $this->get('session')->get($key);
        }
    }

}