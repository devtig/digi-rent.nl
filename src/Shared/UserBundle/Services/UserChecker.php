<?php namespace Shared\UserBundle\Services;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserChecker as BaseUserChecker;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker extends BaseUserChecker {

    private $container;
    private $basehost;

    function __construct(Container $container, $baseHost)
    {
        $this->container = $container;
        $this->basehost = $baseHost;
    }

    public function checkPreAuth(UserInterface $user)
    {
        parent::checkPreAuth($user);

        $request = $this->container->get('request');

        // TODO Retrieve domain from singleton, currentDomainListener
        // $this->subdomain  = $container->get('domain_manager')->getCurrentDomain();

        $subdomain = str_replace(".".$this->basehost, "", $request->getHost());
        $routeName = $request->get('_route');

        // Web environment
        //$this->basehost != $request->getHost()
        if(substr( $routeName, 0, 3 ) != "web"){
            if($user->getCompany()->getSubdomainName() != $subdomain){
                // User is not allowed to login with this domain
                throw new BadCredentialsException();
            }else if(!$user->isAccountNonLocked()) {
                $ex = new LockedException('User account is locked.');
                $ex->setUser($user);
                throw $ex;
            }
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        parent::checkPostAuth($user);
    }

}