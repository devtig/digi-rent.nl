<?php

namespace Shared\UserBundle\Services;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Security;


class AuthenticationHandler implements AuthenticationFailureHandlerInterface, LogoutSuccessHandlerInterface, AuthenticationSuccessHandlerInterface {

    /**
     * @var Router $router
     */
    protected $router;

    /**
     * @var AuthorizationChecker $authorizationChecker
     */
    protected $authorizationChecker;


    /**
     * @var String $baseHost
     */
    protected $baseHost;

    public function __construct($baseHost, Router $router, AuthorizationChecker $authorizationChecker)
    {
        $this->router = $router;
        $this->authorizationChecker = $authorizationChecker;
        $this->baseHost = $baseHost;
    }

    /**
     * Called when authentication fails
     *
     * @param  Request          $request
     * @param  AuthenticationException   $exception
     *
     * @return RedirectResponse never null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        if($request->attributes->get('subdomain')){
            return new RedirectResponse($this->router->generate('app_index', array('subdomain' => $request->attributes->get('subdomain'))));
        }else{
            return new RedirectResponse($this->router->generate('web_index'));
        }
    }

    /**
     * Called when logout procedure succeeds
     *
     * @param  Request          $request
     *
     * @return RedirectResponse
     */
    public function onLogoutSuccess(Request $request)
    {
        $request->getSession()->getFlashBag()->add('success', 'U bent succesvol uitgelogd.');
        if($request->attributes->get('subdomain')) {
            return new RedirectResponse($this->router->generate('app_index', array('subdomain' => $request->attributes->get('subdomain'))));
        }else{
            return new RedirectResponse($this->router->generate('web_user_login'));
        }
    }

    /**
     * Called when authentication succeeds
     *
     * @param  Request          $request
     * @param  TokenInterface   $token
     *
     * @return RedirectResponse never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if(is_null($token->getUser()->getPreviousLogin())){
            $request->getSession()->getFlashBag()->add('info', 'firstLogin');
        }else{
            $request->getSession()->getFlashBag()->add('info', $token->getUser()->getPreviousLogin());
        }

        // TODO currentSubdomain interface implementation instead of request subdomain
        if($request->attributes->get('subdomain')) {
            $response = new RedirectResponse($this->router->generate('app_index', array('subdomain' => $request->attributes->get('subdomain'))));
        }else{
            $response = new RedirectResponse($this->router->generate('web_user_login'));
        }
        return $response;
    }
}