<?php

namespace Shared\EmailBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use Sfk\EmailTemplateBundle\Loader\TwigLoader;
use Sfk\EmailTemplateBundle\Template\EmailTemplate;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TwigMailer implements MailerInterface
{
    /**
     * @var TwigLoader
     */
    protected $mailTemplate;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var \Twig_Environment
     */
    protected $twig;


    /**
     * TwigSwiftMailer constructor.
     *
     * @param TwigLoader $mailTemplate
     * @param Container $container
     * @param \Swift_Mailer $mailer
     * @param UrlGeneratorInterface $router
     * @param \Twig_Environment $twig
     */
    public function __construct(TwigLoader $mailTemplate, Container $container, \Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig)
    {
        $this->container = $container;
        $this->mailTemplate = $mailTemplate;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
    }

    /**
     * Send an email to a user to confirm the successful beta registration
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function sendBetaRegisteredEmail(UserInterface $user)
    {
        $url = $this->router->generate('web_user_beta_confirmed', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $template = $this->mailTemplate->load('@Email/templates/betaRegistered.template.twig', array('user' => $user, 'url' => $url));

        $this->sendMessage($template);
    }

    /**
     * Send an email to a user to confirm the successful beta confirmation
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function sendBetaConfirmationEmail(UserInterface $user)
    {
        $template = $this->mailTemplate->load('@Email/templates/betaConfirmation.template.twig', array('user' => $user));
        $this->sendMessage($template);
    }

    /**
     * Send an email to a user to confirm the successful account confirmation
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $template = $this->mailTemplate->load('@Email/templates/accountRegistration.template.twig', array('user' => $user, 'url' => $url));

        $this->sendMessage($template);
    }

    /**
     * Send an email to a user to confirm the successful account registration
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function sendConfirmationSuccessEmailMessage(UserInterface $user)
    {
        $url = $this->container->get('router')->generate('app_user_login', array('subdomain' => $user->getCompany()->getSubdomainName()), UrlGeneratorInterface::ABSOLUTE_URL);
        $template = $this->mailTemplate->load('@Email/templates/accountConfirmation2.template.twig', array('user' => $user, 'url' => $url));
        $this->sendMessage($template);
    }

    /**
     * Send an email to the admin with the adequate context key (functionname)
     *
     * @param UserInterface $user
     * @param string $context
     *
     * @return void
     */
    public function sendAdminUpdateEmail(UserInterface $user, $context)
    {
        $template = $this->mailTemplate->load('@Email/templates/adminUpdate.template.twig', array('user' => $user, 'context' => $context));
        $this->sendMessage($template);
    }


    /**
     * Send an email to the user with instructions to reset his/her account
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $url = $this->router->generate('web_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $template = $this->mailTemplate->load('@Email/templates/accountResetting.template.twig', array('user' => $user, 'url' => $url));

        $this->sendMessage($template);
    }

    /**
     * Send an email to the user with a confirmation that his/her account has been reset
     *
     * @param UserInterface $user
     *
     * @return void
     */
    public function sendResettingSuccessEmailMessage(UserInterface $user)
    {
        $template = $this->mailTemplate->load('@Email/templates/accountResettingSuccess.template.twig', array('user' => $user));
        $this->sendMessage($template);
    }


    /**
     * Send email with specific template
     *
     * @param EmailTemplate $template
     */
    protected function sendMessage(EmailTemplate $template)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($template->getSubject())
            ->setFrom($this->container->getParameter('admin-mail'))
            ->setTo($template->getFrom())
            ->setBody($template->getBody(), 'text/html');

        // TODO workaround for dev environment to send mails via Gmail (bypassing SSL, because error)
        // https://github.com/swiftmailer/swiftmailer/issues/544
        if (in_array($this->container->get('kernel')->getEnvironment(), array('test', 'dev'))) {
            $https['ssl']['verify_peer'] = FALSE;
            $https['ssl']['verify_peer_name'] = FALSE;

            $transport = \Swift_SmtpTransport::newInstance($this->container->getParameter('mailer_host'), $this->container->getParameter('mailer_port'), $this->container->getParameter('mailer_encryption'))
                ->setUsername($this->container->getParameter('mailer_user'))
                ->setPassword($this->container->getParameter('mailer_password'))
                ->setStreamOptions($https);

            $this->mailer->newInstance($transport)->send($message);

        } else {
            $this->mailer->send($message);
        }
    }

}
