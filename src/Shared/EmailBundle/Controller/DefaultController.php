<?php

namespace Shared\EmailBundle\Controller;

use Shared\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller
{
    public function indexAction(){

        $user =  $this->container->get('doctrine.orm.entity_manager')->getRepository('UserBundle:User')->findOneBy(array('username' => 'pknijff90@gmail.com'));
        $betaTest = false;

        if($betaTest){
            $url = $this->container->get('router')->generate('web_user_beta_confirmed', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        }else{
            $url = $this->container->get('router')->generate('app_user_login', array('subdomain' => $user->getCompany()->getSubdomainName()), UrlGeneratorInterface::ABSOLUTE_URL);
        }

        return $this->render('@Email/templates/accountRegistration.template.twig', array('user' => $user, 'url' => $url));

    }
}
