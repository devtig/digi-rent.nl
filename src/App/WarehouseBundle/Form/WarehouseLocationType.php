<?php

namespace App\WarehouseBundle\Form;

use App\SharedBundle\Form\Type\AddressType;
use App\WarehouseBundle\Entity\WarehouseLocation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;

class WarehouseLocationType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
                ->add('code')
                ->add('type')
                ->add('description')
                ->add('submit', 'submit', array(
                        'label' => 'Opslaan',
                        'attr' => array(
                            'icon' => 'plus'
                        )
                    )
                );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $warehouse = $event->getData();
            $form = $event->getForm();

            if (null === $warehouse->getParent()) {
                $form->add('address', AddressType::class);
            }
        });

    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => WarehouseLocation::class,
            ]
        );
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'warehouseLocation';
    }
}