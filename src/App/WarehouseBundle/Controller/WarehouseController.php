<?php

namespace App\WarehouseBundle\Controller;

use App\CoreBundle\Entity\AddressType;
use App\SharedBundle\Entity\Address;
use App\WarehouseBundle\Entity\WarehouseLocation;
use App\WarehouseBundle\Form\WarehouseLocationType;
use App\WarehouseBundle\Repository\WarehouseLocationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WarehouseController extends Controller
{
    public function indexAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager('dynamic');
        $repo = $manager->getRepository('WarehouseBundle:WarehouseLocation');

        $warehouses = $repo->findBy(array('parent' => null, 'level' => 0, 'archived' => false));
        $warehouseObject = new WarehouseLocation();

        // Check if this is the first warehouse, auto main warehouse
        if(empty($warehouses)){
            $main = true;
        }else{
            $main = null;
        }

        $form = $this->locationForm($repo, $warehouseObject, null, $main);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->persistLocation($warehouseObject);

            // Ticket Event Dispatcher, send mail to client and to administrator
            $request->getSession()->getFlashBag()->add('success', 'Magazijn is aangemaakt.');

            return $this->redirect($this->generateUrl('app_warehouse_index')."#index");
        }

        $responseArray = array(
            'warehouse_form' => $form->createView(),
        );

        if(!empty($warehouses)){
            $responseArray = array_merge_recursive($responseArray, array('warehouses' => $warehouses));
            foreach($warehouses as $warehouse){
                if($warehouse->isMain() == true){
                    $responseArray = array_merge_recursive($responseArray, array('warehouse' => $warehouse));
                }
            }
        }
        return $this->render('WarehouseBundle::index.html.twig', $responseArray );
    }

    public function detailsAction(Request $request, WarehouseLocation $warehouseLocationObject = null)
    {

        if($warehouseLocationObject){

        }else{
            return $this->redirect($this->generateUrl('app_warehouse_index')."#index");
        }

        $warehouseObject = new WarehouseLocation();

        $form = $this->locationForm(null, $warehouseObject, null, null);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->persistLocation($warehouseObject);

            // Ticket Event Dispatcher, send mail to client and to administrator
            $request->getSession()->getFlashBag()->add('success', 'Magazijn is aangemaakt.');

            return $this->redirect($this->generateUrl('app_warehouse_index')."#index");
        }



        return $this->render('WarehouseBundle::details.html.twig',
            array(
                'warehouse_form' => $form,
                'warehouse' => $warehouseLocationObject
                 )
        );

    }

    public function editAction(Request $request, WarehouseLocation $warehouseObject = null)
    {
        $form = $this->locationForm(null, $warehouseObject, $warehouseObject->getParent(), null);
        $form->handleRequest($request);

        $responseArray = array(
            'warehouse_form' => $form->createView(),
        );

        if($warehouseObject){
            $responseArray = array_merge_recursive($responseArray, array('warehouse' => $warehouseObject));
        }

        if ($form->isValid()) {

            $this->persistLocation($warehouseObject);

            // Ticket Event Dispatcher, send mail to client and to administrator
            $request->getSession()->getFlashBag()->add('success', 'Magazijn is aangemaakt.');

            return $this->redirect($this->generateUrl('app_warehouse_details', array('id' => $warehouseObject->getParent()->getId())));
        }

        return $this->render('WarehouseBundle::edit.html.twig', $responseArray );
    }

    public function addAction(Request $request, WarehouseLocation $warehouseLocation = null)
    {

        $manager = $this->getDoctrine()->getManager('dynamic');
        $repo = $manager->getRepository('WarehouseBundle:WarehouseLocation');

        $warehouseObject = new WarehouseLocation();

        $form = $this->locationForm($repo, $warehouseObject, $warehouseLocation, null);
        $form->handleRequest($request);

        $responseArray = array(
            'warehouse_form' => $form->createView(),
        );

        if($warehouseLocation){
            $responseArray = array_merge_recursive($responseArray, array('warehouse' => $warehouseObject));
        }

        if ($form->isValid()) {

            $this->persistLocation($warehouseObject);

            // Ticket Event Dispatcher, send mail to client and to administrator
            $request->getSession()->getFlashBag()->add('success', 'Magazijnlocatie is aangemaakt.');

            return $this->redirect($this->generateUrl('app_warehouse_details', array('id' => $warehouseObject->getParent()->getId())));
        }

        return $this->render('@Warehouse/newSublocation.html.twig', $responseArray );
    }

    function persistLocation(WarehouseLocation $warehouseLocation){
        $em = $this->getDoctrine()->getManager('dynamic');
        $em->persist($warehouseLocation);
        $em->flush();
    }

    function locationForm(WarehouseLocationRepository $repo = null, WarehouseLocation $warehouseLocation, $parent = null, $main = null){

        // Check if this should be a child location
        if($parent){
            $warehouseLocation->setParent($parent);
        }else{
            $address = new Address();
            $address->setAddressTypes(array(AddressType::getDefault()));
            $warehouseLocation->setAddress($address);
        }

        if($main){
            // Check if this is the first warehouse than it should be automatic the main warehouse
            $warehouseLocation->setMain(true);
        }

        return $this->createForm(new WarehouseLocationType(), $warehouseLocation);
    }

    function array_push_associative(&$arr) {
        $args = func_get_args();
        foreach ($args as $arg) {
            if (is_array($arg)) {
                foreach ($arg as $key => $value) {
                    $arr[$key] = $value;
                }
            }else{
                $arr[$arg] = "";
            }
        }
        return $arr;
    }

    public function archiveAction(WarehouseLocation $warehouseEntity){
        $em = $this->getDoctrine()->getManager('dynamic');

        $warehouseEntity->setArchived(true);
        $em->persist($warehouseEntity);
        $em->flush();

        return $this->redirectToRoute('app_warehouse_index');
    }

    public function deleteAction(WarehouseLocation $warehouseEntity){
        $em = $this->getDoctrine()->getManager('dynamic');

        $em->remove($warehouseEntity);
        $em->flush();

        return $this->redirectToRoute('app_warehouse_index');
    }
}
