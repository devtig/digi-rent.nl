<?php

namespace App\WarehouseBundle\Entity;

class LocationType {
    const LOCATION_BUILDING = 1;
    const LOCATION_AISLE = 2;
    const LOCATION_RACK = 3;
    const LOCATION_SHELFHEIGHT = 4;
    const LOCATION_SUBDIVISION = 5;

    static public $types = array(
        self::LOCATION_BUILDING => 'location.building',
        self::LOCATION_AISLE => 'location.aisle',
        self::LOCATION_RACK => 'location.rack',
        self::LOCATION_SHELFHEIGHT => 'location.shelfheight',
        self::LOCATION_SUBDIVISION => 'location.subdivision',
    );

    static public function getDefault(){
        return self::LOCATION_BUILDING;
    }
}