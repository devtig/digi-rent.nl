<?php

namespace App\WarehouseBundle\Entity;

use App\SharedBundle\Entity\Address;
use App\SharedBundle\Entity\Interfaces\AddressInterface;
use App\WarehouseBundle\Entity\Traits\LocationTypeAble;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;
use App\SharedBundle\Entity\Traits as SharedTraits;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as GedmoMapping;

/**
 * Location
 *
 * @GedmoMapping\Tree(type="nested")
 * @ORM\Table(name="warehouse_location")
 * @ORM\Entity(repositoryClass="App\WarehouseBundle\Repository\WarehouseLocationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class WarehouseLocation implements AddressInterface
{
    // Non-referencing Traits
    use Traits\Identifiable,
        Traits\NameAble,
        Traits\Describable,
        Traits\TimeStampable,
        Traits\Sluggable,
        Traits\Archiveable;

    // M:1 Referencing Traits
    use SharedTraits\Addressable;

    // Owning Traits
    use LocationTypeAble;

    function __construct()
    {
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->archived = false;
        $this->setLocationTypeDefault();
    }

    /**
     * Set Address
     *
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        // Only top level locations can have addresses (children not allowed here)
        if(is_null($this->getParent())){
            $this->address = $address;
        }
    }

    /**
     *
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\Product", mappedBy="warehouseLocation")
     */
    protected $products;

    /**
     * @GedmoMapping\TreeParent
     * @ORM\ManyToOne(targetEntity="WarehouseLocation", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="WarehouseLocation", mappedBy="parent", cascade={"persist"})
     */
    private $children;

    /**
     * @GedmoMapping\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @GedmoMapping\TreeLevel
     * @ORM\Column(name="level", type="integer")
     */
    private $level;

    /**
     * @GedmoMapping\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @GedmoMapping\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="main", type="boolean", nullable=true)
     */
    private $main;

    /**
     * @var string
     * @ORM\Column(name="code", type="string", length=2, nullable=true)
     */
    private $code;

    /**
     * Set parent
     *
     * @param WarehouseLocation $parent
     * @return WarehouseLocation
     */
    public function setParent(WarehouseLocation $parent = NULL) {
        $index = array_search($parent->getType(), array_keys(LocationType::$types)) + 2;
        $this->setType($index);
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return WarehouseLocation
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * Set child
     *
     * @param WarehouseLocation $child
     * @return WarehouseLocation
     */
    public function setChild(WarehouseLocation $child) {
        $this->children[] = $child;
        $child->setParent($this);
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getTypes() {
        return LocationType::$types;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get root
     *
     * @return int
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Get level
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Get left
     *
     * @return int
     */
    public function getLeft()
    {
        return $this->lft;
    }
    /**
     * Get right
     *
     * @return int
     */
    public function getRight()
    {
        return $this->rgt;
    }

    /**
     * Is main
     *
     * @return boolean
     */
    public function isMain()
    {
        return $this->main;
    }

    /**
     * Set main
     *
     * @param boolean $main
     */
    public function setMain($main)
    {
        $this->main = $main;
    }

}