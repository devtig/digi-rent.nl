<?php

namespace App\WarehouseBundle\Entity\Traits;

use App\WarehouseBundle\Entity\LocationType;

trait LocationTypeAble {

    /**
     * @var int
     * @ORM\Column(name="type", type="smallint", nullable=false)
     */
    private $type;

    /**
     * Set type
     *
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return $this
     */
    public function setTypeString($type)
    {
        $type = \array_search(\strtolower($type), LocationType::$types);
        if ($type > 0) {
            $this->setType($type);
        }
        return $this;
    }

    /**
     * Get type string
     *
     * @return string
     */
    public function getTypeString()
    {
        if (isset(LocationType::$types[$this->type])) {
            return LocationType::$types[$this->type];
        }
        return LocationType::$types[0];
    }

    /**
     * Get type string
     *
     * @return string
     */
    public function setLocationTypeDefault()
    {
        $this->type = LocationType::getDefault();
    }

}