<?php namespace App\WarehouseBundle\Menu;

use App\CoreBundle\Services\CreateMenuHelper;

class SubMenuBuilder
{

    private $menuHelper;

    function __construct(CreateMenuHelper $menuHelper)
    {
        $this->menuHelper = $menuHelper;
    }

    public function createMenu(array $options)
    {
        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
            array('Aanvragen' => 'tab',
                'attributes' => array(
                    'route' => 'app_warehouse',
                    'edit' => 'app_account_profile_edit',
                ),
            ),
            array('Offertes' => 'tab',
                'attributes' => array(
                    'route' => 'app_account_subscription',
                ),
            ),
            array('Facturen' => 'tab',
                'attributes' => array(
                    'route' => 'app_account_support',
                ),
            ),
            array('Betalingen' => 'tab',
                'attributes' => array(
                    'route' => 'app_account_support',
                ),
            ),
//            array('Instellingen' => 'tab',
//                'icon' => 'wrench',
//                'attributes' => array(
//                    'route' => 'app_settings_index',
//                    'class' => 'page_settings'
//                ),
//            ),
        );
        return $this->menuHelper->generateMenu(array('class' => 'nav nav-tabs'), $menuItems);
    }
}