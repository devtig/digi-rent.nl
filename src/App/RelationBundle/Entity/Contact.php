<?php

namespace App\ContactBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\CoreBundle\Entity\Traits as Traits;
use App\SharedBundle\Entity\Traits as SharedTraits;
use App\CoreBundle\Entity\Branche;

/**
 * Contact
 *
 * @ORM\Table(name="contacts")
 * @ORM\Entity
 */
class Contact
{
    use Traits\Identifiable,
        Traits\NameAble,
        Traits\Describable,
        Traits\TimeStampable,
        SharedTraits\Addressable;

    /**
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\ProductExternal", mappedBy="supplier")
     */
    protected $externalProducts;

    /**
     * @var Branche
     *
     * @ORM\ManyToOne(targetEntity="App\CoreBundle\Entity\Branche", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="branche_id", referencedColumnName="id")
     * @Assert\Type(type="App\CoreBundle\Entity\Branche")
     * @Assert\Valid()
     */
    private $branche;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person", type="string", length=255)
     */
    private $contactPerson1;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person2", type="string", length=255, nullable=true)
     */
    private $contactPerson2;

    /**
     * @var string
     *
     * @ORM\Column(name="main_phone_nr", type="string", length=255)
     */
    private $mainPhoneNr;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person1_phone", type="string", length=255)
     */
    private $contactPerson1Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person1_email", type="string", length=255)
     */
    private $contactPerson1Email;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person2_phone", type="string", length=255, nullable=true)
     */
    private $contactPerson2Phone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person2_email", type="string", length=255, nullable=true)
     */
    private $contactPerson2Email;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255)
     */
    private $website;

    /**
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;
    

    /**
     * Set contactPerson1
     *
     * @param string $contactPerson1
     * @return Contact
     */
    public function setContactPerson1($contactPerson1)
    {
        $this->contactPerson1 = $contactPerson1;

        return $this;
    }

    /**
     * Get contactPerson1
     *
     * @return string 
     */
    public function getContactPerson1()
    {
        return $this->contactPerson1;
    }

    /**
     * Set contactPerson2
     *
     * @param string $contactPerson2
     * @return Contact
     */
    public function setContactPerson2($contactPerson2)
    {
        $this->contactPerson2 = $contactPerson2;

        return $this;
    }

    /**
     * Get contactPerson2
     *
     * @return string 
     */
    public function getContactPerson2()
    {
        return $this->contactPerson2;
    }

    /**
     * Set mainPhoneNr
     *
     * @param string $mainPhoneNr
     * @return Contact
     */
    public function setMainPhoneNr($mainPhoneNr)
    {
        $this->mainPhoneNr = $mainPhoneNr;

        return $this;
    }

    /**
     * Get mainPhoneNr
     *
     * @return string 
     */
    public function getMainPhoneNr()
    {
        return $this->mainPhoneNr;
    }

    /**
     * Set contactPerson1Phone
     *
     * @param string $contactPerson1Phone
     * @return Contact
     */
    public function setContactPerson1Phone($contactPerson1Phone)
    {
        $this->contactPerson1Phone = $contactPerson1Phone;

        return $this;
    }

    /**
     * Get contactPerson1Phone
     *
     * @return string 
     */
    public function getContactPerson1Phone()
    {
        return $this->contactPerson1Phone;
    }

    /**
     * Set contactPerson1Email
     *
     * @param string $contactPerson1Email
     * @return Contact
     */
    public function setContactPerson1Email($contactPerson1Email)
    {
        $this->contactPerson1Email = $contactPerson1Email;

        return $this;
    }

    /**
     * Get contactPerson1Email
     *
     * @return string 
     */
    public function getContactPerson1Email()
    {
        return $this->contactPerson1Email;
    }

    /**
     * Set contactPerson2Phone
     *
     * @param string $contactPerson2Phone
     * @return Contact
     */
    public function setContactPerson2Phone($contactPerson2Phone)
    {
        $this->contactPerson2Phone = $contactPerson2Phone;

        return $this;
    }

    /**
     * Get contactPerson2Phone
     *
     * @return string 
     */
    public function getContactPerson2Phone()
    {
        return $this->contactPerson2Phone;
    }

    /**
     * Set contactPerson2Email
     *
     * @param string $contactPerson2Email
     * @return Contact
     */
    public function setContactPerson2Email($contactPerson2Email)
    {
        $this->contactPerson2Email = $contactPerson2Email;

        return $this;
    }

    /**
     * Get contactPerson2Email
     *
     * @return string 
     */
    public function getContactPerson2Email()
    {
        return $this->contactPerson2Email;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Contact
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Contact
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return Branche
     */
    public function getBranche()
    {
        return $this->branche;
    }

    /**
     * @param Branche $branche
     */
    public function setBranche(Branche $branche)
    {
        $this->branche = $branche;
    }
}
