<?php

namespace App\SettingsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * Settings
 *
 * @ORM\Table(name="settings")
 * @ORM\Entity(repositoryClass="App\SettingsBundle\Repository\Settings")
 */
class Settings
{

    use Traits\Identifiable;
    // WarehouseStrategy
    // Newsletter
    // Templates
    //

}

