<?php

namespace App\CategoryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use App\CategoryBundle\Entity\Category;
use App\CategoryBundle\Entity\CategoryHasSpecifications;
use App\CategoryBundle\Entity\CategoryRepository;

class CategoryNewType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

//        $division = $this->division;

        $builder
            ->add('name', 'text', array(
                'label' => 'Naam:')
            )
//            ->add('subtitle', 'text', array(
//                'label' => 'Sub titel:')
//            )
            ->add('specs', 'collection', array(
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
            ))
            ->add('prefix', 'text', array(
                        'label' => 'Prefix:',
                    'attr' => array(
                        'class' => 'maxLength',
                        'maxLength' => '4'
                    )
                )
            )
            ->add('parent', 'entity', array(
                'required' => false,
                'label' => 'Categorie:',
                'class' => 'CategoryBundle:Category',
                'placeholder' => 'Is een hoofdcategorie',
                'property' => 'indentedTitle',
                'multiple' => false,
                'expanded' => false,
            ))
            ->add('description', 'textarea', array(
                'label' => 'Omschrijving:')
            )
//            ->add('division', 'entity', array(
//                    'placeholder' => 'Selecteer een divisie',
//                    'class' => 'SettingsBundle:Division',
//                    'label' => 'Divisie:',
//                )
//            )
            ->add('contains_products', 'checkbox', array(
                    'label' => 'Bevat producten:',
                    'required' => false,
                )
            )
            ->add('archived', 'checkbox', array(
                    'label' => 'Gearchiveerd:',
                    'required' => false,
                )
            )
            ->add('submit', 'submit', array(
                    'label' => 'Opslaan',
                    'icon' => '<i class="fa fa-save"></i>'
                )
            )
            ->add('cancel', 'submit', array(
                    'label' => 'Annuleer',
                    'icon' => '<i class="fa fa-times"></i>'
                )
            );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\CategoryBundle\Entity\Category',
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_categorybundle_category';
    }
}
