<?php

namespace App\CategoryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use App\CategoryBundle\Entity\Category;
use App\CategoryBundle\Entity\CategoryHasSpecification;
use App\CategoryBundle\Entity\CategoryRepository;

class CategoryDetailsType extends AbstractType
{
    private $division;
    private $category;
    private $categoryRepository;

    public function __construct(Category $category, $categoryRepository)
    {
        $this->category = $category;
//        $this->division = $division;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $division = $this->division;
        $category = $this->category;

        $builder
            ->add('name', 'text', array(
                'label' => 'Naam:')
            )
//            ->add('subtitle', 'text', array(
//                'label' => 'Sub titel:')
//            )
            ->add('prefix', 'text', array(
                    'label' => 'Prefix:',
                    'attr' => array(
                        'maxLength' => '4',
                        'class' => 'maxLength'
                    )
                )
            )
            ->add('parent', 'entity', array(
                'required' => false,
                'label' => 'Categorie:',
                'class' => 'CategoryBundle:Category',
                'placeholder' => 'Is een hoofdcategorie',
                'property' => 'indentedTitle',
                'multiple' => false,
                'expanded' => false,
                'query_builder' => function (\App\CategoryBundle\Repository\CategoryRepository $r) use ($division)
                    {
                        return $r->getCategories();

                    }
                )
            )
            ->add('description', 'textarea', array(
                'label' => 'Omschrijving:',
                'attr' => array( 'style' => 'resize: vertical;')
                )
            )
//            ->add('division', 'entity', array(
//                    'placeholder' => 'Selecteer een divisie',
//                    'class' => 'SettingsBundle:Division',
//                    'label' => 'Divisie:',
//                )
//            )
            ->add('specs', 'collection', array(
                'type' => new CategoryHasSpecsType($this->category, $this->categoryRepository),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => false,
                'label' => false,
                'by_reference' => true,
                'options' => array(
                    'data_class' => 'App\CategoryBundle\Entity\CategoryHasSpecification'
                ),
                )
            )
            ->add('contains_products', 'checkbox', array(
                    'label' => 'Bevat producten:',
                    'required' => false,
                )
            )
            ->add('archived', 'checkbox', array(
                    'label' => 'Gearchiveerd:',
                    'required' => false,
                )
            )
            ->add('submit', 'submit', array(
                    'label' => 'Opslaan',
                    'icon' => '<i class="fa fa-save"></i>'
                )
            )
            ->add('cancel', 'submit', array(
                    'label' => 'Annuleer',
                    'icon' => '<i class="fa fa-times"></i>'
                )
            );

        if($category->isArchived()){
            $builder->add('delete', 'submit', array(
                'label' => 'Verwijder',
                'icon' => '<i class="fa fa-trash"></i>'
                )
            );
        }else{
            $builder->add('archive', 'submit', array(
                'label' => 'Archiveer',
                'icon' => '<i class="fa fa-archive"></i>'
                )
            );
        }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\CategoryBundle\Entity\Category',
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'uniq_backendbundle_categorybundle_category';
    }
}
