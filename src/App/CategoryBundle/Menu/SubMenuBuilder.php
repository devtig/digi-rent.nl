<?php namespace App\CatalogBundle\Menu;

use App\CoreBundle\Services\CreateMenuHelper;

class SubMenuBuilder
{

    private $menuHelper;

    function __construct(CreateMenuHelper $menuHelper)
    {
        $this->menuHelper = $menuHelper;
    }

    public function createMenu(array $options)
    {
        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
            array('Algemeen' => 'tab',
                'attributes' => array(
                    'route' => 'app_catalog_index',
                ),
            ),
            array('Media' => 'tab',
                'attributes' => array(
                    'route' => 'app_catalog_categories_index',
                ),
            ),
            array('Specificatie Template' => 'tab',
                'attributes' => array(
                    'route' => 'app_catalog_products_index'
                ),
            ),
            array('Instellingen' => 'tab',
                'attributes' => array(
                    'route' => 'app_catalog_settings_index',
                ),
            ),
        );
        return $this->menuHelper->generateMenu(array('class' => 'nav nav-tabs border-bottom'), $menuItems);
    }
}