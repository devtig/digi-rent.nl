<?php

namespace App\CategoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as GedmoMapping;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits;

/**
 * Category
 *
 * @GedmoMapping\Tree(type="nested")
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="App\CategoryBundle\Repository\CategoryRepository")
 */
class Category
{
    use Traits\Identifiable,
        Traits\NameAble,
        Traits\Describable,
        Traits\TimeStampable,
        Traits\Sluggable,
        Traits\Enableable,
        Traits\Archiveable;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->archived = false;
        $this->enabled = false;
        $this->specs = new ArrayCollection();
    }

    public function getIndentedTitle()
    {
        return $this->name.' ('.$this->getPrefix().')';
    }

    /**
     * @ORM\OneToMany(targetEntity="App\CategoryBundle\Entity\CategoryHasSpecification", mappedBy="category", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $specs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="contains_products", type="boolean", nullable=false)
     */
    private $containsProducts;

    /**
     * @var string
     *
     * @ORM\Column(name="prefix", type="string", length=4, nullable=false, unique=true)
     * @Assert\NotBlank(message = "Prefix is verplicht")
     * @Assert\Length(
     *      min = 3,
     *      max = 4,
     *      minMessage = "Prefix moet minimaal 3 karakters lang zijn.",
     *      maxMessage = "Prefix mag maximaal 4 karakters lang zijn."
     * )
     *
     */
    private $prefix;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\Product", mappedBy="category")
     */
    protected $products;

    /**
     * @GedmoMapping\TreeParent
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", cascade={"persist"})
     */
    private $children;
    /**
     * @GedmoMapping\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;
    /**
     * @GedmoMapping\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;
    /**
     * @GedmoMapping\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;

    /**
     * @GedmoMapping\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $level;

    /**
     * Add specs
     *
     * @param CategoryHasSpecification $categoryHasSpecification
     * @return Category
     */
    public function addSpec(CategoryHasSpecification $categoryHasSpecification)
    {
        $categoryHasSpecification->setCategory($this);
        $this->specs[] = $categoryHasSpecification;

        return $this;
    }

    public function setSpecs($specs)
    {
        foreach($specs as $spec) {
            var_dump($spec);

            $spec->setCategory($this);
        }
            die();
        $this->specs = $specs;
    }

    /**
     * Remove specType
     *
     * @param CategoryHasSpecification $specRefrence
     */
    public function removeSpec(CategoryHasSpecification $specRefrence)
    {
        $this->specs->removeElement($specRefrence);
    }

    /**
     * Get specType
     *
     * @return ArrayCollection
     */
    public function getSpecs()
    {
        return $this->specs;
    }

    /**
     * Set contains_products
     *
     * @param boolean $containsProducts
     * @return Category
     */
    public function setContainsProducts($containsProducts)
    {
        $this->containsProducts = $containsProducts;
        return $this;
    }

    /**
     * Get contains_product
     *
     * @return boolean
     */
    public function getContainsProducts()
    {
        return $this->containsProducts;
    }

    /**
     * Set prefix
     *
     * @param string $prefix
     * @return Category
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Get products
     *
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set child
     *
     * @param Category $child
     * @return Category
     */
    public function setChild(Category $child) {
        $this->children[] = $child;
        $child->setParent($this);
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param Category $parent
     * @return Category
     */
    public function setParent(Category $parent = NULL) {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return Category
     */
    public function getParent() {
        return $this->parent;
    }

    public function getRoot()
    {
        return $this->root;
    }
    public function getLevel()
    {
        return $this->level;
    }
    public function getLeft()
    {
        return $this->lft;
    }
    public function getRight()
    {
        return $this->rgt;
    }
}

