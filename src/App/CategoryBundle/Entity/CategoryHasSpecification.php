<?php

namespace App\CategoryBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use App\ProductBundle\Entity\ProductSpecification;

/**
 * CategoryHasSpecification
 *
 * @ORM\Table(name="category_specs")
 * @ORM\Entity(repositoryClass="Gedmo\Sortable\Entity\Repository\SortableRepository")
 */
class CategoryHasSpecification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\ManyToOne(targetEntity="App\CategoryBundle\Entity\Category", inversedBy="specs") */
    protected $category;

    /** @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\ProductSpecification") */

    protected $specification;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
    */
    private $position;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->specification->getTitle();
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return CategoryHasSpecification
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set category
     *
     * @param Category $category
     * @return CategoryHasSpecification
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set specification
     *
     * @param ProductSpecification $specification
     * @return CategoryHasSpecification
     */
    public function setSpecification(ProductSpecification $specification = null)
    {
        $this->specification = $specification;

        return $this;
    }

    /**
     * Get specification
     *
     * @return CategoryHasSpecification
     */
    public function getSpecification()
    {
        return $this->specification;
    }

}
