<?php

namespace App\CategoryBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class CategoryRepository extends NestedTreeRepository
{

    public function getCategories()
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.children', 'child', 'WITH', 'child.parent = c')
            ->add('orderBy','c.root ASC, c.lft ASC');

        return $qb ;
    }

    public function whereCurrentDivision (\Doctrine\ORM\QueryBuilder $qb, $division)
    {
        $qb->where('c.division = :division')
            ->setParameter('division', $division);

        return $qb;
    }

}