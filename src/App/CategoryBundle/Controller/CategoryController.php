<?php

namespace App\CategoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\CategoryBundle\Entity\Category;
use App\CategoryBundle\Form\CategoryDetailsType;
use App\CategoryBundle\Form\CategoryNewType;
//use App\SettingsBundle\Entity\Division;

/**
 * Category controller.
 *
 */
class CategoryController extends Controller
{
    public $name = 'category';

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager('dynamic');

        $categories = $em->getRepository('CategoryBundle:Category')->findBy([], ['updated_at' => 'DESC']);

//        $breadcrumbs = $this->get("white_october_breadcrumbs");
//        $breadcrumbs->addItem("Catalogus");
//        $breadcrumbs->addItem("Categorieën");
//        $breadcrumbs->addItem("Overzicht");

        return $this->render('CategoryBundle::index.html.twig', array(
            'categories' => $categories,
            'categoryTree' => $this->getCategoryTree($this)
        ));
    }

    /**
     * Creates a new Category entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Category();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager('dynamic');
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('app_catalog_category_edit', array('id' => $entity->getId())));
        }

        return $this->render('CategoryBundle:Category:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Category entity.
     *
     * @param Category $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Category $entity)
    {
        $form = $this->createForm(new CategoryNewType($entity), $entity, array(
            'action' => $this->generateUrl('app_catalog_category_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Category entity.
     *
     */
    public function newAction($name = null, Request $request)
    {
        $categoryEntity = new Category();
        if($name){
            $categoryEntity->setName($name);
        }
        $em = $this->getDoctrine()->getManager('dynamic');

        $categoryForm = $this->createForm(new CategoryNewType($categoryEntity), $categoryEntity);
        $categoryForm->handleRequest($request);

        if($categoryForm->get('cancel')->isClicked()){
            return $this->redirect($this->generateUrl('app_catalog_categories_index'));
        }

//        $breadcrumbs = $this->get("white_october_breadcrumbs");
//        $breadcrumbs->addItem("Catalogus");
//        $breadcrumbs->addItem("Categorieën", $this->get('router')->generate("categories_main"));
//        $breadcrumbs->addItem("Nieuw");

        $exceptionHelper = $this->get('dr_core.helper_exception');

        if ($categoryForm->isValid()) {
            try {
                $categoryEntity->setPrefix(strtoupper($categoryEntity->getPrefix()));
                $em->persist($categoryEntity);
                $em->flush();
                $this->addFlash(
                    'success',
                    '<a class="notification" href=' . $this->generateUrl('app_catalog_category_edit', ['id' => $categoryEntity->getId()]) . '>' . $categoryEntity->getName() . '</a>' . ' is succesvol aangemaakt'
                );
            } catch (\Exception $e) {
                $this->addFlash('failed', $exceptionHelper->EntityErrorMessage($e));
            }

            return $this->redirect($this->generateUrl('app_catalog_categories_index'));
        }


        return $this->render('@Category/new.html.twig', array(
            'category'      => $categoryEntity,
            'categoryForm'  => $categoryForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager('dynamic');
        $categoryRepo = $em->getRepository('CategoryBundle:Category');

        $categoryEntity = $categoryRepo->find($id);

//        $breadcrumbs = $this->get("white_october_breadcrumbs");
//        $breadcrumbs->addItem("Catalogus");
//        $breadcrumbs->addItem("Categorieën", $this->get('router')->generate("categories_main"));
//        $breadcrumbs->addItem($categoryEntity->getTitle());
//        $breadcrumbs->addItem("details");

        $categoryForm = $this->createForm(new CategoryDetailsType($categoryEntity, $categoryRepo), $categoryEntity);
        $categoryForm->handleRequest($request);

        if($categoryForm->get('cancel')->isClicked()){
            return $this->redirect($this->generateUrl('app_catalog_categories_index'));
        }
//        foreach($categoryEntity->getSpecsTemplate()->getSpecs() as $spec){
//            \Doctrine\Common\Util\Debug::dump($spec->getSpecType()->getTitle());
//        }
//        die();

        $exceptionHelper = $this->get('dr_core.helper_exception');
        $categoryEntity->setPrefix(strtoupper($categoryEntity->getPrefix()));

        if ($categoryForm->isValid()) {
            if($categoryForm->get('submit')->isClicked()) {
                $em->persist($categoryEntity);
                $em->flush();
                $this->addFlash(
                    'success',
                    '<a class="notification" href=' . $this->generateUrl('app_catalog_category_edit', ['id' => $categoryEntity->getId()]) . '>' . $categoryEntity->getName() . '</a>' . ' is succesvol gewijzigd'
                );
            }elseif($categoryForm->has('archive')){
                if($categoryForm->get('archive')->isClicked()){
                    $categoryEntity->setArchived(true);
                    $em->persist($categoryEntity);
                    $em->flush();
                    $this->addFlash(
                        'success',
                        '<a class="notification" href='.$this->generateUrl('app_catalog_category_edit', ['id' => $categoryEntity->getId()]).'>'.$categoryEntity->getName().'</a>'.' is succesvol gearchiveerd'
                    );
                }
            }elseif($categoryForm->get('delete')->isClicked()){
                $categoryTitle = $categoryEntity->getName();
                try{
                    $em->remove($categoryEntity);
                    $em->flush();
                }catch (\Exception $e){
                    $this->addFlash('failed', $exceptionHelper->EntityErrorMessage($e));
                }

                $this->addFlash(
                    'success',
                    'Categorie: '.$categoryTitle.', is succesvol verwijderd'
                );
            }

            return $this->redirect($this->generateUrl('app_catalog_categories_index'));
        }


        if (!$categoryEntity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        return $this->render('CategoryBundle::edit.html.twig', array(
            'specs'      => $categoryEntity->getSpecs(),
            'category'      => $categoryEntity,
            'categoryForm'  => $categoryForm->createView(),
            'path'          => $categoryRepo->getPath($categoryEntity)
        ));
    }


    /**
     * Updates an existing Category entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager('dynamic');

        $category = $em->getRepository('CategoryBundle:Category')->find($id);

        if (!$category) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        return $this->render('CategoryBundle::edit.html.twig', array(
            'category'      => $category,
        ));
    }
    /**
     * Deletes a Category entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager('dynamic');
            $entity = $em->getRepository('CategoryBundle:Category')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Category entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('app_catalog_categories_index'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public static function getCategoryTree($controller){
        $em = $controller->getDoctrine()->getManager('dynamic');

//        $divisionRepo = $em->getRepository('SettingsBundle:Division');
//        $divisionEntity = $divisionRepo->findOneBy(array('title' => 'uniq-rental'));

//        // 0 and 1: entire category repo
        $categoryRepo = $em->getRepository('CategoryBundle:Category');
//
//        $queryBuilder = $categoryRepo
//            ->createQueryBuilder('categoryBuilder');
//        $query = $queryBuilder->select('category')
//            ->from('CategoryBundle:Category', 'category')
//            ->where('category.division = :var1')
//            ->andWhere('category.lvl = :var2')
//            ->setParameter(':var1', $divisionEntity)
//            ->setParameter(':var2', 0)
//            ->getQuery();
//
//var_dump($query);
//        die();

        // 2: each root category
//        $categoryRepo = $em->getRepository('CategoryBundle:Category')->findBy(array('division' => $divisionEntity, 'lvl' => 0));
//
//        echo '<pre>';
//            \Doctrine\Common\Util\Debug::dump($categoryRepo);
//        echo '</pre>';
//        die();

       // 1 : filtered repo with querybuilder (gives 24 times result)
//        $queryBuilder = $categoryRepo
//            ->createQueryBuilder('categoryBuilder');
//        $query = $queryBuilder->select('category')
//            ->from('CategoryBundle:Category', 'category')
//            ->where('category.division = :var1')
//            ->andWhere('category.lvl = :var2')
//            ->setParameter(':var1', $divisionEntity)
//            ->setParameter(':var2', 0)
//            ->getQuery();
//
//////        // 1 : filtered repo with querybuilder
//        var_dump($query->getArrayResult());
////        var_dump($categoryRepo->buildTree($query->getArrayResult()));
//        die();
//        echo '<pre>';
//            \Doctrine\Common\Util\Debug::dump($query->getArrayResult(), $options);
//        echo '</pre>';
//        die();

        // 2: each root category
//        foreach($categoryRepo as $entity){
//            echo '<pre>';
//            \Doctrine\Common\Util\Debug::dump($entity->getDivision()->getTitle());
//            echo '</pre>';
//        }
//        die();

//        var_dump($categoryRepo->childrenHierarchy());
//        die();
        $treeReturn = [];

        foreach($categoryRepo->childrenHierarchy() as $categoryTree){
            $treeReturn[] =  self::getChildren($controller, $categoryRepo, $categoryTree);
        }

//        dump($treeReturn);
//        die();

        return $treeReturn;
    }

    public static function getChildren($controller, $repository, $categoryTree){
        $treeNode = new \stdClass();
        $treeNode->text = $categoryTree['name'];
        $treeNode->archived = $categoryTree['archived'];
//        var_dump($categoryTree);
//        die();
        $treeNode->originalURL = $categoryTree['archived'];

        $treeNode->tags[] = $repository->childCount($repository->find($categoryTree['id']));

        foreach($treeNode->tags as $tag) {
            foreach ($categoryTree['__children'] as $child) {
                $treeNode->nodes[] = self::getChildren($controller, $repository, $child);
            }
        }

        if($controller->name == 'product'){
            if($categoryTree['containsProducts']) {
                $treeNode->href = $controller->generateUrl('app_catalog_product_new', ['category_id' => $categoryTree['id']]);
            }
        }else{
            $treeNode->href = $controller->generateUrl('app_catalog_category_edit', ['id' => $categoryTree['id']]);
        }

        return $treeNode;
    }
}
