<?php

namespace App\SharedBundle\EventSubscriber;

use App\SharedBundle\Entity\Address;
use App\SharedBundle\Entity\Phone;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class EntityRelationSubscriber implements EventSubscriber {

    private $container;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata,
        );
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        // the $metadata is the whole mapping info for this class
        $metadata = $eventArgs->getClassMetadata();
        $traits =  $this->container->getParameter('traits');
        $prefix =  $this->container->getParameter('base-db-prefix');

        $namingStrategy = $eventArgs
            ->getEntityManager()
            ->getConfiguration()
            ->getNamingStrategy()
        ;

        foreach($traits as $trait => $params) {
            switch ($trait) {
                case "multiAddressable":
                    // Check intersections between current class interfaces and interfaces to add dynamic relation
                    if (count(array_intersect(class_implements($metadata->getName()), $params['interfaces'])) > 0) {
                        $metadata->mapManyToMany(array(
                            'targetEntity' => Address::class,
                            'fieldName' => 'addresses',
                            'cascade' => array('persist'),
                            'joinTable' => array(
                                'name' => $prefix . strtolower($namingStrategy->classToTableName($metadata->getName())) . '_addresses',
                                'joinColumns' => array(
                                    array(
                                        'name' => $namingStrategy->joinKeyColumnName($metadata->getName()),
                                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                                        'onDelete' => 'CASCADE',
                                        'onUpdate' => 'CASCADE',
                                    ),
                                ),
                                'inverseJoinColumns' => array(
                                    array(
                                        'name' => 'address_id',
                                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                                        'onDelete' => 'CASCADE',
                                        'onUpdate' => 'CASCADE',
                                    ),
                                )
                            )
                        ));
                    }
                    break;
                case "addressable":
                    // Check intersections between current class interfaces and interfaces to add dynamic relation
                    if (count(array_intersect(class_implements($metadata->getName()), $params['interfaces'])) > 0) {
                        $metadata->mapOneToOne(array(
                            'fieldName' => 'address',
                            'targetEntity' => Address::class,
                            'joinColumns' => array(
                                0 => array(
                                    'name' => 'address_id',
                                    'referencedColumnName' => 'id',

                                    'nullable' => true,
                                ),
                            ),
                        ));
                    }
                    break;
                case "phoneable":
                    // Check intersections between current class interfaces and interfaces to add dynamic relation
                    if (count(array_intersect(class_implements($metadata->getName()), $params['interfaces'])) > 0) {
                        $metadata->mapManyToMany(array(
                            'targetEntity' => Phone::class,
                            'fieldName' => 'phonenumbers',
                            'cascade' => array('persist'),
                            'joinTable' => array(
                                'name' => $prefix . strtolower($namingStrategy->classToTableName($metadata->getName())) . '_phonenumbers',
                                'joinColumns' => array(
                                    array(
                                        'name' => $namingStrategy->joinKeyColumnName($metadata->getName()),
                                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                                        'onDelete' => 'CASCADE',
                                        'onUpdate' => 'CASCADE',
                                    ),
                                ),
                                'inverseJoinColumns' => array(
                                    array(
                                        'name' => 'phonenumber_id',
                                        'referencedColumnName' => $namingStrategy->referenceColumnName(),
                                        'onDelete' => 'CASCADE',
                                        'onUpdate' => 'CASCADE',
                                    ),
                                )
                            )
                        ));
                    }
                    break;
            }
        }
    }
}