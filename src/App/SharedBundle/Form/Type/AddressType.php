<?php

namespace App\SharedBundle\Form\Type;

use App\CoreBundle\Form\Type\AddressTypeType;
use App\SharedBundle\Entity\Address;
use App\WarehouseBundle\Entity\WarehouseLocation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('streetname')
            ->add('housenumber')
            ->add('housenumberExtra')
            ->add('zipcode')
            ->add('city')
            ->add('country');

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            $form = $event->getForm();

            if($event->getForm()->getParent()){
                $parent = $event->getForm()->getParent()->getData();
                if (!$parent instanceof WarehouseLocation) {
                    $form->add('addressTypes', CollectionType::class,
                        array(
                            'entry_type' => AddressTypeType::class,
                            'allow_add' => true
                        )
                    );
                }
            }else{

            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Address::class
        ,
        ));

    }

}