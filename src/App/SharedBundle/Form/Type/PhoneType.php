<?php

namespace App\SharedBundle\Form\Type;

use App\SharedBundle\Entity\Phone;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhoneType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('number', 'text', array(
                "label"=>"form.phonenumber",
            ));
//        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
//            $parent = $event->getForm()->getParent()->getData();
//            $form = $event->getForm();
//
//            if (!$parent instanceof WarehouseLocation) {
//                $form->add('addressTypes', CollectionType::class,
//                    array(
//                        'entry_type' => AddressTypeType::class
//                    )
//                );
//            }
//        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Phone::class
        ,
        ));

    }

}