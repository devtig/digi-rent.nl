<?php

namespace App\SharedBundle\Entity\Interfaces;

use App\SharedBundle\Entity\Address;
use Doctrine\Common\Collections\ArrayCollection;

interface MultiAddressInterface
{
    public function getAddresses();
    public function setAddresses(ArrayCollection $addresses);
    public function addAddress(Address $address);
    public function removeAddress(Address $address);
}