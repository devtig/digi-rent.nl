<?php

namespace App\SharedBundle\Entity\Interfaces;

use App\SharedBundle\Entity\Address;

interface AddressInterface
{
    public function getAddress();
    public function setAddress(Address $address);
}