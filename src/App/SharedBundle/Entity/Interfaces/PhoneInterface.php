<?php

namespace App\SharedBundle\Entity\Interfaces;

use App\SharedBundle\Entity\Phone;
use Doctrine\Common\Collections\ArrayCollection;

interface PhoneInterface
{
    public function getPhones();
    public function setPhones(ArrayCollection $phones);
    public function addPhone(Phone $phone);
    public function removePhone(Phone $phone);
}