<?php

namespace App\SharedBundle\Entity;

use App\CoreBundle\Entity\AddressType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\CoreBundle\Entity\Traits;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="App\SharedBundle\Repository\AddressRepository")
 */
class Address
{
    use Traits\Identifiable;

    /**
     * @var array
     * @ORM\Column(name="address_types", type="array", nullable=false)
     */
    private $addressTypes;

    /**
     * @var int
     * @ORM\Column(name="country_id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="streetname", type="string", length=255, nullable=false)
     */
    private $streetname;

    /**
     * @var string
     *
     * @Assert\Regex(pattern="/^\d*$/", message="Invalid housenumber, put extra's in extra field")
     * @ORM\Column(name="housenumber", type="string", length=255, nullable=false)
     */
    private $housenumber;

    /**
     * @var string
     *
     * @ORM\Column(name="housenumber_extra", type="string", length=255, nullable=true)
     */
    private $housenumberExtra;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=255, nullable=false)
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;


    /**
     * Get addressTypes
     *
     * @return array AddressTypes
     */
    public function getAddressTypes()
    {
        //TODO uitwerken!
        return $this->addressTypes;
//
////        foreach (AddressType::$addressTypes as $addressType) {
//////            dump($addressType);
//////            die();
////            $addressTypes = array_merge($addressTypes, AddressType::$addressTypes );
////        }
//        $addressTypes = array_merge($addressTypes, AddressType::$addressTypes );
//
//        // we need to make sure to have at least one role
////        $addressTypes[] = static::ROLE_DEFAULT;
//
//        return array_unique($addressTypes);
    }

    /**
     * Set addressTypes
     *
     * @param array $addressTypes
     * @return Address
     */
    public function setAddressTypes(array $addressTypes)
    {
        $this->addressTypes = array();

        foreach ($addressTypes as $addressType) {
            $this->addAddressType($addressType);
        }

        return $this;
    }


    /**
     * Add addressType
     *
     * @param int $addressType
     * @return Address
     */
    public function addAddressType($addressType)
    {

        if($this->addressTypes == null){
            $this->addressTypes = array();
        }

        if (!in_array($addressType, $this->addressTypes, true)) {
            $this->addressTypes[] = $addressType;
        }

        return $this;
    }

    /**
     * Remove addressType
     *
     * @param  string $addressType
     * @return Address
     */
    public function removeAddressType($addressType)
    {
        if (false !== $key = array_search(strtoupper($addressType), $this->addressTypes, true)) {
            unset($this->addressTypes[$key]);
            $this->addressTypes = array_values($this->addressTypes);
        }

        return $this;
    }

    /**
     * Set country
     *
     * @param int $country
     * @return Address
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return int
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set streetname
     *
     * @param string $streetname
     * @return Address
     */
    public function setStreetname($streetname)
    {
        $this->streetname = $streetname;

        return $this;
    }

    /**
     * Get streetname
     *
     * @return string
     */
    public function getStreetname()
    {
        return $this->streetname;
    }

    /**
     * Set housenumber
     *
     * @param string $housenumber
     * @return Address
     */
    public function setHousenumber($housenumber)
    {
        $this->housenumber = $housenumber;

        return $this;
    }

    /**
     * Get housenumber
     *
     * @return string
     */
    public function getHousenumber()
    {
        return $this->housenumber;
    }

    /**
     * Set housenumberExtra
     *
     * @param string $housenumberExtra
     * @return Address
     */
    public function setHousenumberExtra($housenumberExtra)
    {
        $this->housenumberExtra = $housenumberExtra;

        return $this;
    }

    /**
     * Get housenumberExtra
     *
     * @return string
     */
    public function getHousenumberExtra()
    {
        return $this->housenumberExtra;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return Address
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }
}

