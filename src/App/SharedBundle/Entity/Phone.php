<?php

namespace App\SharedBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\PhoneType;

/**
 * Phone
 *
 * @ORM\Table(name="phone")
 * @ORM\Entity(repositoryClass="App\SharedBundle\Repository\PhoneRepository")
 */
class Phone
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="phone_type", type="integer", nullable=false)
     */
    private $phoneType;

    /**
     * @var string
     * @Assert\NotBlank(message="Please enter a phone number.", groups={"Registration", "Profile"})
     * @Assert\Regex(
     *      pattern="/^(?=^.{10,11}$)0\d*-?\d*$|\B@([a-z0-9](?:-?[a-z0-9]){5,31})/",
     *      match="false",
     *      message="Invalid phone number or skype username"
     * )
     * @ORM\Column(name="number", type="string", length=11, nullable=false)
     */
    private $number;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get addressType
     *
     * @return int AddressType
     */
    public function getPhoneType()
    {
        return $this->phoneType;
    }

    /**
     * Get addressTypeString
     *
     * @return string AddressType
     */
    public function getPhoneTypeString()
    {

        if (isset(PhoneType::$phoneTypes[$this->phoneType])) {
            return PhoneType::$phoneTypes[$this->phoneType];
        }
        return PhoneType::$phoneTypes[0];

    }

    /**
     * Set PhoneType
     *
     * @param integer $phoneType
     *
     * @return $this
     */
    public function setPhoneType($phoneType)
    {
        $this->phoneType = $phoneType;
        return $this;
    }

    /**
     * Set PhoneType string
     *
     * @param string $phoneType
     *
     * @return $this
     */
    public function SetPhoneTypeString($phoneType)
    {
        $phoneType = \array_search(\strtolower($phoneType), PhoneType::$phoneTypes);
        if ($phoneType > 0) {
            $this->setPhoneType($phoneType);
        }
        return $this;
    }



    /**
     * Set number
     *
     * @param string $number
     * @return Phone
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

}

