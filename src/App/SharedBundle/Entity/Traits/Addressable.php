<?php

namespace App\SharedBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use App\SharedBundle\Entity\Address;

trait Addressable {

    /**
     * @var Address
     * @ORM\ManyToOne(targetEntity="App\SharedBundle\Entity\Address", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    protected $address;

    /**
     * Get address
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address
     *
     * @param Address $address
     * @return $this
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;

        return $this;
    }

}