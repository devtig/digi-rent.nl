<?php

namespace App\SharedBundle\Entity\Traits;

use Shared\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use App\SharedBundle\Entity\Address;

trait CreatedByAble {

    /**
     * @var User
     * @ORM\Column(name="created_by", nullable=false)
     */
    protected $createdBy;

    /**
     * Get user
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return $this
     */
    public function setCreatedBy(User $user)
    {
        $this->createdBy = $user;

        return $this;
    }

}