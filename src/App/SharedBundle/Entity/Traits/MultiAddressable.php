<?php

namespace App\SharedBundle\Entity\Traits;

use App\SharedBundle\Entity\Address;
use Doctrine\Common\Collections\ArrayCollection;

trait MultiAddressable {

    /**
     * @var ArrayCollection
     */
    protected $addresses;

    public function getAddresses()
    {
        return $this->addresses;
    }

    public function setAddresses(ArrayCollection $addresses)
    {
        $this->addresses = $addresses;

        return $this;
    }

    public function addAddress(Address $address)
    {
        $this->addresses[] = $address;
    }

    public function removeAddress(Address $address){
        if ($this->addresses->contains($address)) {
            $this->addresses->removeElement($address);
        }
    }
}