<?php

namespace App\SharedBundle\Entity\Traits;

use App\SharedBundle\Entity\Address;
use App\SharedBundle\Entity\Phone;
use Doctrine\Common\Collections\ArrayCollection;

trait Phoneable {

    /**
     * @var ArrayCollection
     */
    protected $phones;

    public function getPhones()
    {
        return $this->phones;
    }

    public function setPhones(ArrayCollection $phones)
    {
        $this->phones = $phones;

        return $this;
    }

    public function addPhone(Phone $phone)
    {
        if (!$this->phones->contains($phone)) {
            $this->phones[] = $phone;
        }
    }

    public function removePhone(Address $phone){
        if ($this->phones->contains($phone)) {
            $this->phones->removeElement($phone);
        }
    }
}