<?php

namespace App\SharedBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\CategoryBundle\Entity\CategoryHasSpecification;
use App\ProductBundle\Entity\ProductSpecification;

class LoadSpecTemplates extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $specTypes = array(
            array(
                'name' => 'Wattage',
                'active' => true
            ),
            array(
                'name' => 'Kleur',
                'active' => true
            ),
            array(
                'name' => 'Bezoekersaantal',
                'active' => true
            ),
            array(
                'name' => 'RMS vermogen',
                'active' => true
            ),
            array(
                'name' => 'Gewicht',
                'active' => true
            ),
            array(
                'name' => 'Diameter woofer',
                'active' => true
            ),
            array(
                'name' => 'Aansluitingen',
                'active' => true
            ),
            array(
                'name' => 'SPL',
                'active' => true
            ),
            array(
                'name' => 'Frequenties',
                'active' => true
            ),
            array(
                'name' => 'Resolutie',
                'active' => true
            ),
            array(
                'name' => 'Ansi-lumen',
                'active' => true
            ),
        );

        $this->addSpecTypes($specTypes, $manager);
    }

    private function addSpecTypes($specTypes, ObjectManager $manager){

        foreach($specTypes as $specTypes_array){
            $specType = new ProductSpecification();
            $specType->setTitle($specTypes_array['name']);
            $specType->setActive($specTypes_array['active']);
            $manager->persist($specType);
        }
        $manager->flush();
        $this->specsTemplate($manager);
    }

    private function specsTemplate(ObjectManager $manager){

        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
        $categories = $em->getRepository('CategoryBundle:Category');
        $speakers = $categories->findOneBy(array('prefix' => 'SPE'));
        $subwoofers = $categories->findOneBy(array('prefix' => 'SUB'));
        $beamers = $categories->findOneBy(array('prefix' => 'BEAM'));

        $specs = $em->getRepository('ProductBundle:ProductSpecification');
        $SPL = $specs->findOneBy(array('title' => 'SPL'));
        $wattage = $specs->findOneBy(array('title' => 'Wattage'));
        $frequentie = $specs->findOneBy(array('title' => 'Frequenties'));
        $resolutie = $specs->findOneBy(array('title' => 'Resolutie'));
        $ansi = $specs->findOneBy(array('title' => 'Ansi-lumen'));

        $templates = array(
            array(
                'category' => $speakers,
                'specs' => array(
                    array(
                        'spec' => $wattage,
                        'position' => 1
                    ),
                    array(
                        'spec' => $SPL,
                        'position' => 2
                    ),
                    array(
                        'spec' => $frequentie,
                        'position' => 3
                    )
                )
            ),
            array(
                'category' => $subwoofers,
                'specs' => array(
                    array(
                        'spec' => $wattage,
                        'position' => 1
                    ),
                    array(
                        'spec' => $SPL,
                        'position' => 2
                    ),
                    array(
                        'spec' => $frequentie,
                        'position' => 3
                    )
                )
            ),
            array(
                'category' => $beamers,
                'specs' => array(
                    array(
                        'spec' => $resolutie,
                        'position' => 1
                    ),
                    array(
                        'spec' => $ansi,
                        'position' => 2
                    ),
                    array(
                        'spec' => $frequentie,
                        'position' => 3
                    )
                )
            ),
        );

        $this->addTemplates($templates, $manager);
    }

    private function addTemplates($templates, ObjectManager $manager){

        foreach($templates as $templates_array){
            foreach($templates_array['specs'] as $templateReference_array){
                $templateReference = new CategoryHasSpecification();
                $templateReference->setSpecification($templateReference_array['spec']);
                $templateReference->setPosition($templateReference_array['position']);
                $templateReference->setCategory($templates_array['category']);

//                $templates_array['category']->addSpec($templateReference);

                $manager->persist($templateReference);
            }
            $manager->persist($templates_array['category']);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}
