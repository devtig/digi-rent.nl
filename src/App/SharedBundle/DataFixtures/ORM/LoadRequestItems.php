<?php
//
//namespace App\ProjectBundle\DataFixtures\ORM;
//
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//use App\ProjectBundle\Entity\ProjectListItem;
//
//class LoadRequestItems extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
//{
//    private $container;
//
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    public function load(ObjectManager $manager)
//    {
//        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
//        $requestRepo = $em->getRepository('ProjectBundle:ProjectRequest');
//        $projectRepo = $em->getRepository('ProjectBundle:Project');
//        $productRepo = $em->getRepository('ProductBundle:Product');
//
//        $project = $projectRepo->findOneBy(array('title' => 'Summervibes'));
//        $project1 = $projectRepo->findOneBy(array('title' => 'Bruiloft'));
//
//        $request = $requestRepo->findOneBy(array('project' => $project));
//        $request2 = $requestRepo->findOneBy(array('project' => $project1));
//        $topkast = $productRepo->findOneBy(array('sku' => 'GLD-TOP-001'));
//        $subwoofer = $productRepo->findOneBy(array('sku' => 'GLD-SUB-001'));
//
//        $ledset = $productRepo->findOneBy(array('sku' => 'SET-GLD-001'));
//        $andereset = $productRepo->findOneBy(array('sku' => 'SET-GLD-002'));
//
//        $requests = array(
//            array(
//                'product' => $topkast,
//                'request' => $request,
//                'amount' => 2,
//                'price_ex' => $topkast->getPriceEx(),
//                'tax' => $topkast->getTax(),
//                'discount' => 0
//            ),
//            array(
//                'product' =>  $subwoofer,
//                'request' => $request,
//                'amount' => 2,
//                'price_ex' => $subwoofer->getPriceEx(),
//                'tax' => $subwoofer->getTax(),
//                'discount' => 0,
//            ),
//            array(
//                'product' => $ledset,
//                'request' => $request2,
//                'amount' => 2,
//                'price_ex' => $ledset->getPriceEx(),
//                'tax' => $ledset->getTax(),
//                'discount' => 0
//            ),
//            array(
//                'product' =>  $andereset,
//                'request' => $request2,
//                'amount' => 2,
//                'price_ex' => $andereset->getPriceEx(),
//                'tax' => $andereset->getTax(),
//                'discount' => 0,
//            )
//        );
//
//        $this->addRequests($requests, $manager);
//    }
//
//    private function addRequests($requests, ObjectManager $manager){
//
//        foreach($requests as $requests_array){
//            $request = new ProjectListItem();
//            $request->setRequest($requests_array['request']);
//            $request->setProduct($requests_array['product']);
//            $request->setAmount($requests_array['amount']);
//            $request->setPriceEx($requests_array['price_ex']);
//            $request->setTax($requests_array['tax']);
//            $request->setDiscount($requests_array['discount']);
//            $manager->persist($request);
//        }
//        $manager->flush();
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 11; // the order in which fixtures will be loaded
//    }
//}
