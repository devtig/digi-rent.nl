<?php

namespace App\SharedBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\ProductBundle\Entity\ProductsSet;

class LoadSets extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
//        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
//        $repo = $em->getRepository('ProductBundle:Product');
//        $geluidset = $repo->findOneBy(array('sku' => 'SET-GLD-001'));
//        $topkast = $repo->findOneBy(array('sku' => 'GLD-TOP-001'));
//        $subwoofer = $repo->findOneBy(array('sku' => 'GLD-SUB-001'));
//
//        $set = array(
//            array(
//                'amount' => 4,
//                'set' => $geluidset,
//                'product1' => $topkast,
//                'product2' => $subwoofer
//            )
//        );
//
//        $this->addSet($set, $manager);
    }

    private function addSet($products, ObjectManager $manager){

        foreach($products as $product_array){
            $product = new ProductsSet();
            $product->setAmount($product_array['amount']);
            $product->setSet($product_array['set']);
            $product->addProduct($product_array['product1']);
            $product->addProduct($product_array['product2']);
            $manager->persist($product);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6; // the order in which fixtures will be loaded
    }
}
