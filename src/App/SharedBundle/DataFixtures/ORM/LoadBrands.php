<?php

namespace App\SharedBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\ProductBundle\Entity\Brand;

class LoadBrands extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $brands = array(
            array(
                'name' => 'dB Technologies',
            ),
            array(
                'name' => 'Showtec',
            ),
            array(
                'name' => 'Laserworld',
            ),
            array(
                'name' => 'Shure',
            ),
            array(
                'name' => 'Sennheiser',
            ),
            array(
                'name' => 'Xilica',
            ),
            array(
                'name' => 'American DJ',
            ),
            array(
                'name' => 'DBX',
            ),
            array(
                'name' => 'Sunlite',
            ),
            array(
                'name' => 'Pioneer',
            ),
            array(
                'name' => 'Behringer',
            ),
            array(
                'name' => 'Martin',
            ),
            array(
                'name' => 'Uniq',
            ),
            array(
                'name' => 'Magix FX',
            ),
            array(
                'name' => 'Prolyte',
            ),
            array(
                'name' => 'Yamaha',
            )
        );

        $this->addBrands($brands, $manager);
    }

    private function addBrands($brands, ObjectManager $manager)
    {
        foreach($brands as $brand_array){
            $brand = new Brand();
            $brand->setName($brand_array['name']);
            $manager->persist($brand);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }
}
