<?php
//
//namespace App\ProjectBundle\DataFixtures\ORM;
//
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//use App\ProjectBundle\Entity\ProjectQuotation;
//
//class LoadQuotations extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
//{
//    private $container;
//
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    public function load(ObjectManager $manager)
//    {
//        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
//        $repo = $em->getRepository('ProjectBundle:Project');
//        $divionsRepo = $em->getRepository('SettingsBundle:Division');
//        $uniqRental = $divionsRepo->findOneBy(array('title' => 'uniq-rental'));
//        $summervibes = $repo->findOneBy(array('title' => 'Summervibes'));
//        $bruiloft = $repo->findOneBy(array('title' => 'Bruiloft'));
//
//        $quotations = array(
//            array(
//                'project' => $bruiloft,
//                'number' => 'UR-2015-001',
//                'remark' => 'Geen korting voor jou chefke',
//                'discount' => 0,
//                'status' => 'Nog niet bevestigd',
//                'acceptdate' => new \DateTime('NOW'),
//                'division' => $uniqRental,
//                'pdf_location' => '/uploads/userid/check_if_okay/quotation.pdf?12412547'
//            ),
//            array(
//                'project' => $summervibes,
//                'number' => 'UR-2015-002',
//                'remark' => 'Speciale korting omdat jij het bent timmetje',
//                'discount' => 15,
//                'status' => 'Nog niet bevestigd',
//                'acceptdate' => new \DateTime('NOW'),
//                'division' => $uniqRental,
//                'pdf_location' => '/uploads/userid/check_if_okay/quotation.pdf?12412547'
//            )
//        );
//
//        $this->addQuotations($quotations, $manager);
//    }
//
//    private function addQuotations($quotations, ObjectManager $manager){
//
//        foreach($quotations as $quotations_array){
//            $quotation = new ProjectQuotation();
//            $quotation->setProject($quotations_array['project']);
//            $quotation->setNumber($quotations_array['number']);
//            $quotation->setRemark($quotations_array['remark']);
//            $quotation->setDiscount($quotations_array['discount']);
//            $quotation->setStatus($quotations_array['status']);
//            $quotation->setAcceptDate($quotations_array['acceptdate']);
//            $quotation->setDivision($quotations_array['division']);
//            $quotation->setPdfLocation($quotations_array['pdf_location']);
//            $manager->persist($quotation);
//        }
//        $manager->flush();
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 12; // the order in which fixtures will be loaded
//    }
//}
