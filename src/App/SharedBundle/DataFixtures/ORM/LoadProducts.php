<?php

namespace App\SharedBundle\DataFixtures\ORM;

use App\CoreBundle\Entity\Tax;
use Shared\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\ProductBundle\Entity\Product;
use App\ProductBundle\Entity\ProductActivity;
use App\ProductBundle\Entity\ProductPhysical;
use App\ProductBundle\Entity\ProductTag;

class LoadProducts extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
//        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
//        $repo = $em->getRepository('CategoryBundle:Category');
//        $brands = $em->getRepository('ProductBundle:Brand');
//        $topkasten = $repo->findOneBy(array('name' => 'Topkasten'));
//        $subwoofers = $repo->findOneBy(array('name' => 'Subwoofers'));
//        $ledspots = $repo->findOneBy(array('name' => 'Led spots'));
//        $sets = $repo->findOneBy(array('name' => 'Geluidsets'));
//
//        $dbTechnologies = $brands->findOneBy(array('name' => 'dB Technologies'));
//        $showtec = $brands->findOneBy(array('name' => 'Showtec'));
//
//        $products = array(
//            array(
//                'category' => $topkasten,
//                'sku' => 'GLD-TOP-001',
//                'name' => 'dB Technologies DVX 15 HP topkast 1000watt',
//                'subtitle' => 'De DVX-serie bestaat uit een reeks luidsprekers en monitoren met ingebouwde digitale versterkers.',
//                'descr_short' => 'In de luidsprekers zitten aparte versterkers voor de hoge en lage frequenties zodat er een mooi helder en vol geluid wordt geproduceerd.',
//                'descr_full' => 'Met een geluidsdruk van 132 dB en 750 Watt aan vermogen is de D15 zeer geschikt voor grotere ruimtes. Hij is uitgerust met een 1.4 inch driver met 2.5 inch spreekspoel en is ontworpen om de grootste vermogens aan te kunnen. De 15 inch woofer zorgt voor een krachtige basweergave, zodat een extra subwoofer niet per se nodig is. De behuizing van het multifunctionele ontwerp heeft een schuin paneel voor monitoring, bevestigingspunten en zelfs een speciale monitorsetup, aangestuurd door de ingebouwde DSP.',
//                'price_ex' => 30.0,
//                'brand' => $dbTechnologies,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'ZGiqV_Vmpys',
//                'archived' => true,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Shoarma bakken", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//            array(
//                'category' => $topkasten,
//                'sku' => 'GLD-TOP-002',
//                'name' => 'dB Technologies DVX 12 HP topkast 1000watt',
//                'subtitle' => 'De DVX-serie bestaat uit een reeks luidsprekers en monitoren met ingebouwde digitale versterkers.',
//                'descr_short' => 'In de luidsprekers zitten aparte versterkers voor de hoge en lage frequenties zodat er een mooi helder en vol geluid wordt geproduceerd.',
//                'descr_full' => 'Met een geluidsdruk van 132 dB en 750 Watt aan vermogen is de D12 zeer geschikt voor grotere ruimtes. Hij is uitgerust met een 1.4 inch driver met 2.5 inch spreekspoel en is ontworpen om de grootste vermogens aan te kunnen. De 12 inch woofer zorgt voor een krachtige basweergave, zodat een extra subwoofer niet per se nodig is. De behuizing van het multifunctionele ontwerp heeft een schuin paneel voor monitoring, bevestigingspunten en zelfs een speciale monitorsetup, aangestuurd door de ingebouwde DSP.',
//                'price_ex' => 30.0,
//                'brand' => $dbTechnologies,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'ZGiqV_Vmpys',
//                'archived' => true,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Shoarma bakken", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//            array(
//                'category' => $subwoofers,
//                'sku' => 'GLD-SUB-001',
//                'name' => 'dB Technologies DVA S10 subwoofer 1000watt',
//                'subtitle' => 'De DVA-serie bestaat uit een reeks luidsprekers en subwoofers met ingebouwde digitale versterkers.',
//                'descr_short' => 'De DVA S10DP is een krachtige subwoofer met een forse 18-inch woofer welke door een 1000 Watt versterker wordt aangedreven.',
//                'descr_full' => 'Met een geluidsdruk van 132 dB en 750 Watt aan vermogen is de D15 zeer geschikt voor grotere ruimtes. Hij is uitgerust met een 1.4 inch driver met 2.5 inch spreekspoel en is ontworpen om de grootste vermogens aan te kunnen. De 15 inch woofer zorgt voor een krachtige basweergave, zodat een extra subwoofer niet per se nodig is. De behuizing van het multifunctionele ontwerp heeft een schuin paneel voor monitoring, bevestigingspunten en zelfs een speciale monitorsetup, aangestuurd door de ingebouwde DSP.',
//                'price_ex' => 40.0,
//                'brand' => $dbTechnologies,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'ZGiqV_Vmpys',
//                'archived' => false,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Shoarma bakken", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//            array(
//                'category' => $subwoofers,
//                'sku' => 'GLD-SUB-002',
//                'name' => 'dB Technologies DVA S30 subwoofer 3000watt',
//                'subtitle' => 'De DVA-serie bestaat uit een reeks luidsprekers en subwoofers met ingebouwde digitale versterkers.',
//                'descr_short' => 'De DVA S30DP is een krachtige subwoofer met een forse dubbele 18-inch woofer welke door een 3000 Watt versterker wordt aangedreven.',
//                'descr_full' => 'Met een geluidsdruk van 132 dB en 750 Watt aan vermogen is de D15 zeer geschikt voor grotere ruimtes. Hij is uitgerust met een 1.4 inch driver met 2.5 inch spreekspoel en is ontworpen om de grootste vermogens aan te kunnen. De 15 inch woofer zorgt voor een krachtige basweergave, zodat een extra subwoofer niet per se nodig is. De behuizing van het multifunctionele ontwerp heeft een schuin paneel voor monitoring, bevestigingspunten en zelfs een speciale monitorsetup, aangestuurd door de ingebouwde DSP.',
//                'price_ex' => 75.0,
//                'brand' => $dbTechnologies,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'ZGiqV_Vmpys',
//                'archived' => false,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Dikke speaker", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//            array(
//                'category' => $ledspots,
//                'sku' => 'LI-SPOT-LED-001',
//                'name' => 'Showtec Compact Par 7 Tri LED spot',
//                'subtitle' => 'Uplighting toepassingen, decoratie of zelfs discotheekverlichting.',
//                'descr_short' => 'De Compact Par 7 is veelzijdig in te zetten. Dankzij Tri-LED techniek wordt een nette kleurenmenging gerealiseerd, waardoor warme kleuren gemaakt kunnen worden..',
//                'descr_full' => 'Het ontwerp van de spot kan lekker klein blijven dankzij de 7 Tri-LED modules. Hierdoor is de spot eenvoudig weg te werken of te transporteren. De lichtbron bestaat uit zeven Tri-LED modules, waarbij rood, groen en blauw te mengen zijn. Hierdoor kunt u talloze kleurencombinaties maken.',
//                'price_ex' => 10.0,
//                'brand' => $showtec,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'Zu3i_x9KKoU',
//                'archived' => false,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Dikke speaker", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//            array(
//                'category' => $ledspots,
//                'sku' => 'LI-SPOT-LED-002',
//                'name' => 'Showtec LED Par 56',
//                'subtitle' => 'De nieuwe generatie par LED lampen zijn nu nog zuiniger en een stuk feller dan voorheen.',
//                'descr_short' => 'U wordt verrast door het gemak van RGB kleurenmixen en de kostenbesparing qua energie.',
//                'descr_full' => 'Deze par 56 is zeskanaals DMX gestuurd en bevat 153 RGB LED\'s (51 rood, 51 groen, 51 blauw). Daarnaast bevat de par een ingebouwde microfoon en kan daarom muziekgestuurd worden ingezet. Door de meerdere preset instellingen kunt u deze lampen inzetten zonder dat u een DMX signaal nodig heeft.',
//                'price_ex' => 7.0,
//                'brand' => $showtec,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'ZGiqV_Vmpys',
//                'archived' => false,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Dikke speaker", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//            array(
//                'category' => $sets,
//                'sku' => 'SET-GLD-001',
//                'name' => 'Geluid setje 1x Top + S10',
//                'subtitle' => 'Deze set is echt niet meer normaal.',
//                'descr_short' => 'Goed voor feestjes in de huiskamer enzo, maar ook in de feest zaaltjes enzo.',
//                'descr_full' => 'Deze set is echt niet meer normaal, dat voor maar een paar tientjes. Dikke subwoofer en topkasten voor je fissa. Kabels enzo zitten er ook bij. Kijk bij Uniqness om te zien wat er nog meer te belevne valt hiero.',
//                'price_ex' => 100.0,
//                'brand' => NULL,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'ZGiqV_Vmpys',
//                'archived' => false,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Dikke speaker", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//            array(
//                'category' => $sets,
//                'sku' => 'SET-GLD-002',
//                'name' => '2x DVX 15 HP Topkasten met 2x DVA S30 Subwoofer',
//                'subtitle' => 'Deze set is echt niet meer normaal.',
//                'descr_short' => 'Goed voor feestjes in de huiskamer enzo, maar ook in de feest zaaltjes enzo.',
//                'descr_full' => 'Deze set is echt niet meer normaal, dat voor maar een paar tientjes. Dikke subwoofer en topkasten voor je fissa. Kabels enzo zitten er ook bij. Kijk bij Uniqness om te zien wat er nog meer te belevne valt hiero.',
//                'price_ex' => 150.0,
//                'brand' => NULL,
//                'tax' => Tax::NL_TAX_HIGH,
//                'discount' => NULL,
//                'video_url' => 'ZGiqV_Vmpys',
//                'archived' => false,
//                'onsale' => false,
//                'thumbnail' => "/src/ergens/is/een/plaatjetevinden.jpg",
//                'uniqness' => '{"uniq": ["Dikke speaker", "Gekke shit","Niet normaal", "Biatch"]}',
//            ),
//        );
//
//        $this->addProducts($products, $manager);
//        $this->addComboSuggestionsAndAlternativesToProduct();
////        $PhysicalProductActivities = $this->addProductActivities();
////        $this->addPhysicalProducts($PhysicalProductActivities);
//        $this->addTags();
    }

    private function addProducts($products, ObjectManager $manager)
    {

        foreach($products as $product_array){
            $user = new User();
            $user->setFirstname('System');

            $product = new Product();
            $product->setCreatedBy($user);
            $product->setCategory($product_array['category']);
            $product->setSku($product_array['sku']);
            $product->setName($product_array['name']);
            $product->setSubtitle($product_array['subtitle']);
            if($product_array['brand'] != null){
                $product->setBrand($product_array['brand']);
            }
            $product->setDescrShort($product_array['descr_short']);
            $product->setDescription($product_array['descr_full']);
            $product->setPriceEx($product_array['price_ex']);
            $product->setTax($product_array['tax']);
            $product->setDiscount($product_array['discount']);
            $product->setVideoUrl($product_array['video_url']);
            $product->setArchived($product_array['archived']);
            $product->setOnsale($product_array['onsale']);
            $product->setImageName($product_array['thumbnail']);
            $product->setUniqness($product_array['uniqness']);
            $product->setWarehouseLocation($manager->getRepository('WarehouseBundle:WarehouseLocation')->findOneBy(array('name' => "Hoofd loods")));
            $manager->persist($product);
        }
        $manager->flush();
    }

    private function addComboSuggestionsAndAlternativesToProduct()
    {
        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
        $products = $em->getRepository('ProductBundle:Product')->findAll();

        $count = count($products);

        for ($i = 0; $i < $count - 1; $i++){
            $products[$i]->addComboSuggestion($products[rand(0, $count - 1)]);
            $products[$i]->addAlternative($products[rand(0, $count - 1)]);
            $em->persist($products[$i]);
        }

        $em->flush();
    }

    private function addPhysicalProducts($PhysicalProductActivities)
    {
        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
        $products = $em->getRepository('ProductBundle:Product')->findAll();
        $count = count($products);

        for ($i = 0; $i < $count - 1; $i++){
            $stock = 4;

            $randomLocation = rand(1, 99);

            for ($j = 0; $j < $stock; $j++){
                $physicalProduct = new ProductPhysical();
                $physicalProduct->setProduct($products[$i]);
                $physicalProduct->setLocation("2.".$randomLocation);
                $physicalProduct->setBarcode($products[$i]->getSku().'-'.$j);
                $physicalProduct->setArchived(false);

                if($j < count($PhysicalProductActivities)){
                    $activityCount = $j;
                    $em->persist($PhysicalProductActivities[$activityCount]);
                }

                $physicalProduct->addProductActivity($PhysicalProductActivities[$activityCount]);
                $products[$i]->addPhysicalProduct($physicalProduct);
                $em->persist($physicalProduct);
            }
            $em->persist($products[$i]);
        }

        $em->flush();
    }

    private function addTags()
    {
        $em = $this->container->get('doctrine')->getEntityManager('dynamic');

        $tags = ['Speaker', 'Geluid', 'Speakerkast', 'Subwoofer', 'Topkast', 'DB Technologies', 'Showtec', 'DVX', 'LED'];

        foreach ($tags as $tagName){
            $tag = new ProductTag();
            $tag->setName($tagName);

            $em->persist($tag);
        }

        $em->flush();

        $tags = $em->getRepository('ProductBundle:ProductTag')->findAll();
        $products = $em->getRepository('ProductBundle:Product')->findAll();

        $productCount = count($products);
        $tagCount = count($tags);

        for ($i = 0; $i < $productCount - 1; $i++){
            $products[$i]->addTag($tags[rand(0, $tagCount - 1)]);
            $products[$i]->addTag($tags[rand(0, $tagCount - 1)]);
            $products[$i]->addTag($tags[rand(0, $tagCount - 1)]);
            $products[$i]->addTag($tags[rand(0, $tagCount - 1)]);
            $products[$i]->addTag($tags[rand(0, $tagCount - 1)]);
            $products[$i]->addTag($tags[rand(0, $tagCount - 1)]);
            $products[$i]->addTag($tags[rand(0, $tagCount - 1)]);
            $em->persist($products[$i]);
        }

        $em->flush();
    }

    private function addProductActivities(){

        $userManager = $this->container->get('fos_user.user_manager');
        $peter = $userManager->findUserBy(array('email' => 'peter@uniq-group.nl'))->getId();
        $paul = $userManager->findUserBy(array('email' => 'paul@uniq-group.nl'))->getId();

        if($peter == null || $paul == null){
            echo "Peter of paul bestaat niet in de database";
            die();
        }

        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
        $productActivityState = $em->getRepository('ProductBundle:ProductActivityState');
        $project = $em->getRepository('ProductBundle:ProductActivityState');

        $verhuuruit = $productActivityState->findOneBy(array('name' => 'Voorraad uitgaand (verhuur)'));
        $verhuurin = $productActivityState->findOneBy(array('name' => 'Voorraad inkomend (verhuur)'));
        $onderhouduit = $productActivityState->findOneBy(array('name' => 'Onderhoud (uit voorraad)'));

        $summervibes = $project->findOneBy(array('name' => 'Summervibes'));
        $bruiloft = $project->findOneBy(array('name' => 'Bruiloft'));

        $productActivities = array(
            array(

                'name' => "Verhuur klusje",
                'project' => $summervibes,
                'activity_state' => $verhuuruit,
                'created' => new \DateTime('NOW'),
                'modified' => new \DateTime('NOW'),
                'stock_leaving' => (new \DateTime('now'))->modify('-2 days'),
                'stock_leaving_state' => "GloedjeNieuw",
                'stock_leaving_by' => $peter,
                'stock_receiving_expected' => (new \DateTime('now'))->modify('+2 days'),
                'stock_receiving' => new \DateTime('NOW'),
                'stock_receiving_state' => "Helemaal naar de klote",
                'stock_receiving_by' => $paul,
                'description' => "Product kwam bij terugkomst helemaal verzopen terug.",
            ),
            array(
                'name' => "Onderhoud",
                'project' => NULL,
                'activity_state' => $onderhouduit,
                'created' => new \DateTime('NOW'),
                'modified' => new \DateTime('NOW'),
                'stock_leaving' => new \DateTime('NOW'),
                'stock_leaving_state' => "Kapoet",
                'stock_leaving_by' => $paul,
                'stock_receiving_expected' => (new \DateTime('now'))->modify('+3 days'),
                'stock_receiving' => NULL,
                'stock_receiving_state' => NULL,
                'stock_receiving_by' => NULL,
                'description' => "Triac moet vervangen worden.",
            ),
            array(
                'name' => "Verhuur klus",
                'project' => $bruiloft,
                'activity_state' => $verhuurin,
                'created' => new \DateTime('NOW'),
                'modified' => new \DateTime('NOW'),
                'stock_leaving' => new \DateTime('NOW'),
                'stock_leaving_state' => "Werkend",
                'stock_leaving_by' => $peter,
                'stock_receiving_expected' => (new \DateTime('now'))->modify('+5 days'),
                'stock_receiving' => (new \DateTime('now'))->modify('+6 days'),
                'stock_receiving_state' => "Werkend",
                'stock_receiving_by' => $paul,
                'description' => "Terug op voorraad",
            ),
            array(
                'name' => "Verhuur klus2",
                'project' => $bruiloft,
                'activity_state' => $verhuurin,
                'created' => new \DateTime('NOW'),
                'modified' => new \DateTime('NOW'),
                'stock_leaving' => (new \DateTime('now'))->modify('+5 days'),
                'stock_leaving_state' => "Werkend",
                'stock_leaving_by' => $peter,
                'stock_receiving_expected' => (new \DateTime('now'))->modify('+10 days'),
                'stock_receiving' => (new \DateTime('now'))->modify('+10 days'),
                'stock_receiving_state' => "Werkend",
                'stock_receiving_by' => $paul,
                'description' => "Terug op voorraad",
            ),
        );

        $ProductObjectArray =  $this->addPhysicalProductsActivities($productActivities);

        return $ProductObjectArray;
    }

    private function addPhysicalProductsActivities($physicalActArray)
    {
        $productActivitiesArray = array();

        foreach($physicalActArray as $physicalAct){

            $physicalActivity = new ProductActivity();
            $physicalActivity->setTitle($physicalAct['name']);
            $physicalActivity->setProject($physicalAct['project']);
            $physicalActivity->setActivityState($physicalAct['activity_state']);
            $physicalActivity->setCreated($physicalAct['created']);
            $physicalActivity->setModified($physicalAct['modified']);
            $physicalActivity->setStockLeaving($physicalAct['stock_leaving']);
            $physicalActivity->setStockLeavingState($physicalAct['stock_leaving_state']);
            $physicalActivity->setStockLeavingBy($physicalAct['stock_leaving_by']);
            $physicalActivity->setStockReceivingExpected($physicalAct['stock_receiving_expected']);
            $physicalActivity->setStockReceiving($physicalAct['stock_receiving']);
            $physicalActivity->setStockReceivingState($physicalAct['stock_receiving_state']);
            $physicalActivity->setStockReceivingBy($physicalAct['stock_receiving_by']);
            $physicalActivity->setDescription($physicalAct['description']);

            $productActivitiesArray[] = $physicalActivity;
        }
        return $productActivitiesArray;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }
}
