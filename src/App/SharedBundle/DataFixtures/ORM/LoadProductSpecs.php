<?php

namespace App\SharedBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\ProductBundle\Entity\ProductHasSpecification;
use App\ProductBundle\Entity\ProductSpecification;

class LoadProductSpecs extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
//        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
//        $repo = $em->getRepository('ProductBundle:Product');
//        $product = $repo->findOneBy(array('sku' => 'GLD-TOP-001'));
//
//        $repo = $em->getRepository('CategoryBundle:Category');
//        $productCategory = $repo->findOneBy(array('prefix' => 'SPE'));
//
//
//        $specs = $productCategory->getSpecs();
//
//        foreach($specs as $spec){
//            $productSpec = new ProductHasSpecification();
//            $productSpec->setProduct($product);
//            $productSpec->setSpecType($spec->getSpecification());
//            $productSpec->setPosition($spec->getPosition());
//            $productSpec->setValue('1000');
//
//            $em->persist($productSpec);
//        }
//
//        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9; // the order in which fixtures will be loaded
    }
}
