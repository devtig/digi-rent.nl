<?php
//
//namespace App\ProjectBundle\DataFixtures\ORM;
//
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//use App\ProjectBundle\Entity\ProjectListItem;
//
//class LoadQuotationItems extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
//{
//    private $container;
//
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    public function load(ObjectManager $manager)
//    {
//        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
//
//        $projectRepo = $em->getRepository('ProjectBundle:Project');
//        $quotationRepo = $em->getRepository('ProjectBundle:ProjectQuotation');
//        $requestRepo = $em->getRepository('ProjectBundle:ProjectRequest');
//
//        $productRepo = $em->getRepository('ProductBundle:Product');
//        $ledspot1 = $productRepo->findOneBy(array('sku' => 'LI-SPOT-LED-001'));
//        $ledspot2 = $productRepo->findOneBy(array('sku' => 'LI-SPOT-LED-002'));
//
//        // Projects
//        $bruiloft = $projectRepo->findOneBy(array('title' => 'Bruiloft'));
//        $summervibes = $projectRepo->findOneBy(array('title' => 'Summervibes'));
//
//        $bruiloftQuotation = $quotationRepo->findOneBy(array('project' => $bruiloft));
//        $summervibesQuotation = $quotationRepo->findOneBy(array('project' => $summervibes));
//
//        $bruiloftRequest = $requestRepo->findOneBy(array('project' => $bruiloft));
//        $summervibesRequest = $requestRepo->findOneBy(array('project' => $summervibes));
//
//        $bruiloftListItems = $bruiloftRequest->getProjectListItems();
//        $summervibestListItems = $summervibesRequest->getProjectListItems();
//
//        foreach($bruiloftListItems as $projectListItem){
//
//            $projectListItem->setQuotation($bruiloftQuotation);
//        }
//
//        foreach($summervibestListItems as $projectListItem){
//            $projectListItem->setQuotation($summervibesQuotation);
//        }
//
//
//        $extraProductQuotation = array(
//            array(
//                'product' =>  $ledspot1,
//                'quotation' => $bruiloftQuotation,
//                'amount' => 4,
//                'price_ex' => $ledspot1->getPriceEx(),
//                'tax' => $ledspot1->getTax(),
//                'discount' => 5,
//            ),
//            array(
//                'product' =>  $ledspot1,
//                'quotation' => $summervibesQuotation,
//                'amount' => 10,
//                'price_ex' => $ledspot1->getPriceEx(),
//                'tax' => $ledspot1->getTax(),
//                'discount' => 10,
//            ),
//            array(
//                'product' =>  $ledspot2,
//                'quotation' => $summervibesQuotation,
//                'amount' => 20,
//                'price_ex' => $ledspot2->getPriceEx(),
//                'tax' => $ledspot2->getTax(),
//                'discount' => 10,
//            )
//        );
//
//        $this->addRequests($extraProductQuotation, $manager);
//    }
//
//    private function addRequests($requests, ObjectManager $manager){
//        foreach($requests as $requests_array){
//            $request = new ProjectListItem();
//            $request->setQuotation($requests_array['quotation']);
//            $request->setProduct($requests_array['product']);
//            $request->setAmount($requests_array['amount']);
//            $request->setPriceEx($requests_array['price_ex']);
//            $request->setTax($requests_array['tax']);
//            $request->setDiscount($requests_array['discount']);
//            $manager->persist($request);
//        }
//        $manager->flush();
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 12; // the order in which fixtures will be loaded
//    }
//}
