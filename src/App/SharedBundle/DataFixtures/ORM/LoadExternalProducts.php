<?php
//
//namespace App\SharedBundle\DataFixtures\ORM;
//
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//use App\ProductBundle\Entity\ProductExternal;
//
//class LoadExternalProducts extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
//
//    private $container;
//
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    public function load(ObjectManager $manager)
//    {
//        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
//        $product = $em->getRepository('ProductBundle:Product');
////        $supplier = $em->getRepository('RelationBundle:Supplier');
//        $product1 = $product->findOneBy(array('sku' => 'SET-GLD-001'));
//        $product2 = $product->findOneBy(array('sku' => 'GLD-SUB-001'));
//        $ngs = $supplier->findOneBy(array('name' => 'NGS Rental'));
//        $tp = $supplier->findOneBy(array('name' => 'Transport Partners'));
//
//        $activityStateArray = array(
//            array(
//                'supplier' => $ngs,
//                'product' => $product1,
//                'supplier_sku' => "Dik product jonge",
//                'supplier_price_ex' => 12.0,
//                'supplier_stock' => 12,
//            ),
//            array(
//                'supplier' => $ngs,
//                'product' => $product2,
//                'supplier_sku' => "Een ander product jonge",
//                'supplier_price_ex' => 152.0,
//                'supplier_stock' => 2,
//            ),
//            array(
//                'supplier' => $tp,
//                'product' => $product1,
//                'supplier_sku' => "Tp heeft ook spul hoor",
//                'supplier_price_ex' => 212.0,
//                'supplier_stock' => 2,
//            ),
//            array(
//                'supplier' => $tp,
//                'product' => $product2,
//                'supplier_sku' => "TP heeft alles bots! Veel goeeiekoper",
//                'supplier_price_ex' => 122.0,
//                'supplier_stock' => 112,
//            ),
//        );
////        $this->addActivityTypes($activityStateArray);
//    }
//
//    private function addActivityTypes($activityTypesArray)
//    {
//        $em = $this->container->get('doctrine')->getEntityManager('uniq_rental');
//
//        foreach($activityTypesArray as $actvityStateObject){
//            $productExternal = new ProductExternal();
//            $productExternal->setSupplier($actvityStateObject['supplier']);
//            $productExternal->setProduct($actvityStateObject['product']);
//            $productExternal->setSupplierSku($actvityStateObject['supplier_sku']);
//            $productExternal->setSupplierStock($actvityStateObject['supplier_stock']);
//            $productExternal->setSupplierPriceEx($actvityStateObject['supplier_price_ex']);
//            $productExternal->setCreated($actvityStateObject['created']);
//            $productExternal->setModified($actvityStateObject['modified']);
//            $productExternal->setArchived($actvityStateObject['archived']);
//
//            $em->persist($productExternal);
//        }
//
//        $em->flush();
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 8; // the order in which fixtures will be loaded
//    }
//
//}