<?php

namespace App\SharedBundle\DataFixtures\ORM;

use App\CategoryBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCategories extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $categories = array(
            array(
                'prefix' => 'GLD',
                'name' => 'Geluid',
                'metaDescr' => 'geluid moet je horen',
                'metaKey' => 'geluid, speakers, boxen',
                'subtitle' => 'Geluidje',
                'descr' => 'Geluidje met wat meer tekst',
                'image' => '/location/image.png',
                'imageAlt' => 'Plaatje van geluid',
                'contains_product' => false,
                'children' => array(
                    array(
                        'prefix' => 'SPE',
                        'name' => 'Speakers en monitoren',
                        'metaDescr' => 'Speakers moet je horen',
                        'metaKey' => 'speakers, boxen',
                        'subtitle' => 'Speakers',
                        'descr' => 'Speakers met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van Speakers',
                        'contains_product' => false,
                        
                        'children' => array(
                            array(
                                'prefix' => 'TOP',
                                'name' => 'Topkasten',
                                'metaDescr' => 'Topkasten moet je horen',
                                'metaKey' => 'Topkasten, boxen',
                                'subtitle' => 'Topkasten',
                                'descr' => 'Topkasten met wat meer tekst',
                                'image' => '/location/image.png',
                                'imageAlt' => 'Plaatje van Topkasten',
                                'contains_product' => true,
                            ),
                            array(
                                'prefix' => 'MON',
                                'name' => 'Monitoren',
                                'metaDescr' => 'monitoren moet je voelen',
                                'metaKey' => 'monitoren, Bass, boxen',
                                'subtitle' => 'monitoren',
                                'descr' => 'monitoren met wat meer tekst',
                                'image' => '/location/image.png',
                                'imageAlt' => 'Plaatje van monitoren',
                                'contains_product' => true,
                            ),
                        )
                    ),
                    array(
                        'prefix' => 'SUB',
                        'name' => 'Subwoofers',
                        'metaDescr' => 'Bass moet je voelen',
                        'metaKey' => 'Sub, Bass, boxen',
                        'subtitle' => 'Bass',
                        'descr' => 'Bass met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van Subwoofers',
                        'contains_product' => true,
                    ),

                )
            ),
            array(
                'prefix' => 'LICH',
                'name' => 'Licht',
                'metaDescr' => 'licht moet je zien',
                'metaKey' => 'licht, lampen, boxen',
                'subtitle' => 'Lampje',
                'descr' => 'Licht met wat meer tekst',
                'image' => '/location/image.png',
                'imageAlt' => 'Plaatje van licht',
                'contains_product' => false,
                'children' => array(
                    array(
                        'prefix' => 'MOV',
                        'name' => 'Movingheads',
                        'metaDescr' => 'movingheads moet je zien',
                        'metaKey' => 'movingheads, lampen, bliepbliep',
                        'subtitle' => 'movingheads',
                        'descr' => 'movingheads met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van movingheads',
                        'contains_product' => true,
                    ),
                    array(
                        'prefix' => 'SPOT',
                        'name' => 'Spots',
                        'metaDescr' => 'Spots moet je zien',
                        'metaKey' => 'Spots, lampen, klinkklink',
                        'subtitle' => 'Spots',
                        'descr' => 'Spots met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van Spots',
                        'contains_product' => false,
                        'children' => array(
                            array(
                                'prefix' => 'VOLG',
                                'name' => 'Volgspots',
                                'metaDescr' => 'volgspots moet je zien',
                                'metaKey' => 'volgspots, lampen, bliepbliep',
                                'subtitle' => 'volgspots',
                                'descr' => 'volgspots met wat meer tekst',
                                'image' => '/location/image.png',
                                'imageAlt' => 'Plaatje van volgspots',
                                'contains_product' => true,
                            ),
                            array(
                                'prefix' => 'LDSP',
                                'name' => 'Led spots',
                                'metaDescr' => 'Led Spots moet je zien',
                                'metaKey' => 'led Spots, lampen, klinkklink',
                                'subtitle' => 'led Spots',
                                'descr' => 'led Spots met wat meer tekst',
                                'image' => '/location/image.png',
                                'imageAlt' => 'Plaatje van led Spots',
                                'contains_product' => true,
                            ),
                            array(
                                'prefix' => 'FRES',
                                'name' => 'Fresnel spots',
                                'metaDescr' => 'fresnel moet je zien',
                                'metaKey' => 'fresnel, lampen, boxen',
                                'subtitle' => 'fresnel',
                                'descr' => 'fresnel met wat meer tekst',
                                'image' => '/location/image.png',
                                'imageAlt' => 'Plaatje van een fresnel',
                                'contains_product' => true,
                            ),
                        )
                    ),
                )
            ),
            array(
                'prefix' => 'FX',
                'name' => 'Effecten',
                'metaDescr' => 'fx moet je zien',
                'metaKey' => 'fx, lampen, boxen',
                'subtitle' => 'fx',
                'descr' => 'fx met wat meer tekst',
                'image' => '/location/image.png',
                'imageAlt' => 'Plaatje van fx',
                'contains_product' => false,
                'children' => array(
                    array(
                        'prefix' => 'CO2',
                        'name' => 'CO2',
                        'metaDescr' => 'fx moet je zien',
                        'metaKey' => 'fx, lampen, boxen',
                        'subtitle' => 'fx',
                        'descr' => 'fx met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van fx',
                        'contains_product' => true,
                    ),
                    array(
                        'prefix' => 'LAS',
                        'name' => 'Lasers',
                        'metaDescr' => 'fx moet je zien',
                        'metaKey' => 'fx, lampen, boxen',
                        'subtitle' => 'fx',
                        'descr' => 'fx met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van fx',
                        'contains_product' => true,
                    ),
                )
            ),
            array(
                'prefix' => 'VID',
                'name' => 'Video',
                'metaDescr' => 'video moet je zien',
                'metaKey' => 'video, lampen, boxen',
                'subtitle' => 'video',
                'descr' => 'video met wat meer tekst',
                'image' => '/location/image.png',
                'imageAlt' => 'Plaatje van video',
                'contains_product' => false,
                'children' => array(
                    array(
                        'prefix' => 'BEAM',
                        'name' => 'Beamers',
                        'metaDescr' => 'Beamer moet je zien',
                        'metaKey' => 'Beamer, lampen',
                        'subtitle' => 'Beamertjess',
                        'descr' => 'Beamer met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van Beamer',
                        'contains_product' => true,
                    ),
                )
            ),
            array(
                'prefix' => 'KAB',
                'name' => 'Bekabeling',
                'metaDescr' => 'stroom moet er zijn',
                'metaKey' => 'stroom, lampen, boxen',
                'subtitle' => 'stroompjess',
                'descr' => 'stroom dan met wat meer tekst',
                'image' => '/location/image.png',
                'imageAlt' => 'Plaatje van stroom',
                'contains_product' => false,
                
                'children' => array(
                    array(
                        'prefix' => 'VIKA',
                        'name' => 'Video kabels',
                        'metaDescr' => 'video kabels moet je zien',
                        'metaKey' => 'Video, lampen',
                        'subtitle' => 'video kabeltjes',
                        'descr' => 'Beamer met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van kabels',
                        'contains_product' => false,
                        
                        'children' => array(
                            array(
                                'prefix' => 'DVI',
                                'name' => 'DVI',
                                'metaDescr' => 'dvi moet je zien',
                                'metaKey' => 'Beamer, lampen',
                                'subtitle' => 'Beamertjess',
                                'descr' => 'Beamer met wat meer tekst',
                                'image' => '/location/image.png',
                                'imageAlt' => 'Plaatje van DVI',
                                'contains_product' => true,
                            ),
                            array(
                                'prefix' => 'HDMI',
                                'name' => 'HDMI',
                                'metaDescr' => 'HDMI moet je zien',
                                'metaKey' => 'HDMI, lampen',
                                'subtitle' => 'HDMItjess',
                                'descr' => 'HDMI met wat meer tekst',
                                'image' => '/location/image.png',
                                'imageAlt' => 'Plaatje van HDMI',
                                'contains_product' => true,
                            ),
                        )
                    ),
                    array(
                        'prefix' => 'KRA',
                        'name' => 'Krach stroom kabels',
                        'metaDescr' => 'Kracht stroom kabels moet je zien',
                        'metaKey' => 'Krachtstroom, lampen',
                        'subtitle' => 'Kracht stroom kabeltjes',
                        'descr' => 'Kracht met wat meer tekst',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van Kracht',
                        'contains_product' => true,
                    ),
                )
            ),
            array(
                'prefix' => 'SET',
                'name' => 'Samengestelde sets',
                'metaDescr' => 'Setjes',
                'metaKey' => 'stroom, lampen, boxen', // TODO Should be an array
                'subtitle' => 'Deze sets zijn echt niet normaal, speciaal samengesteld voor alle mongols die er niks van snappen.',
                'descr' => 'Onze zets zijn niet meer normaal ouwe.',
                'image' => '/location/image.png',
                'imageAlt' => 'Plaatje van setjes',
                'contains_product' => false,
                'children' => array(
                    array(
                        'prefix' => 'GLDS',
                        'name' => 'Geluidsets',
                        'metaDescr' => 'Geluidsets in de cat samengestelde sets',
                        'metaKey' => 'geluidset, sletjes',
                        'subtitle' => 'geluidsets van dj set tot geluidsetjes',
                        'descr' => 'geluidset op je dakt',
                        'image' => '/location/image.png',
                        'imageAlt' => 'Plaatje van geluidsets',
                        'contains_product' => true,
                    ),
                )
            )
        );
        $this->newCategories($categories, $manager);
    }

    private function createCategory($category){
        $categoryObject = new Category();
        $categoryObject->setPrefix($category['prefix']);
        $categoryObject->setName($category['name']);
        $categoryObject->setDescription($category['descr']);
        $categoryObject->setContainsProducts($category['contains_product']);
        return $categoryObject;
    }

    private function newCategories($categories, ObjectManager $manager)
    {
        foreach($categories as $category){
            $parentCategoryObject = $this->createCategory($category);
            foreach($category['children'] as $categoryChild){
                $childCategoryObject = $this->createCategory($categoryChild);
                $parentCategoryObject->setChild($childCategoryObject);

                if(isset($categoryChild['children'])){
                    foreach($categoryChild['children'] as $childCategoryChild){
                        $childCategoryChildObject = $this->createCategory($childCategoryChild);
                        $childCategoryObject->setChild($childCategoryChildObject);
                    }
                }
            }
            $manager->persist($parentCategoryObject);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}