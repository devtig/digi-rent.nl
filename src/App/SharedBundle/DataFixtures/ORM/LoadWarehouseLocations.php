<?php

namespace App\SharedBundle\DataFixtures\ORM;

use App\SharedBundle\Entity\Address;
use App\WarehouseBundle\Entity\WarehouseLocation;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadWarehouseLocations extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $em = $this->container->get('doctrine')->getManager('default');
        $countryRepo = $em->getRepository('CoreBundle:Country');

        $holland = $countryRepo->findOneBy(array('abbreviation' => 'NL'))->getId();

        $warehouseLocations = array(
            array(
                'main' => true,
                'name' => 'Hoofd loods',
                'description' => 'Loods waar alle voorraad ligt van Uniq Rental en Uniq FX',
                'location' => 'ST',
                'type' => 30,
                'address' => array(
                    array(
                    'types' => array(10, 12),
                    'streetname' => 'Prins Reinierstraat',
                    'housenumber' => 18,
                    'housenumberExtra' => 'L',
                    'zipcode' => '4651RZ',
                    'city' => 'Steenbergen NB',
                    'country' => $holland,
                    ),
                ),
                'locations' =>  array(
                    array(
                        'name' => 'Stelling in brede gang (Truss & Rigging) en voorraad',
                        'location' => 'A',
                        'type' => 31,
                        'locations' => array(
                            array(
                                'name' => 'Links, Truss & rigging',
                                'location' => 'L',
                                'type' => 32,
                            ),
                            array(
                                'name' => 'Voorraad',
                                'location' => 'R',
                                'type' => 32,
                                'locations' => array(
                                    array(
                                        'name' => 'Truss',
                                        'location' => '01',
                                        'type' => 33,
                                        'locations' => array(
                                            array(
                                                'name' => 'Hoeken',
                                                'location' => '01',
                                                'type' => 34,
                                                'locations' => array(
                                                    array(
                                                        'name' => 'Vierkante hoeken',
                                                        'location' => 'A',
                                                        'type' => 35,
                                                    ),
                                                    array(
                                                        'name' => 'Driehoeken',
                                                        'location' => 'B',
                                                        'type' => 35,
                                                    ),
                                                    array(
                                                        'name' => 'Ronde hoeken',
                                                        'location' => 'C',
                                                        'type' => 35,
                                                    ),
                                                    array(
                                                        'name' => 'Ovale hoeken',
                                                        'location' => 'D',
                                                        'type' => 35,
                                                    ),
                                                ),
                                            ),
                                            array(
                                                'name' => 'kistjes voor truss',
                                                'location' => '02',
                                                'type' => 34,
                                                'locations' => array(
                                                    array(
                                                        'name' => 'Trusspennenkoffers',
                                                        'location' => 'A',
                                                        'type' => 35,
                                                    )
                                                ),
                                            ),
                                        ),
                                    ),
                                    array(
                                        'name' => 'Truss delen tot 1 meter',
                                        'location' => '02',
                                        'type' => 33,
                                    )
                                ),
                            ),
                        ),
                    ),
                    array(
                        'name' => 'Verzin maar wat B',
                        'location' => 'B',
                        'type' => 31,
                    ),
                    array(
                        'name' => 'Verzin maar wat C ',
                        'location' => 'C',
                        'type' => 31,
                    ),
                    array(
                        'name' => 'Verzin maar wat D',
                        'location' => 'D',
                        'type' => 31,
                    ),
                ),
            ),
        );
        $this->newWarehouseLocations($warehouseLocations, $manager);
    }
    private function newWarehouseLocations($warehouseLocations, ObjectManager $manager)
    {
        foreach($warehouseLocations as $warehouseLocation){

            $warehouseLocationObject = $this->createWarehouseLocation($warehouseLocation);

            foreach($warehouseLocation['address'] as $warehouseLocationAddress){
                $addressObject = new Address();

                $addressObject->addAddressType($warehouseLocationAddress['types'][0]);
                $addressObject->setStreetname($warehouseLocationAddress['streetname']);
                $addressObject->setHousenumber($warehouseLocationAddress['housenumber']);
                $addressObject->setHousenumberExtra($warehouseLocationAddress['housenumberExtra']);
                $addressObject->setZipcode($warehouseLocationAddress['zipcode']);
                $addressObject->setCity($warehouseLocationAddress['city']);
                $addressObject->setCountry($warehouseLocationAddress['country']);
                // Add address to company
                $warehouseLocationObject->setAddress($addressObject);
            }
            $this->addChildrenLocations($warehouseLocationObject, $warehouseLocation);

            $manager->persist($warehouseLocationObject);

            $manager->flush();
        }
    }

    public function createWarehouseLocation($warehouseLocation){
        $warehouseLocationObject = new WarehouseLocation();
        if(isset($warehouseLocation['main'])){
            $warehouseLocationObject->setMain($warehouseLocation['main']);
        }
        if(isset($warehouseLocation['name'])){
            $warehouseLocationObject->setName($warehouseLocation['name']);
        }
        if(isset($warehouseLocation['description'])){
            $warehouseLocationObject->setDescription($warehouseLocation['description']);
        }
        if(isset($warehouseLocation['location'])){
            $warehouseLocationObject->setcode($warehouseLocation['location']);
        }
        if(isset($warehouseLocation['type'])){
            $warehouseLocationObject->setType($warehouseLocation['type']);
        }
        return $warehouseLocationObject;
    }

    public function addChildrenLocations($warehouseLocationObject, $warehouseLocation){
        foreach($warehouseLocation['locations'] as $locationInWarehouse){
            $locationInWarehouseObject = $this->createWarehouseLocation($locationInWarehouse);
            $warehouseLocationObject->setChild($locationInWarehouseObject);

            if(isset($locationInWarehouse['locations'])){
                $this->addChildrenLocations($locationInWarehouseObject, $locationInWarehouse);
            }
        }
    }
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}