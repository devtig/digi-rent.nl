<?php

namespace App\SharedBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\ProductBundle\Entity\ProductActivityState;

class LoadProductActivities extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $activityStateArray = array(
            array(
                'name' => "Voorraad uitgaand (verhuur)",
            ),
            array(
                'name' => "Voorraad inkomend (verhuur)",
            ),
            array(
                'name' => "Onderhoud (uit voorraad)",
            ),
            array(
                'name' => "Onderhoud (terug op voorraad)",
            ),
        );
        $this->addActivityTypes($activityStateArray);
    }

    private function addActivityTypes($activityTypesArray)
    {
        $em = $this->container->get('doctrine')->getEntityManager('dynamic');

        foreach($activityTypesArray as $actvityStateObject){
            $actvityState = new ProductActivityState();
            $actvityState->setName($actvityStateObject['name']);

            $em->persist($actvityState);
        }

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }

}