<?php

namespace App\CoreBundle\Model;

use App\SubdomainBundle\Entity\Subdomain;
use App\SubdomainBundle\Model\SubdomainInterface;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class PasswordManager {

    private $options = array(
            'uppercase' => true,
            'lowercase' => true,
            'numbers' => true,
            'symbols' => false,
            'length' => 16
        );

    private $encoderFactory;
    private $generator;
    private $secret;
    private $dbPrefix;

    public function __construct(EncoderFactory $encoderFactory, ComputerPasswordGenerator $generator, $secret, $dbPrefix){
        $this->encoderFactory = $encoderFactory;
        $this->generator = $generator;
        $this->secret = $secret;
        $this->dbPrefix = $dbPrefix;
    }

    public function generatePassword($options = array()){

        if(!empty($options)){
            $this->options = $options;
        }

        return $this->generator
            ->setUppercase($this->options['uppercase'])
            ->setLowercase($this->options['lowercase'])
            ->setNumbers($this->options['numbers'])
            ->setSymbols($this->options['symbols'])
            ->setLength($this->options['length'])
            ->generatePassword();
    }

    public function secureSubdomain(Subdomain $subdomain){
        $dbName = $this->generatePassword();
        $dbUsername = $this->generatePassword();

        $subdomain->setDbName($this->dbPrefix.$dbName);
        $subdomain->setUsername($dbUsername);
        $subdomain->setPlainPassword($this->getSubdomainPassword($subdomain));

        return $subdomain;
    }

    public function getSubdomainPassword(Subdomain $subdomain){
        $parts = substr($subdomain->getDbName(), 5, 10) . substr($subdomain->getUsername(), 2, 9) . substr($subdomain->getSalt(), 4, 11);
        return hash('sha256', $this->secret.$parts);
    }

    public function getEncoder($subdomain)
    {
        return $this->encoderFactory->getEncoder($subdomain);
    }

}