<?php

namespace App\CoreBundle\Form\Type;

use App\CoreBundle\Entity\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressTypeType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver)
    {
        $addressTypes = AddressType::$addressTypes;
        unset($addressTypes[0]);
        $resolver->setDefaults(
            [
                'choices_as_values' => true,
                'choices'           => array_flip($addressTypes),
            ]
        );
    }
    public function getParent()
    {
        return method_exists(AbstractType::class, 'getBlockPrefix') ? ChoiceType::class : 'choice';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }
    public function getBlockPrefix()
    {
        return 'addressType';
    }
}