<?php
//
//namespace App\CoreBundle\Services;
//
//use Symfony\Component\HttpFoundation\Request;
//use Doctrine\DBAL\Connection;
//use Doctrine\Bundle\DoctrineBundle\Registry;
//
///**
// * Creates a Doctrine connection from attributes in the Request
// */
//class DynamicDoctrineConnection
//{
//    private $request;
//    private $dynamicConnection;
//    private $doctrine;
//
//    public function __construct(Request $request, Connection $dynamicConnection, Registry $doctrine)
//    {
//        $this->request = $request;
//        $this->dynamicConnection = $dynamicConnection;
//        $this->doctrine = $doctrine;
//    }
//
//    public function onKernelRequest()
//    {
//        if ($this->request->attributes->has('subdomain')) {
//
//            $dbName = 'dr_' . $this->request->attributes->get('subdomain');
//
//            $this->dynamicConnection->close();
//
//            $reflectionConn = new \ReflectionObject($this->dynamicConnection);
//            $reflectionParams = $reflectionConn->getProperty('_params');
//            $reflectionParams->setAccessible(true);
//
//            $params = $reflectionParams->getValue($this->dynamicConnection);
//            $params['dbname'] = $dbName;
//
//            $reflectionParams->setValue($this->dynamicConnection, $params);
//            $reflectionParams->setAccessible(false);
//
//            $this->doctrine->resetEntityManager('dynamic');
//        }
//    }
//}