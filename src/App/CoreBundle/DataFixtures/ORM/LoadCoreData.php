<?php

namespace App\CoreBundle\DataFixtures\ORM;

use App\CoreBundle\Entity\AddressType;
use App\CoreBundle\Entity\Branche;
use App\CoreBundle\Entity\Country;
use App\CoreBundle\Entity\PhoneType;
use App\CoreBundle\Entity\Tax;
use App\CoreBundle\Entity\TicketStatus;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCoreData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    private $manager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

//        // AddressTypes
//        $addressTypes = array(
//            array(
//                'name' => 'Bezoekadres',
//            ),
//            array(
//                'name' => 'Postadres',
//            ),
//            array(
//                'name' => 'Afhaaladres',
//            ),
//        );


//        // TicketStates
//        $ticketStates = array(
//            array(
//                'name' => 'Ontvangen',
//            ),
//            array(
//                'name' => 'In behandeling',
//            ),
//            array(
//                'name' => 'Afgerond',
//            ),
//        );
//
//        // Priorities
//        $priorities = array(
//            array(
//                'name' => 'Laag',
//            ),
//            array(
//                'name' => 'Gemiddeld',
//            ),
//            array(
//                'name' => 'Urgent',
//            ),
//        );
//
//        // Priorities
//        $priorities = array(
//            array(
//                'name' => 'Laag',
//            ),
//            array(
//                'name' => 'Gemiddeld',
//            ),
//            array(
//                'name' => 'Urgent',
//            ),
//        );

//        // PhoneTypes
//        $phoneTypes = array(
//            array(
//                'name' => 'Mobiel',
//            ),
//            array(
//                'name' => 'Vast',
//            ),
//            array(
//                'name' => 'Skype',
//            ),
//            array(
//                'name' => 'Fax',
//            ),
//        );

        // Branches
        $branches = array(
            array(
                'name' => 'Evenementen',
                'description' => 'Alles wat te maken heeft met evenementen, van geluid, licht tot tenten, effecten en decoratie.',
                'children' => array(
                    array(
                        'name' => 'Catering',
                        'description' => 'Faciliteiten voor het verzorgen van catering zoals: ovens, koelkasten, tafels en stoelen.',
                    ),
                    array(
                        'name' => 'Decoratie',
                        'description' => 'Aankleding van evenementlocaties zoals: podia, tuinen en zalen.',
                    ),
                    array(
                        'name' => 'Techniek (Audio, Video, Light)',
                        'description' => 'Techniek op evenementen zoals: speakers, licht en microfoons.',
                    ),
                    array(
                        'name' => 'Special Effects',
                        'description' => 'Techniek op evenementen zoals: confetti, co2 en rook.',
                    ),
                    array(
                        'name' => 'Tenten en overkappingen',
                        'description' => 'Voorzieningen van mobiele evenement locaties zoals: aluhallen, pagodetenten, circus en stretchtenten',
                    ),
                )
            ),
            array(
                'name' => 'Transport',
                'description' => 'Alles wat te maken heeft met transportmiddelen, van klein tot groot transport.',
                'children' => array(
                    array(
                        'name' => 'Klein transport',
                        'description' => 'Vervoersmiddelen zoals: motoren, personen auto\'s en kleine aanhangwagens.',
                    ),
                    array(
                        'name' => 'Groot transport',
                        'description' => 'Vervoersmiddelen zoals: bakwagens, vrachtwagens, grote aanhangers of opleggers.',
                    ),
                )
            ),
            array(
                'name' => 'Bouw',
                'description' => 'Alles wat te maken heeft met de bouw, van gereedschap tot machines tot vervoer en transport.',
                'children' => array(
                    array(
                        'name' => 'Gereedschap',
                        'description' => 'Materialen zoals boren, zagen en steigers.',
                    ),
                    array(
                        'name' => 'Machines',
                        'description' => 'Verhuur van bouwmachines, bijvoorbeeld: mortelmolen, boren.',
                    ),
                    array(
                        'name' => 'Vervoer en transport',
                        'description' => 'Verhuur van bijvoorbeeld: vrachtwagens, cementwagen en hijskraan.',
                    ),
                )
            ),
            //                array(
//                    'name' => 'Decor-, podium- en standbouw',
//                    'description' => 'Verzorging van o.a. beursstands, theater decors of podium bouw.',
//                ),
//                array(
//                    'name' => 'Entertainment (optredens)',
//                    'description' => 'Entertainment (optredens)',
//                ),
//                array(
//                    'name' => 'Detachering',
//                    'description' => 'Detachering',
//                ),
        );

       // Countries
        $countries = array(
            array(
                'name' => 'The Netherlands',
                'abbreviation' => 'NL',
                'currency' => 'EUR',
                'timezone' => 'CET',
            ),
            array(
                'name' => 'Belgium',
                'abbreviation' => 'BE',
                'currency' => 'EUR',
                'timezone' => 'CET',
            ),
            array(
                'name' => 'Germany',
                'abbreviation' => 'DE',
                'currency' => 'EUR',
                'timezone' => 'CET',
            ),
            array(
                'name' => 'France',
                'abbreviation' => 'FR',
                'currency' => 'EUR',
                'timezone' => 'CET',
            ),
            array(
                'name' => 'Great Britain',
                'abbreviation' => 'GB',
                'currency' => 'GBP',
                'timezone' => 'GMT',
            ),
        );

        // Tax
        $taxes = array(
            'NL' => array(
                array(
                    'title' => 'BTW hoog tarief',
                    'percentage' => 21,
                    'description' => 'Dit is het algemene btw-tarief in Nederland dat geldt voor alle goederen en diensten die niet vrijgesteld zijn, en die niet onder het 6%- of 0%-tarief vallen.',
                    'introduced' => new \DateTime('1-10-2012')
                ),
                array(
                    'title' => 'BTW laag tarief',
                    'percentage' => 6,
                    'description' => 'Dit is het lage btw-tarief in Nederland dat geldt voor bepaalde diensten en goederen die voornamelijk voorzien in de eerste levensbehoeften.',
                    'introduced' => new \DateTime('1-10-12')
                )  ,
                array(
                    'title' => 'BTW verlegd tarief',
                    'percentage' => 0,
                    'description' => 'Dit is het verlegde btw-tarief in Nederland dat voornamelijk gebruikt wordt wanneer diensten of goederen internationaal geleverd worden.',
                    'introduced' => new \DateTime('1-10-12')
                ),
            ),
            'BE' => array(
                array(
                    'title' => 'BTW hoog tarief',
                    'percentage' => 21,
                    'description' => 'Dit is het algemene btw-tarief in België dat geldt voor alle goederen en diensten die niet vrijgesteld zijn, en die niet onder het 6%- of 0%-tarief vallen.',
                    'introduced' => new \DateTime('1-10-2012')
                ),
                array(
                    'title' => 'BTW tussen tarief',
                    'percentage' => 12,
                    'description' => 'Dit is het tussen btw-tarief in België dat voornamelijk gebruikt wordt voor economisc of sociaal oogpunt belangrijk zijn.',
                    'introduced' => new \DateTime('1-10-12')
                )  ,
                array(
                    'title' => 'BTW laag tarief',
                    'percentage' => 6,
                    'description' => 'Dit is het lage btw-tarief in België dat voornamelijk gebruikt wordt voor basisproduction en diensten met een sociaal karakter',
                    'introduced' => new \DateTime('1-10-12')
                ),
            ),
        );
        // Leave it at this order!
//        $this->newAddressTypes($addressTypes);
//        $this->newTicketStates($ticketStates);
//        $this->newPhoneTypes($phoneTypes);
        $this->newBranches($branches);
        $this->newCountries($countries);
//        $this->newTaxes($taxes);
        $manager->flush();

    }

    private function newAddressTypes($addressTypes)
    {
        foreach($addressTypes as $addressType){
            $addressTypeObject = new AddressType();
            $addressTypeObject
                ->setName($addressType['name']);

            $this->manager->persist($addressTypeObject);
        }
        return;
    }

    private function newTicketStates($ticketStates)
    {
        foreach($ticketStates as $ticketState){
            $ticketStateObject = new TicketStatus();
            $ticketStateObject
                ->setName($ticketState['name']);

            $this->manager->persist($ticketStateObject);
        }
        return;
    }

    private function newPhoneTypes($phoneTypes)
    {
        foreach($phoneTypes as $phoneType){
            $phoneTypeObject = new PhoneType();
            $phoneTypeObject
                ->setName($phoneType['name']);

            $this->manager->persist($phoneTypeObject);
        }
        return;
    }

    private function newBranches($branches)
    {
        foreach ($branches as $brancheParent) {
            $brancheParentObject = new Branche();
            $brancheParentObject->setName($brancheParent['name']);
            $brancheParentObject->setDescription($brancheParent['description']);

            if(isset($brancheParent['children'])){
                foreach($brancheParent['children'] as $brancheChild){
                    $brancheChildObject = new Branche();
                    $brancheChildObject->setName($brancheChild['name']);
                    $brancheChildObject->setDescription($brancheChild['description']);
                    $brancheParentObject->setChild($brancheChildObject);
                }
            }
            $this->manager->persist($brancheParentObject);
        }
        return;
    }

    private function newCountries($countries)
    {
        foreach($countries as $country){
            $countryObject = new Country();
            $countryObject
                ->setName($country['name'])
                ->setAbbreviation($country['abbreviation'])
                ->setCurrency($country['currency'])
                ->setTimezone($country['timezone']);

            $this->manager->persist($countryObject);
        }

        // Countries need to be saved (taxes relation)
        $this->manager->flush();

        return;
    }

    private function newTaxes($taxes)
    {
        $em = $this->container->get('doctrine')->getManager();
        $countries = $em->getRepository('CoreBundle:Country');
        $x = 0;
        $y = 1;
        foreach($taxes as $taxItem){
            $countryAbr = key( array_slice($taxes, $x, $y, true ));
            $country = $countries->findOneBy(array('abbreviation' => $countryAbr));
            $x++;
            $y++;

            foreach($taxItem as $tax ){
                $taxObject = new Tax();
                $taxObject->setTitle($tax['title']);
                $taxObject->setCountry($country);
                $taxObject->setDescription($tax['description']);
                $taxObject->setIntroduced($tax['introduced']);
                $taxObject->setPercentage($tax['percentage']);

                $this->manager->persist($taxObject);
            }
        }
        return;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
}