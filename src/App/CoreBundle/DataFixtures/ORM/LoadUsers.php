<?php
//
//namespace App\CoreBundle\DataFixtures\ORM;
//
//use App\SharedBundle\Entity\Phone;
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Shared\UserBundle\Entity\User;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//
//class LoadUsers extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
//{
//
//    private $container;
//
//    /**
//     * Sets the container.
//     *
//     * @param ContainerInterface|null $container A ContainerInterface instance or null
//     */
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function load(ObjectManager $manager)
//    {
//        $em = $this->container->get('doctrine')->getManager();
//        $companyRepo = $em->getRepository('CompanyBundle:Company');
//
//        $ngs = $companyRepo->findOneBy(array('name' => 'NGS'));
//        $bsf = $companyRepo->findOneBy(array('name' => 'BSF'));
//        $uniq = $companyRepo->findOneBy(array('name' => 'Uniq Group'));
//
//        $users = array(
//            array(
//                'firstname' => 'peter',
//                'lastname' => 'knijff',
//                'email' => 'pknijff90@gmail.com',
//                'password' => '123123',
//                'role' => 'ROLE_SUPER_ADMIN',
//                'mobile' => '06-46408646',
//                'skype' => '@pknijff',
//                'newsletter' => true,
//                'company' => $uniq
//            ),
//            array(
//                'firstname' => 'peter',
//                'lastname' => 'knijff',
//                'email' => 'peter@uniq-group.nl',
//                'password' => '123123',
//                'role' => 'ROLE_ADMIN',
//                'mobile' => '06-46408646',
//                'skype' => '@pknijff',
//                'newsletter' => true,
//                'company' => $uniq
//            ),
//            array(
//                'firstname' => 'paul',
//                'middlename' => 'van der',
//                'lastname' => 'heijden',
//                'email' => 'paul@uniq-group.nl',
//                'password' => '123123',
//                'role' => 'ROLE_ADMIN','ROLE_USER',
//                'mobile' => '06-10042872',
//                'company' => $uniq
//            ),
//            array(
//                'firstname' => 'Sander',
//                'lastname' => 'Dingenouts',
//                'email' => 'sander@ngs.nl',
//                'password' => '123123',
//                'role' => 'ROLE_ADMIN',
//                'mobile' => '06-23913596',
//                'company' => $ngs
//            ),
//            array(
//                'firstname' => 'Roy',
//                'lastname' => 'Kreeft',
//                'email' => 'roy@ngs.nl',
//                'password' => '123123',
//                'mobile' => '06-2sas4243',
//                'company' => $ngs
//            ),
//            array(
//                'firstname' => 'Bob',
//                'lastname' => 'Cuelenaere',
//                'email' => 'bob@bsf.nl',
//                'password' => '123123',
//                'mobile' => '06-sdadsdsa',
//                'company' => $bsf
//            ),
//        );
//
//        foreach ($users as $user) {
//
//            $userObject = new User();
//            $userObject
//                ->setFirstname($user['firstname']);
//            $userObject ->setLastname($user['lastname'])
//                        ->setUsername($user['firstname'].$user['lastname'])
//                        ->setEmail($user['email'])
//                        ->setPlainPassword($user['password'])
//                        ->setCompany($user['company'])
//                        ->setDayOfBirth(new \DateTime("1990-02-03"))
//                        ->setEnabled(true);
//
//            if(isset($user['role'])){
//                $userObject->addRole($user['role']);
//            }
//
//            if(isset($user['middlename'])){
//                $userObject->setMiddleName($user['middlename']);
//            }
//
//            if(isset($user['newsletter'])){
//                $userObject->setNewsletter($user['newsletter']);
//            }
//
//            if(isset($user['mobile'])){
//                $phoneObject = new Phone();
//                $phoneObject
//                    ->setNumber($user['mobile'])
//                    ->setPhoneType(10);
//                    $userObject->addPhone($phoneObject);
//            }
//
//            if(isset($user['skype'])){
//                $phoneObject = new Phone();
//                $phoneObject
//                    ->setNumber($user['skype'])
//                    ->setPhoneType(12);
//                $userObject->addPhone($phoneObject);
//            }
//
//            $manager->persist($userObject);
//
//            $manager->flush();
//        }
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 2; // the order in which fixtures will be loaded
//    }
//}