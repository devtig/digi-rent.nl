<?php
//
//namespace App\CoreBundle\DataFixtures\ORM;
//
//use App\CoreBundle\Entity\PhoneType;
//use App\SharedBundle\Entity\Address;
//use App\SharedBundle\Entity\Phone;
//use App\CompanyBundle\Entity\Company;
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//
//class LoadCompanies extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
//{
//
//    private $container;
//
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function load(ObjectManager $manager)
//    {
//        $em = $this->container->get('doctrine')->getManager();
//        $countryRepo = $em->getRepository('CoreBundle:Country');
//
//        $holland = $countryRepo->findOneBy(array('abbreviation' => 'NL'));
//
//        $companies = array(
//            array(
//                'name' => 'Uniq Group',
//                'subdomainName' => 'uniq',
//                'btw' => 'NL851023757B01',
//                'kvk' => '538055589',
//                'address' => array(
//                    array(
//                    'types' => array(10, 12),
//                    'streetname' => 'Prins Reinierstraat',
//                    'housenumber' => 18,
//                    'housenumberExtra' => 'L',
//                    'zipcode' => '4651RZ',
//                    'city' => 'Steenbergen NB',
//                    'country' => $holland,
//                    ),
//                    array(
//                    'types' => array(11),
//                    'streetname' => 'Noorddonk',
//                    'housenumber' => 115,
//                    'housenumberExtra' => '',
//                    'zipcode' => '4651ZD',
//                    'city' => 'Steenbergen NB',
//                    'country' => $holland,
//                    ),
//                ),
//                'vast' => '0167-700512'
//            ),
//            array(
//                'name' => 'Devtig',
//                'subdomainName' => 'devtig',
//                'btw' => 'NL851023757B01',
//                'kvk' => '53801989',
//                'address' => array(
//                    array(
//                        'types' => array(10, 12),
//                        'streetname' => 'Prins Reinierstraat',
//                        'housenumber' => 18,
//                        'housenumberExtra' => 'L',
//                        'zipcode' => '4651RZ',
//                        'city' => 'Steenbergen NB',
//                        'country' => $holland,
//                    ),
//                    array(
//                        'types' => array(11),
//                        'streetname' => 'Noorddonk',
//                        'housenumber' => 115,
//                        'housenumberExtra' => '',
//                        'zipcode' => '4651ZD',
//                        'city' => 'Steenbergen NB',
//                        'country' => $holland,
//                    ),
//                ),
//                'vast' => '0167-700512'
//            ),
//            array(
//                'name' => 'NGS',
//                'subdomainName' => 'ngs',
//                'btw' => 'NL851023327B10',
//                'kvk' => '53801123',
//                'address' => array(
//                    array(
//                        'types' => array(10, 11, 12),
//                        'streetname' => 'Kleine Kerkstraat',
//                        'housenumber' => 13,
//                        'housenumberExtra' => '',
//                        'zipcode' => '4651EV',
//                        'city' => 'Steenbergen NB',
//                        'country' => $holland,
//                    ),
//                ),
//                'mobile' => '0623913596'
//            ),
//            array(
//                'name' => 'BSF',
//                'subdomainName' => 'bspecial',
//                'btw' => 'NL851023327B10',
//                'kvk' => '53805223',
//                'address' => array(
//                    array(
//                        'types' => array(10, 11, 12),
//                        'streetname' => 'Kwarenvliet',
//                        'housenumber' => 1,
//                        'housenumberExtra' => '',
//                        'zipcode' => '4651WE',
//                        'city' => 'Steenbergen NB',
//                        'country' => $holland,
//                    ),
//                ),
//                'mobile' => '06-18486772'
//            ),
//        );
//
//        $this->newCompanies($companies, $manager);
//
//    }
//
//    private function newCompanies($companies, ObjectManager $manager)
//    {
//        foreach($companies as $company){
//
//            $companyObject = new Company();
//
//            foreach($company['address'] as $companyAddress){
//                $addressObject = new Address();
//                $addressObject->setAddressTypes($companyAddress['types']);
//                $addressObject->setStreetname($companyAddress['streetname']);
//                $addressObject->setHousenumber($companyAddress['housenumber']);
//                $addressObject->setHousenumberExtra($companyAddress['housenumberExtra']);
//                $addressObject->setZipcode($companyAddress['zipcode']);
//                $addressObject->setCity($companyAddress['city']);
//                $addressObject->setCountry($companyAddress['country']->getId());
//                // Add address to company
//                $companyObject->addAddress($addressObject);
//            }
//            $companyObject->setName($company['name']);
//            $companyObject->setSubdomainName($company['subdomainName']);
//            $companyObject->setBtw($company['btw']);
//            $companyObject->setKvk($company['kvk']);
//
//
//            if(isset($company['vast'])){
//                $phoneObject = new Phone();
//                $phoneObject
//                    ->setNumber($company['vast'])
//                    ->setPhoneType(11);
//                $companyObject->addPhone($phoneObject);
//            }
//
//            if(isset($company['mobile'])){
//                $phoneObject = new Phone();
//                $phoneObject
//                    ->setNumber($company['mobile'])
//                    ->setPhoneType(10);
//                $companyObject->addPhone($phoneObject);
//            }
//
//            $manager->persist($companyObject);
//
//            $manager->flush();
//        }
//
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 1; // the order in which fixtures will be loaded
//    }
//}