<?php
//
//namespace App\CoreBundle\DataFixtures\ORM;
//
//use App\SupportBundle\Entity\FAQ;
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//
//class LoadSupport extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
//{
//    private $container;
//
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function load(ObjectManager $manager)
//    {
//
//        $em = $this->container->get('doctrine')->getManager();
//
//        $userRepo = $em->getRepository('UserBundle:User');
//        $peter = $userRepo->findOneBy(array('email' => 'sander@ngs.nl'));
//        $faqs = array(
//            array(
//                'name' => 'Is het mogelijk om data vanuit Rentman te exporteren?',
//                'answer' => 'Ja, data kunnen als Excel bestand worden geëxporteerd. Het exporteren is binnen enkele ogenblikken gebeurd. Uitgebreide instructies daarvoor zijn hier te vinden: Items exporteren',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Kan ik met QR-codes werken?',
//                'answer' => 'Ja, in Rentman is het mogelijk om QR-codes te gebruiken. Labels zijn gekoppeld aan de serienummers van artikelen. Met een QR-code scanner of met een smartphone is het mogelijk om materiaal in en uit te boeken en zo een verhuurhistorie op te bouwen. Bij een losse verhuur kan een artikel ook meteen met behulp van de code in het systeem worden ingeboekt.',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Omvat de demoversie alle functionaliteiten van de normale versie?',
//                'answer' => 'Ja. Alle functionaliteiten van de normale versie zijn ook in de testversie te vinden.',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Hoe duur is de support van Rentman?',
//                'answer' => 'De support van Rentman is onbeperkt en gratis.',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Hoe activeer ik mijn account?',
//                'answer' => 'Als u ingelogd bent in uw Rentman staat rechtsboven je naam. Ga met de muis op je naam en er opent een drop-down menu. Druk op ‘Activeer account’ en volg de beschreven stappen.',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Hoe is veiligheid van Rentman gegarandeerd?',
//                'answer' => 'Uw gegevens zijn persoonlijk en vertrouwelijk. Daarom zijn deze goed beveiligd op bankniveau (SSL-verbinding) en alleen voor jou toegankelijk.
//
//Doordat alle data op twee verschillende datacenters zijn opgeslagen, is het bijna onmogelijk dat uw informatie verdwijnt. Ook in het geval van een beschadiging aan uw computer zijn alle informatie in de cloud terug te vinden. Verder worden van de servers constant back-ups gemaakt.',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Hoe veel opslagruimte heb ik nodig?',
//                'answer' => 'In het begin is de 1 gigabyte, die deel van het basispakket uitmaakt, genoeg opslagruimte om bij uw projecten horende documenten (bv. offertes, facturen etc.) op te slaan. Met de standardtemplates die al in Rentman staan, kunt u tot 2000 offertes en facturen genereren.
//
//Als u het maximum van uw opslagruimte bijna heeft bereikt, dan krijgt u een melding en dan kunt u de opslagruimte upgraden. Daarnaast kunt u ook documenten downloaden en daarna uit Rentman verwijderen om zo weer meer opslagruimte ter beschikking te hebben.',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Hoe veel beheerderslicenties heb ik nodig?',
//                'answer' => 'Hoe veel beheerderslicenties nodig zijn, is afhankelijk van de bedrijfsgrootte en de manier van werken. Het is mogelijk om met meerdere personen een beheerderslicentie gebruiken, als deze personen niet tegelijkertijd aan het werk zijn. Anders is het aan te raden om twee of meer beheerderslicenties te gaan gebruiken. Na de testfase kunt u het waarschijnlijk het beste zelf inschatten.',
//                'action' => '',
//            ),
//            array(
//                'name' => 'Wat kan een gewone gebruiker?',
//                'answer' => 'De gewone gebruiker heeft inzicht in alle actuele projecten, kan materiaal beheren en materiaallijsten printen. Hij heeft toegang tot de urenregistratie en de agenda. De administrator heeft de mogelijkheid om voor elke gebruiker verschillende rechten in te stellen zodat het per gebruiker verschilt, welke dingen hij kan zien en welke hij mag bewerken.',
//                'action' => '',
//            ),
//        );
//
//        foreach($faqs as $faq){
//            $faqObject = new FAQ();
//            $faqObject->setName($faq['name'])
//                ->setAnswer($faq['answer'])
//                ->setAction($faq['action'])
//                ->setCreatedBy($peter);
//
//           $manager->persist($faqObject);
//        }
//
//        $manager->flush();
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 4; // the order in which fixtures will be loaded
//    }
//}