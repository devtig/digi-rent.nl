<?php
//
//namespace App\CoreBundle\DataFixtures\ORM;
//
//use App\SharedBundle\Entity\Address;
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
//use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\DependencyInjection\ContainerAwareInterface;
//use Symfony\Component\DependencyInjection\ContainerInterface;
//
//class LoadAdditionalUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
//{
//    private $container;
//
//    public function setContainer(ContainerInterface $container = null)
//    {
//        $this->container = $container;
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function load(ObjectManager $manager)
//    {
//        $em = $this->container->get('doctrine')->getManager();
//
//        $userRepo = $em->getRepository('UserBundle:User');
//        $companyRepo = $em->getRepository('CompanyBundle:Company');
//        $countryRepo = $em->getRepository('CoreBundle:Country');
//
//        $holland = $countryRepo->findOneBy(array('abbreviation' => 'NL'));
//
//        $roy = $userRepo->findOneBy(array('email' => 'roy@ngs.nl'));
//        $sander = $userRepo->findOneBy(array('email' => 'sander@ngs.nl'));
//        $bob = $userRepo->findOneBy(array('email' => 'bob@bsf.nl'));
//
//
//        $ngs = $companyRepo->findOneBy(array('name' => 'NGS'));
//        $bsf = $companyRepo->findOneBy(array('name' => 'BSF'));
//
//        $addressObject = new Address();
//        $addressObject
//            ->setStreetname('Fortplein')
//            ->setAddressTypes(array(10));
//        $addressObject->setCountry($holland->getId());
//        $addressObject->setHousenumber(12)
//            ->setHousenumberExtra('A')
//            ->setZipcode('4651QR')
//            ->setCity('Steenbergen NB');
//
//        $manager->persist($addressObject);
//        $manager->flush();
//
//
//        $sander->setCompany($ngs);
//        $roy->setCompany($ngs);
//        $bob->setCompany($bsf);
//
//        $peter = $userRepo->findOneBy(array('email' => 'peter@uniq-group.nl'));
//        $paul = $userRepo->findOneBy(array('email' => 'paul@uniq-group.nl'));
//        $uniq = $companyRepo->findOneBy(array('name' => 'Uniq Group'));
//
//        $peter->setCompany($uniq);
//        $paul->setCompany($uniq);
//        $manager->persist($peter);
//        $manager->persist($paul);
//
//        $manager->persist($sander);
//        $manager->persist($bob);
//        $manager->persist($roy);
//
//        $manager->flush();
//    }
//
//    /**
//     * {@inheritDoc}
//     */
//    public function getOrder()
//    {
//        return 3; // the order in which fixtures will be loaded
//    }
//}