<?php

namespace App\CoreBundle\Event;

/**
 * Contains all events thrown in the FOSUserBundle
 */
final class DREvents
{
    /**
     * The BETA_REGISTRATION_SUCCESS event occurs when the registration form is submitted successfully.
     *
     * This event allows you to set the response instead of using the default one.
     *
     * @Event("FOS\UserBundle\Event\FormEvent")
     */
    const BETA_REGISTRATION_SUCCESS = 'dr_app.beta_registration.success';

    /**
     * The SECURITY_IMPLICIT_LOGIN event occurs when the user is logged in programmatically.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("FOS\UserBundle\Event\UserEvent")
     */
    const BETA_REGISTRATION_CONFIRMED = 'dr_app.beta_registration.confirmed';

}
