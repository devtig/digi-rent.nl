<?php

namespace App\CoreBundle\Exceptions;

use Doctrine\ORM\EntityManager;

class ExceptionHelper {

    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function EntityErrorMessage(\Exception $exception) {
        if (strpos($exception->getMessage(),'foreign key') !== false) {
            return 'Kan niet verwijderd worden, <a class="messageLink" href="#1200">#1200</a>.';
        }else if(strpos($exception->getMessage(),'Shoarma') !== false){
            return 'Kan niet verwijderd worden, <a class="messageLink" href="#1200">#1200</a>.';
        }

        return 'Actie niet uitgevoerd, <a class="messageLink" href="#1000">#1000</a>';
    }

}