<?php

namespace App\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Tax
 *
 * @ORM\Table(name="core_country")
 * @ORM\Entity
 */
class Country
{

    use Traits\Identifiable,
        Traits\NameAble;
    /**
     * @var string
     *
     * @ORM\Column(name="abbreviation", type="string", length=3, unique=true)
     * @Assert\NotBlank(message = "Afkorting is verplicht")
     */
    private $abbreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3, unique=false)
     * @Assert\NotBlank(message = "Valuta is verplicht")
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=3, unique=false)
     * @Assert\NotBlank(message = "Tijdzone is verplicht")
     */
    private $timezone;

    /**
     * Set abbreviation
     *
     * @param string $abbreviation
     * @return Country
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation
     *
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Country
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return Country
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

}
