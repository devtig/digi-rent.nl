<?php

namespace App\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as GedmoMapping;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits;

/**
 * Branche
 *
 * @GedmoMapping\Tree(type="nested")
 * @ORM\Table(name="core_branche")
 * @ORM\Entity(repositoryClass="App\CoreBundle\Repository\BrancheRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Branche
{
    use Traits\Identifiable,
        Traits\NameAble,
        Traits\Describable,
        Traits\Sluggable,
        Traits\TimeStampable;

    function __construct()
    {
        $this->children = new ArrayCollection();
    }

    // Tree
    private $level;
    /**
     * @GedmoMapping\TreeParent
     * @ORM\ManyToOne(targetEntity="Branche", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    /**
     * @ORM\OneToMany(targetEntity="Branche", mappedBy="parent", cascade={"persist"})
     */
    private $children;
    /**
     * @GedmoMapping\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;
    /**
     * @GedmoMapping\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;
    /**
     * @GedmoMapping\TreeRoot
     * @ORM\Column(type="integer", nullable=true)
     */
    private $root;
    /**
     * @GedmoMapping\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */

    /**
     * Set child
     *
     * @param Branche $child
     * @return Branche
     */
    public function setChild(Branche $child) {
        $this->children[] = $child;
        $child->setParent($this);
    }

    /**
     * Get children
     *
     * @return Branche
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param Branche $parent
     * @return Branche
     */
    public function setParent(Branche $parent = NULL) {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return Branche
     */
    public function getParent() {
        return $this->parent;
    }

    public function getRoot()
    {
        return $this->root;
    }
    public function getLevel()
    {
        return $this->level;
    }
    public function getLeft()
    {
        return $this->lft;
    }
    public function getRight()
    {
        return $this->rgt;
    }

}

