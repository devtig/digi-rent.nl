<?php

namespace App\CoreBundle\Entity;

class Priority {

    const PRIORITY_LOW = 20;
    const PRIORITY_MEDIUM = 21;
    const PRIORITY_HIGH = 22;

    static public $priorities = array(
        self::PRIORITY_LOW => 'priority.low',
        self::PRIORITY_MEDIUM => 'priority.medium',
        self::PRIORITY_HIGH => 'priority.high',
    );

}