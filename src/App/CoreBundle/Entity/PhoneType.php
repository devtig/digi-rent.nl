<?php

namespace App\CoreBundle\Entity;

class PhoneType {
    
    const PHONE_MOBILE = 10;
    const PHONE_LINE = 11;
    const PHONE_SKYPE = 12;
    const PHONE_FAX = 13;

    static public $phoneTypes = array(
        self::PHONE_MOBILE => 'phoneType.mobile',
        self::PHONE_LINE => 'phoneType.line',
        self::PHONE_SKYPE => 'phoneType.skype',
        self::PHONE_FAX => 'phoneType.fax',
    );
}