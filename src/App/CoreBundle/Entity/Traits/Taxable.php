<?php

namespace App\CoreBundle\Entity\Traits;

use App\CoreBundle\Entity\Tax;

trait Taxable {

    /**
     * @var integer
     *
     * @ORM\Column(name="tax", type="smallint", nullable=false)
     */
    protected $tax;

    /**
     * Set tax
     *
     * @param integer $tax
     *
     * @return $this
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * Set tax string
     *
     * @param string $tax
     *
     * @return $this
     */
    public function setTaxString($tax)
    {
        $tax = \array_search(\strtolower($tax), Tax::$taxes);
        if ($tax > 0) {
            $this->setTax($tax);
        }
        return $this;
    }
    /**
     * Get tax
     *
     * @return integer
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Get tax string
     *
     * @return string
     */
    public function getTaxString()
    {
        if (isset(Tax::$taxes[$this->tax])) {
            return Tax::$taxes[$this->tax];
        }
        return Tax::$taxes[0];
    }
}