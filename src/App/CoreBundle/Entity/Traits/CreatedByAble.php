<?php

namespace App\CoreBundle\Entity\Traits;

use Shared\UserBundle\Entity\User;

trait CreatedByAble {

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Shared\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * Set created_by
     *
     * @param User $createdBy
     *
     * @return $this
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get created_by
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}