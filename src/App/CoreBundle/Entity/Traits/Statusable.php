<?php

namespace App\CoreBundle\Entity\Traits;

use App\CoreBundle\Entity\Status;

trait Statusable {

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    protected $status;

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Set status string
     *
     * @param string $status
     *
     * @return $this
     */
    public function setStatusString($status)
    {
        $status = \array_search(\strtolower($status), Status::$statuses);
        if ($status > 0) {
            $this->setStatus($status);
        }
        return $this;
    }
    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status string
     *
     * @return string
     */
    public function getStatusString()
    {
        if (isset(Status::$statuses[$this->status])) {
            return Status::$statuses[$this->status];
        }
        return Status::$statuses[0];
    }
}