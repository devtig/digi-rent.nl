<?php

namespace App\CoreBundle\Entity\Traits;

trait Sluggable {

    /**
     * @var string
     *
     * @Gedmo\Mapping\Annotation\Slug(fields={"name"})
     * @ORM\Column(name="slug", length=128, unique=true)
     */
    protected $slug;

    /**
     * get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

}