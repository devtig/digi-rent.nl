<?php

namespace App\CoreBundle\Entity\Traits;

use App\CoreBundle\Entity\Priority;

trait Priorable {

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="smallint", nullable=false)
     */
    protected $priority;

    /**
     * Set priority.
     *
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }
    /**
     * Set priority string.
     *
     * @param string $priority
     */
    public function setPriorityString($priority)
    {
        $priority = \array_search(\strtolower($priority), Priority::$priorities);
        if ($priority > 0) {
            $this->setPriority($priority);
        }
    }
    /**
     * Get priority.
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
    /**
     * Get priority string.
     *
     * @return string
     */
    public function getPriorityString()
    {
        if (isset(Priority::$priorities[$this->priority])) {
            return Priority::$priorities[$this->priority];
        }
        return Priority::$priorities[0];
    }

}