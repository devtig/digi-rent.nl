<?php

namespace App\CoreBundle\Entity;

use Simpleweb\SaaSBundle\Entity\Plan as BasePlan;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="core_plans")
 */
class Plan extends BasePlan
{
    /**
     * @ORM\OneToMany(targetEntity="App\CoreBundle\Entity\Subscription", mappedBy="plan")
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    protected $subscriptions;
}
