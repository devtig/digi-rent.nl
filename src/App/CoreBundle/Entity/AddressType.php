<?php

namespace App\CoreBundle\Entity;

class AddressType {

    const ADDRESSTYPE_VISIT = 10;
    const ADDRESSTYPE_WAREHOUSE = 11;
    const ADDRESSTYPE_PICKUP = 12;

    static public $addressTypes = array(
        self::ADDRESSTYPE_VISIT => 'addressType.visit',
        self::ADDRESSTYPE_WAREHOUSE => 'addressType.warehouse',
        self::ADDRESSTYPE_PICKUP => 'addressType.pickup',
    );

    static public function getDefault(){
        return self::ADDRESSTYPE_VISIT;
    }
}