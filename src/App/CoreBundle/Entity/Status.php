<?php

namespace App\CoreBundle\Entity;

class Status {

    // Ticket statusses
    const STATUS_NONE = 0;
    const STATUS_OPEN = 10;
    const STATUS_IN_PROGRESS = 11;
    const STATUS_ON_HOLD = 12;
    const STATUS_CLOSED = 13;

    // Product statusses
    const STATUS_AVAILABLE = 21;

    // Project statusses
    const STATUS_SENT = 31;

    static public $statuses = array(
        self::STATUS_NONE => 'status.none',
        self::STATUS_OPEN => 'status.open',
        self::STATUS_IN_PROGRESS => 'status.inprogress',
        self::STATUS_ON_HOLD => 'status.onhold',
        self::STATUS_CLOSED => 'status.closed',
        self::STATUS_AVAILABLE => 'status.available',
        self::STATUS_SENT => 'status.available',
    );

}