<?php

namespace App\CoreBundle\Entity;

class Tax {

    // Dutch Taxes
    const NL_TAX_LOW = 6;
    const NL_TAX_HIGH = 21;

    static public $taxes = array(
        self::NL_TAX_LOW => 'tax.dutchLow',
        self::NL_TAX_HIGH => 'tax.dutchHigh',
    );

}