<?php

namespace App\CoreBundle\Entity;

use FOS\UserBundle\Model\UserInterface;
use Simpleweb\SaaSBundle\Entity\PlanInterface;
use Simpleweb\SaaSBundle\Entity\Subscription as BaseSubscription,
    Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Plan;

/**
 * @ORM\Entity
 * @ORM\Table(name="company_subscriptions")
 */
class Subscription extends BaseSubscription
{
    /**
     * @ORM\ManyToOne(targetEntity="App\CoreBundle\Entity\Plan", inversedBy="subscriptions")
     */
    protected $plan;

}