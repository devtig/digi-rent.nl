<?php

namespace App\CoreBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Tests for menu builders
 */
class MenuBuilderTest extends WebTestCase
{
    /**
     * Tests if the latests posts appear in the blog sidebar
     *
     * @dataProvider getBlogUrls
     */
    public function testBlogSidebarLatests($url)
    {
        $name = "blog-sidebar-latests";
        $this->checkBlogSidebar($name, $url);
    }

    /**
     * Tests if the categories appear in the blog sidebar
     *
     * @dataProvider getBlogUrls
     */
    public function testBlogSidebarCategories($url)
    {
        $name = "blog-sidebar-categories";
        $this->checkBlogSidebar($name, $url);
    }

    /**
     * Tests if the tags appear in the blog sidebar
     *
     * @dataProvider getBlogUrls
     */
    public function testBlogSidebarTags($url)
    {
        $name = "blog-sidebar-tags";
        $this->checkBlogSidebar($name, $url);
    }

    /**
     * Creates the test suite for blog sidebar
     */
    public function checkBlogSidebar($name, $url)
    {
        $client = self::createClient();
        $crawler = $client->request('GET', $url);

        $this->assertGreaterThan(
            0,
            $crawler->filter('#' . $name)->count(),
            sprintf('The page %s shows the ' . $name . ' menu', $url)
        );

        $this->assertGreaterThan(
            0,
            $crawler->filter('#' . $name .' li')->count(),
            sprintf('In page %s , the ' . $name . ' menu shows items', $url)
        );
    }

    public function getBlogUrls()
    {
        return array(
            array('/en/blog/'),
            array('/en/blog/post/cras-dignissim-vestibulum-ultrices'),
            array('/en/blog/category/vestibulum'),
            array('/en/blog/tag/volutpat'),
        );
    }
}