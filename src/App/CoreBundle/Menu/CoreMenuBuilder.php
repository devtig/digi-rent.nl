<?php namespace App\CoreBundle\Menu;

use App\CoreBundle\Services\CreateMenuHelper;

class CoreMenuBuilder {

    private $menuHelper;

    function __construct(CreateMenuHelper $menuHelper)
    {
        $this->menuHelper = $menuHelper;
    }

    public function createTopMenu(array $options)
    {
        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
//            array('Instellingen' => 'static',
//                'icon' => 'cog',
//                'attributes' => array(
//                    'route' => 'app_settings_index',
//                    'update' => 2,
//                ),
//            ),
            array('Project' => 'dropdown',
                'hideLabel' => false,
                'icon' => 'caret-down',
                'attributes' => array(
                ),
                'class' => 'hidden-xs hidden-sm'
            ),
            array('Berichten' => 'dropdown',
                'hideLabel' => true,
                'icon' => 'inbox',
                'attributes' => array(
                ),
                'class' => 'hidden-xs'
            ),
            array('Gesprekken' => 'dropdown',
                'hideLabel' => true,
                'icon' => 'comments',
                'attributes' => array(
                ),
                'class' => 'hidden-xs'
            ),
            array('Account' => 'dropdown',
                'hideLabel' => true,
                'icon' => 'user',
                'attributes' => array(
                    'Account' => array(
                        'icon' => 'user',
                        'route' => 'app_account_profile',
                    ),
                    'Bedrijf' => array(
                        'icon' => 'suitcase',
                        'route' => 'app_company_profile',
                        'update' => 2,
                    ),
                    'Support' => array(
                        'icon' => 'comments',
                        'divider_prepend' => true,
                        'route' => 'app_account_support',
                        'update' => 2,
                    ),
                    'Instellingen' => array(
                        'icon' => 'cog',
                        'route' => 'app_settings_index',
                    ),
                    'Uitloggen' => array(
                        'icon' => 'sign-out',
                        'route' => 'app_user_logout',
                    ),
                ),
            ),
        );
        return $this->menuHelper->generateMenu(array('class' => 'nav navbar-right'), $menuItems);
    }

    public function createSideMenu(array $options)
    {
        $menuItems = array(
            array('DASHBOARD' => 'static',
                'icon' => 'dashboard',
                'attributes' => array(
                    'route' => 'app_index',
                ),
            ),
            array('PLANNING' => 'static',
                'icon' => 'calendar',
                'attributes' => array(
                    'route' => 'app_settings_index',
                ),
            ),
            array('PROJECTEN' => 'static',
                'icon' => 'archive',
                'attributes' => array(
                    'route' => 'app_settings_index',
                ),
            ),
            array('CATALOGUS' => 'static',
                'icon' => 'book',
                'attributes' => array(
                    'route' => 'app_catalog_index',
                ),
            ),
            array('ADMINISTRATIE' => 'static',
                'icon' => 'bar-chart-o',
                'attributes' => array(
                    'route' => 'app_administration_index',
                ),
            ),
            array('RELATIES' => 'static',
                'icon' => 'users',
                'attributes' => array(
                    'route' => 'app_settings_index',
                ),
            ),
            array('STATISTIEKEN' => 'static',
                'icon' => 'eye',
                'attributes' => array(
                    'route' => 'app_settings_index',
                ),
            ),
            array('APPS' => 'static',
                'icon' => 'gg',
                'attributes' => array(
                    'route' => 'app_settings_index',
                ),
            ),
        );

        return $this->menuHelper->generateMenu(
            array('class' => 'nav navbar-nav side-nav', 'id' => 'side'),
            $menuItems
        );
    }


//
//    public function createMenu($menuItems, ItemInterface $menu){
//        foreach ($menuItems as $menuItem) {
//            $menuItemName = key( array_slice( $menuItem, 0, 1, true ));
//
//            // Is menu item of type array? If so it is an dropdown item, if not, it's a static item
//            if($menuItem[$menuItemName] == 'static'){
//                $menu->addChild($menuItemName, array('route' => $menuItem['attributes']['route']))
//                    ->setAttribute('hideLabel', true)
//                    ->setAttribute('icon', 'fa fa-'.$menuItem['icon']);
//                if(isset($menuItem['update'])){
//                    $menu[$menuItemName]->setAttribute('update', $menuItem['update']);
//                }
//            }else if($menuItem[$menuItemName] == 'dropdown'){
//                $menu->addChild($menuItemName, array('uri' => "#" ) )
//                    ->setAttribute('dropdown', true)
//                    ->setAttribute('class', 'dropdown-top')
//                    ->setAttribute('icon', 'fa fa-'.$menuItem['icon']);
//
//                $attributeArray = [];
//                $updateCount = 0;
//
//                $i = 0;
//                $y = 1;
//                //Submenu items of dropdown
//                foreach($menuItem['attributes'] as $attributes){
//
//                    $attributeArray[] = key( array_slice($menuItem['attributes'], $i, $y++, true ));
//                    $updateCount = $this->addAttributes($menu, $menuItemName, $attributeArray[$i], $attributes, $updateCount);
//                    $i++;
//                }
//                //Are there any updates?
//                if($updateCount > 0){
//                    $menu[$menuItemName]->setAttribute('update', $updateCount);
//                }
//            }
//        }
//        return $menu;
//    }
//
//
//    public function addAttributes(ItemInterface $menu, $menuItemName, $subMenuItemName, $attributes, $updateCount)
//    {
//
//        if(isset($attributes['route'])){
//            $menu[$menuItemName]->addChild($subMenuItemName, array('route' => $attributes['route']));
//        }
//
//        // Maybe in future, check here for URI value
//        if(isset($attributes['icon'])){
//            $menu[$menuItemName][$subMenuItemName]->setAttribute('icon', 'fa fa-'.$attributes['icon']);
//        }
//        if(isset($attributes['update'])){
//            $menu[$menuItemName][$subMenuItemName]->setAttribute('update', $attributes['update']);
//
//            // Sums up update values for menu item update value
//            $updateCount += $attributes['update'];
//        }
//
//        return $updateCount;
//    }


//    public function createSideMenu(array $options)
//    {
//
//        $menu = $this->factory->createItem('root');
//        $menu->setChildrenAttributes(array('class' => 'nav navbar-nav side-nav', 'id' => 'side'));
//
//        $sideMenuItems = array(
//            array(
//                'DASHBOARD' => 'dashboard',
//                array(
//                    'route' => 'app_settings_index',
//                    'update' => 2,
//                ),
//            ),
//            array(
//                'MAGAZIJN' => 'cubes',
//                array(
//                    'route' => 'app_settings_index',
//                    'update' => 2,
//                ),
//            ),
//            array(
//                'PLANNING' => 'calendar',
//                array(
//                    'route' => 'app_settings_index',
//                    'update' => 2,
//                ),
//            ),
//            array(
//                'PROJECTEN' => 'archive',
//                array(
//                    'route' => 'app_settings_index',
//                    'update' => 2,
//                ),
//            ),
//            array(
//                'RELATIES' => 'users',
//                array(
//                    'route' => 'app_settings_index',
//                    'update' => 2,
//                ),
//            ),
//        );
//
//        foreach($sideMenuItems as $menuItem){
//            $menuItemName = key( array_slice( $menuItem, 0, 1, true ));
//
////            dump($menuItem[$menuItemName]);
//            $menu->addChild('DASHBOARD', array('route' => 'app_index'))
//                ->setAttribute('userprofile_prepend', true)
//                ->setAttribute('icon', 'fa fa-dashboard');
//        }
//
//
//
//
////        // Get current division
////        $division = $this->getDivision();
////
////        // Generate dashboard template (re-used)
////        $menu = $this->generateMenuFirstPart($menu, $factory, $division);
//
//        // Generate specific dashboard menu items
////        $menu = $this->dashMenu($menu);
//
////        $menu = $this->generateMenuLastPart($menu, $factory, $division);
//
//        return $menu;
//    }

//    public function dashMenu(ItemInterface $menu)
//    {
//        $menu->addChild('Portfolio', array('uri' => '#portfolio'))
//            ->setAttributes(array(
//                'icon' => 'fa fa-file-o',
//            ));
//        $menu->addChild('Catalogus', array('uri' => 'javascript:;' ))
//            ->setChildrenAttributes(array(
//                'class' => 'nav collapse',
//                'id' => 'catalogus',
//            ))
//            ->setLinkAttributes(array(
//                'class' => 'accordion-toggle collapsed',
//                'data-parent' => '#side',
//                'data-toggle' => 'collapse',
//                'data-target' => '#catalogus',
//            ))
//            ->setAttributes(array(
//                'class' => 'panel',
//                'icon' => 'fa fa-book',
//                'dropdown' => true
//            ));
//
//        $menu['Catalogus']->addChild('Categorieën', array('route' => 'app_index'))
//            ->setAttributes(array(
//                'icon' => 'fa fa-angle-double-right',
//            ));
//        $menu['Catalogus']->addChild('Producten', array('route' => 'app_index'))
//            ->setAttributes(array(
//                'icon' => 'fa fa-angle-double-right',
//            ));
//        $menu['Catalogus']->addChild('Tags', array('route' => 'app_index'))
//            ->setAttributes(array(
//                'icon' => 'fa fa-angle-double-right',
//            ));
//        $menu['Catalogus']->addChild('Specificaties', array('route' => 'app_index'))
//            ->setAttributes(array(
//                'icon' => 'fa fa-angle-double-right',
//            ));
//        $menu['Catalogus']->addChild('Referenties', array('uri' => '#'))
//            ->setAttributes(array(
//                'icon' => 'fa fa-angle-double-right',
//            ));
//        $menu['Catalogus']->addChild('Merken', array('route' => 'app_index'))
//            ->setAttributes(array(
//                'icon' => 'fa fa-angle-double-right',
//            ));
//
//        return $menu;
//    }

//    public function blogSidebarLatests(array $options)
//    {
//        $menu = $this->factory->createItem('root');
//        $menu->setChildrenAttributes(array(
//            'class' => 'list-unstyled',
//            'id' => 'blog-sidebar-latests'
//        ));
//        $latests = $this->em->getRepository('AppBundle:Post')->getLatest(3);
//        foreach ($latests as $latest)
//        {
//            $menu->addChild($latest['title'], array(
//                'route'           => 'blog_post',
//                'routeParameters' => array('slug' => $latest['slug'])
//            ));
//        }
//        return $menu;
//    }
//    public function blogSidebarCategories(array $options)
//    {
//        $menu = $this->factory->createItem('root');
//        $menu->setChildrenAttributes(array(
//            'class' => 'list-unstyled',
//            'id' => 'blog-sidebar-categories'
//        ));
//        $categories = $this->em->getRepository('AppBundle:Category')->findActive();
//        foreach ($categories as $category)
//        {
//            $menu->addChild($category->getName(), array(
//                'route'           => 'blog_category',
//                'routeParameters' => array('slug' => $category->getSlug())
//            ));
//        }
//        return $menu;
//    }
//    public function blogSidebarTags(array $options)
//    {
//        $menu = $this->factory->createItem('root');
//        $menu->setChildrenAttributes(array(
//            'class' => 'list-unstyled',
//            'id' => 'blog-sidebar-tags'
//        ));
//        $tags = $this->em->getRepository('AppBundle:Tag')->findActive();
//        foreach ($tags as $tag)
//        {
//            $menu->addChild($tag->getName(), array(
//                'route'           => 'blog_tag',
//                'routeParameters' => array('slug' => $tag->getSlug())
//            ));
//        }
//        return $menu;
//    }
}