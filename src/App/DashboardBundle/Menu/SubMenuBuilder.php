<?php namespace App\DashboardBundle\Menu;

use App\CoreBundle\Services\CreateMenuHelper;

class SubMenuBuilder
{
    private $menuHelper;

    function __construct(CreateMenuHelper $menuHelper)
    {
        $this->menuHelper = $menuHelper;
    }

    public function createMenu(array $options)
    {
        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
            array('Overzicht' => 'tab',
                'attributes' => array(
                    'uri' => 'index',
                ),
            ),
            array('Activiteiten' => 'tab',
                'attributes' => array(
                    'uri' => 'profile'
                ),
            ),
            array('Instellingen' => 'tab',
                'icon' => 'wrench',
                'attributes' => array(
                    'route' => 'app_settings_index',
                    'class' => 'page_settings'
                ),
            ),
        );

        return $this->menuHelper->generateMenu(array('class' => 'nav nav-tabs nav-tabs-small'), $menuItems);
    }
}