<?php

namespace App\DashboardBundle\Controller;

use Hip\MandrillBundle\Dispatcher;
use Hip\MandrillBundle\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DashboardController extends Controller
{


    public function indexAction()
    {

//        $dispatcher = $this->get('hip_mandrill.dispatcher');
//
//        $message = new Message();
//
//        $message
//            ->setFromEmail('no-reply@digi-rent.nl')
//            ->setFromName('Customer Care')
//            ->addTo('peter@uniq-rental.nl')
//            ->setSubject('Some Subject')
//            ->setHtml('<html><body><h1>Some Content</h1></body></html>');
//
//        $result = $dispatcher->send($message);

        return $this->render('DashboardBundle:Dashboard:dashboard.html.twig');
    }
}
