<?php namespace App\CompanyBundle\Menu;

use App\CoreBundle\Services\CreateMenuHelper;

class SubMenuBuilder
{

    private $menuHelper;

    function __construct(CreateMenuHelper $menuHelper)
    {
        $this->menuHelper = $menuHelper;
    }

    public function createMenu(array $options)
    {
        // Menu items, each wrapped in an array for generation process
        $menuItems = array(
            array('Profiel' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_profile',
                ),
            ),
            array('Magazijn' => 'tab',
                'attributes' => array(
                    'route' => 'app_warehouse_index',
                ),
            ),
            array('Personeel' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_employees'
                ),
            ),
            array('Wagenpark' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_vehicles',
                    'update' => 2,
                ),
            ),
            array('Wagenpark' => 'tab',
                'attributes' => array(
                    'route' => 'app_company_vehicles',
                    'update' => 2,
                ),
            ),
        );
        return $this->menuHelper->generateMenu(array('class' => 'nav nav-tabs border-bottom'), $menuItems);
    }
}