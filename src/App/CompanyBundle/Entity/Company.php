<?php

namespace App\CompanyBundle\Entity;

use App\CoreBundle\Entity\Branche;
use App\SharedBundle\Entity\Phone;
use App\SharedBundle\Entity\Interfaces\MultiAddressInterface;
use Shared\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;
use App\SharedBundle\Entity\Traits as SharedTraits;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\CompanyBundle\Entity\Traits as CompanyTraits;

/**
 * Company
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="App\CompanyBundle\Repository\CompanyRepository")
 * @UniqueEntity(
 *     fields={"subdomainName"},
 *     message="Domeinnaam is reeds geregistreerd, kies een andere naam."
 * )
 * @UniqueEntity(
 *     fields={"kvk"},
 *     message="Kvk-nummer is reeds geregistreerd."
 * )
 *
 * @Vich\Uploadable
 */
class Company implements MultiAddressInterface
{
    // Non-referencing Traits
    use Traits\Identifiable,
        Traits\TimeStampable,
        CompanyTraits\Subscribable,
        Traits\NameAble;

    // Referencing Traits
    use SharedTraits\MultiAddressable;

    public function __construct() {
        $this->addresses = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->phonenumbers = new ArrayCollection();
        $this->created_at = new \DateTime('now');
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="company_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $imageName;

    /**
     * @var Branche
     *
     * @ORM\ManyToOne(targetEntity="App\CoreBundle\Entity\Branche", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="branche_id", referencedColumnName="id")
     * @Assert\Type(type="App\CoreBundle\Entity\Branche")
     * @Assert\Valid()
     */
    private $branche;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\SharedBundle\Entity\Phone", cascade={"persist"})
     * @ORM\JoinTable(name="company_phonenumbers",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="phonenumber_id", referencedColumnName="id", unique=true)}
     *  )
     */
    private $phonenumbers;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Shared\UserBundle\Entity\User", mappedBy="company")
     */
    private $users;

    /**
     *
     * @var int
     *
     * @ORM\Column(name="kvk", type="integer", length=8, unique=true, nullable=true)
     */
    private $kvk;

    /**
     * @var string
     *
     * @ORM\Column(name="btw", type="string", length=255, nullable=true)
     */
    private $btw;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^[a-z]+$/",
     *     match=true,
     *     message="Subdomeinnaam bevat verkeerde tekens (alleen letters)."
     * )
     *
     * @ORM\Column(name="subdomain_name", type="string", length=255, unique=true, nullable=false)
     */
    private $subdomainName;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_account", type="string", length=255, nullable=true)
     */
    private $bankAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_bic", type="string", length=255, nullable=true)
     */
    private $bankBic;


    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;



    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Company
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated_at = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get ImageFile
     *
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Company
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get ImageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }


    /**
     * @return Branche
     */
    public function getBranche()
    {
        return $this->branche;
    }

    /**
     * @param Branche $branche
     */
    public function setBranche(Branche $branche)
    {
        $this->branche = $branche;
    }

    /**
     * Get phonenumbers
     *
     * @return ArrayCollection
     */
    public function getPhonenumbers()
    {
        return $this->phonenumbers;
    }

    /**
     * Set phonenumber
     *
     * @param Phone $phone
     * @return Company
     */
    public function addPhone(Phone $phone)
    {
        if (!$this->phonenumbers->contains($phone)) {
            $this->phonenumbers[] = $phone;
        }
    }

    /**
     * Remove phonenumber
     *
     * @param  Phone $phone
     * @return Company
     */
    public function removePhone(Phone $phone){
        if ($this->phonenumbers->contains($phone)) {
            $this->phonenumbers->removeElement($phone);
        }
    }


    /**
     * Get users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add user
     *
     * @param User $user
     * @return Company
     */
    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }
    }

    /**
     * Remove user
     *
     * @param  User $user
     * @return Company
     */
    public function removeUser(User $user){
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
    }

    /**
     * Get kvk
     *
     * @return int
     */
    public function getKvk()
    {
        return $this->kvk;
    }

    /**
     * Set kvk
     *
     * @param int $kvk
     * @return Company
     */
    public function setKvk($kvk)
    {
        $this->kvk = $kvk;
    }

    /**
     * Get btw
     *
     * @return string
     */
    public function getBtw()
    {
        return $this->btw;
    }

    /**
     * Set btw
     *
     * @param string $btw
     * @return Company
     */
    public function setBtw($btw)
    {
        $this->btw = $btw;
    }

    /**
     * Get subdomainName
     *
     * @return string
     */
    public function getSubdomainName()
    {
        return strtolower($this->subdomainName);
    }

    /**
     * Set subdomainName
     *
     * @param  string $subdomainName
     * @return User
     */
    public function setSubdomainName($subdomainName)
    {
        $this->subdomainName = strtolower($subdomainName);
    }


    /**
     * Get bankaccount
     *
     * @return string
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * Set bankaccount
     *
     * @param string $bankAccount
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    /**
     * Get bankbic
     *
     * @return string
     */
    public function getBankBic()
    {
        return $this->bankBic;
    }

    /**
     * Set bankbic
     *
     * @param string $bankBic
     * @return Company
     */
    public function setBankBic($bankBic)
    {
        $this->bankBic = $bankBic;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set website
     *
     * @param string $website
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }
}

