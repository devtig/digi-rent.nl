<?php

namespace App\CompanyBundle\Entity\Traits;

use App\CoreBundle\Entity\Subscription;
use Doctrine;
use FOS;


trait Subscribable
{

    /**
     * @ORM\OneToMany(targetEntity = "App\CoreBundle\Entity\Subscription", mappedBy = "company", cascade = {"persist"})
     * @ORM\OrderBy({"created_at" = "DESC"})
     */
    protected $subscriptions;

    /**
     * @return Subscription
     */
    public function getSubscription()
    {
        return $this->getSubscriptions()->last();
    }

    /**
     * @return Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @param Subscription $subscription
     * @return FOS\UserBundle\Model\UserInterface
     */
    public function addSubscription(Subscription $subscription)
    {
        $this->getSubscriptions()->add($subscription);
        $subscription->setUser($this);

        return $this;
    }

    /**
     * @param Subscription $subscription
     * @return FOS\UserBundle\Model\UserInterface
     */
    public function removeSubscription(Subscription $subscription)
    {
        $this->getSubscriptions()->removeElement($subscription);

        return $this;
    }
}
