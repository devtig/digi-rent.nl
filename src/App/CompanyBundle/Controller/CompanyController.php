<?php

namespace App\CompanyBundle\Controller;

use App\CompanyBundle\Entity\Company;
use App\CompanyBundle\Form\Type\CompanyProfileType;
use App\SharedBundle\Entity\Address;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CompanyController extends Controller {

    public function indexAction(Request $request){

        $em = $this->getDoctrine()->getManager('default');

        $form = $this->createForm(new CompanyProfileType(), $this->getUser()->getCompany());
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');
            $em->persist($this->getUser()->getCompany());
            $userManager->updateUser($this->getUser());

            return new RedirectResponse($this->generateUrl('app_company_profile'));
        }

        return $this->render('@Company/index.html.twig', array(
            'form' => $form->createView(),

        ));
    }

    public function employeesAction(Request $request){

        return $this->render('@Company/employees.html.twig');
    }

    public function vehiclesAction(Request $request){

        return $this->render('@Company/vehicles.html.twig');
    }

//    public function editAction(Request $request){
//
//        return $this->render('@User/App/Support/support.html.twig');
//    }

}