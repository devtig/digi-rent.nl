<?php

namespace App\CompanyBundle\Form\Type;

use App\SharedBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyProfileType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('branche')
            ->add('subdomainName')
            ->add('addresses', CollectionType::class,
                array(
                    'entry_type' => AddressType::class,
                    'allow_add'    => true,
                )
            )
            ->add('kvk')
            ->add('btw')
            ->add('submit', 'submit',
                array(
                    'label' => 'Opslaan',
                    'attr' => array(
                        'icon' => 'send'
                    )
                )
            );

        $builder->add('imageFile', 'vich_image', array(
            'required'      => false,
            'allow_delete'  => false, // not mandatory, default is true
            'download_link' => false, // not mandatory, default is true
        ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\CompanyBundle\Entity\Company',
            'error_bubbling' => true,
        ));
    }
}