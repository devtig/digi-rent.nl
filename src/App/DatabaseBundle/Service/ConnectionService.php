<?php

namespace App\DatabaseBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;

class ConnectionService {

    private $default;
    private $subdomain;
    private $defaultConnection;
    private $subdomainConnection;

    function __construct(Registry $doctrine)
    {
        $this->default = $doctrine->getManager('default');
        $this->subdomain = $doctrine->getManager('subdomain');
        $this->defaultConnection = $doctrine->getManager('default')->getConnection();
        $this->subdomainConnection = $doctrine->getManager('subdomain');
    }

    public function getRepositoryFromDefault($table, $where = array())
    {
        $condition = key(array_slice($where, 0, 1, true));

        foreach ($where as $statement) {
            $column = key(array_slice($statement, 0, 1, true));
            $data = $statement[$column];
        }

        $sql = "SELECT * FROM ".$table." ".strtoupper($condition)." ".$column." = '".$data."';";

//        dump($sql);
//        die();
        return $this->defaultConnection->fetchAll($sql);
    }

    public function getRepositoryFromSubdomain($bundle, $entity, $findBy = array()){
        $bundle = (is_null($bundle)) ? 'SubdomainBundle' : $bundle;
       return $this->subdomainConnection
            ->getRepository($bundle.':'.$entity)
            ->findOneBy($findBy);
    }

    public function insertDataToSubdomain($data = array())
    {
        $valueFields = '(company_id, name, db_name, username, password, salt, enabled)';

        $sql = "INSERT INTO dr_subdomain ".$valueFields." VALUES ".$column.";";

        return $this->defaultConnection->fetchAll($sql);
    }
}