<?php

namespace App\DatabaseBundle\Model;

use App\SubdomainBundle\Entity\Subdomain;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOException;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Symfony\Component\DependencyInjection\Container;

class DatabaseManager {

    private $connection;
    private $connectionParams;
    private $container;
    private $generator;
    private $credentials = array();
    private $secret;
    private $dbPrefix;

    public function __construct(Connection $connection, Container $container, ComputerPasswordGenerator $generator, $secret, $dbPrefix){
        $this->connection = $connection;
        $this->connectionParams = $connection->getParams();
        $this->container = $container;
        $this->generator = $generator;
        $this->secret = $secret;
        $this->dbPrefix = $dbPrefix;
    }

    public function prepareConnection(Subdomain $subdomain)
    {
        $connectPassword = $this->getConnectPassword($subdomain);

        if(!is_null($connectPassword)){
            // Check if the manager already has credentials?
            $this->credentials = array(
                'driver' => "pdo_mysql",
                'host' => "127.0.0.1",
                'port' => null,
                'charset' => 'UTF8',
                'dbname' => $subdomain->getDbName(),
                'user' => $subdomain->getUsername(),
                'password' => $connectPassword,
                'driverOptions' => array(),
                'defaultTableOptions' => array(),
            );
            $this->connect($this->credentials);
        }else{
            throw new \Doctrine\DBAL\ConnectionException;
        }
    }

    public function setConnectionForSubdomain($subdomainName){

        $subdomain = $this->container->get('dr_database.connection_service')->getRepositoryFromSubdomain('SubdomainBundle','Subdomain', array('name' => $subdomainName));
        if(is_null($subdomain)){
            return false;
        }else{
            $this->prepareConnection($subdomain);
        }
    }

    private function getConnectPassword(Subdomain $subdomain){

        $passwordManager = $this->container->get('dr_core.password_manager');
        $encoder = $passwordManager->getEncoder($subdomain);

        $plainPassword = $passwordManager->getSubdomainPassword($subdomain);

        // TODO This check makes every page load 500ms slower, the password is 100% a match, but for security reasons, do we have to check it?
        if($subdomain->getPassword() === $encoder->encodePassword($plainPassword, $subdomain->getSalt())){
            return $plainPassword;
        }else{
            return null;
        }
    }

    private function connect($credentials = array())
    {
        //TODO Switching databases should happen after succesful login!
        // we also check if the current connection needs to be closed based on various things
        // have left that part in for information here
        // $appId changed from that in the connection?
        if ($this->connection->isConnected()) {
            $this->connection->close();
        }

        // Set DB connection with credentials
        $params['driver'] = $credentials['driver'];
        $params['host'] = $credentials['host'];
        $params['port'] = $credentials['port'];
        $params['dbname'] = $credentials['dbname'];
        $params['user'] = $credentials['user'];
        $params['password'] = $credentials['password'];
        $params['charset'] = $credentials['charset'];
        $params['driverOptions'] = $credentials['driverOptions'];
        $params['defaultTableOptions'] = $credentials['defaultTableOptions'];



        // Set up the parameters for the parent
        $this->connection->__construct(
            $params, $this->connection->getDriver(), $this->connection->getConfiguration(),
            $this->connection->getEventManager()
        );

        try {
            $this->connection->connect();
        } catch (\Exception $e) {
            // log and handle exception
            dump($e);
            dump($this->credentials);
            dump($this->connection);
            dump('Something went wrong (ChangeConnection: ' . $e->getMessage());
            die();
        }
    }

    public function createDatabase($credentials = array()){
        $dbQuery =
            'CREATE DATABASE '.$credentials['dbName'].';'.
            'CREATE USER '.$credentials['username'].'@127.0.0.1 IDENTIFIED BY "'.$credentials['password'].'";'.
            'GRANT ALL PRIVILEGES ON '.$credentials['dbName'].'.* TO '.$credentials['username'].'@127.0.0.1 WITH GRANT OPTION;'.
            'FLUSH PRIVILEGES;'
        ;

        $statement = $this->connection->prepare($dbQuery);

        try{
            $statement->execute();
        }catch( \Exception $e )
        {
            // TODO catch every error for testing purposes
            return $e->getMessage();
        }

        return true;
    }
}