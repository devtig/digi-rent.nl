<?php

namespace App\DatabaseBundle\EventListener;

use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\SyntaxErrorException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class CreateDatabaseListener implements EventSubscriberInterface
{
    private $container;
    private $logger;

    public function __construct(LoggerInterface $logger, ContainerInterface $container)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_CONFIRM => 'createDatabase',
        );
    }

    public function createDatabase(GetResponseUserEvent $event)
    {
        /** @var $subdomainManager \App\SubdomainBundle\Model\SubdomainManager */
        $subdomainManager = $this->container->get('dr_subdomain.subdomain_manager');

        $this->logger->info('Yea, it totally works!');

        try {
            $subdomainManager->createSubdomain($event->getUser()->getCompany());
        }catch (\Exception $e) {
            switch($e) {
                case $e instanceof UniqueConstraintViolationException:
                    dump('Oops, something went wrong... The domain '.$event->getUser()->getCompany().' already exists.');
                    // TODO maybe update the domain then or throw custom event to notify the client and myself?
                    break;
                default:
                    dump('All okay');
                    break;
            }
        }

        // TODO Redirect to subdomain url?

        try{
            $databaseManager = $this->container->get('dr_database.database_manager');
            $databaseManager->createDatabase($subdomainManager->getCredentials());
            $databaseManager->prepareConnection($subdomainManager->getSubdomain($event->getUser()->getCompany()->getSubdomainName()));
            try{
                $em = $this->container->get('doctrine.orm.dynamic_entity_manager');
                $tool = new SchemaTool($em);
                $tool->createSchema($em->getMetadataFactory()->getAllMetadata());
            }catch (ToolsException $e){
                dump($e->getMessage());
                die();
            }
        }catch (\Exception $e){
            switch($e) {
                case $e instanceof PDOException:
                        dump($errorCode = $e->getErrorCode());
                        dump($sqlState = $e->getSQLState());
                        die();
                    break;
                case $e instanceof SyntaxErrorException:
                        dump($errorCode = $e->getErrorCode());
                        dump($sqlState = $e->getSQLState());
                        die();
                    break;
                default:
                    dump('All okay');
                    break;
            }

//            if(!is_null($errorCode) || !is_null($sqlState)){
//                $errorMessage = preg_replace('/(.*)]: (.*):(.*)/sm', '\2', $e->getMessage());
//                // TODO notify visitor something went wrong, sent email with details
//            }
        }
    }
}