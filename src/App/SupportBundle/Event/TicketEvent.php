<?php

namespace App\SupportBundle\Event;

use App\SupportBundle\Entity\Ticket;
use Symfony\Component\EventDispatcher\Event;

class TicketEvent extends Event {

    protected $ticket;

    public function __construct(Ticket $ticket){
        $this->ticket = $ticket;
    }

    public function getTicket(){
        return $this->ticket;
    }
}