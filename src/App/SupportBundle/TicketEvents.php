<?php

namespace App\SupportBundle;


final class TicketEvents {

    /**
     * The hackzilla.ticket.create event is thrown each time an ticket is created
     * in the system.
     *
     * The hackzilla.ticket.update event is thrown each time an ticket is updated
     * in the system.
     *
     * The hackzilla.ticket.delete event is thrown each time an ticket is deleted
     * in the system.
     *
     * The event listeners receives an
     * SupportBundle\Event\TicketEvent instance.
     *
     * @var string
     */
    const TICKET_CREATE = 'digirent.ticket.create';
    const TICKET_UPDATE = 'digirent.ticket.update';
    const TICKET_DELETE = 'digirent.ticket.delete';
}