<?php

namespace App\SupportBundle\Form\Ticket;

use App\SupportBundle\Entity\Ticket;
use App\SupportBundle\Form\Ticket\TicketMessageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;


class TicketType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('subject')
            ->add('messages', CollectionType::class,
                array(
                    'entry_type' => TicketMessageType::class,
                    'label' => 'Message',
                    'options' => array('label' => false)
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Ticket::class,
                'new_ticket' => false,
            ]
        );
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }
    public function getBlockPrefix()
    {
        return 'ticket';
    }
}