<?php

namespace App\SupportBundle\Form\Ticket;

use App\SupportBundle\Entity\TicketMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class TicketMessageType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextareaType::class,
                array(
                'label' => false,
                )
            ) ->add('submit', 'submit',
                array(
                    'label' => 'Verstuur',
                    'attr' => array(
                        'icon' => 'send'
                    )
                )
            );
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => TicketMessage::class,
            ]
        );
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }
    public function getBlockPrefix()
    {
        return 'ticketMessage';
    }
}