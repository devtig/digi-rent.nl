<?php

namespace App\SupportBundle\Form\Type;

use App\SupportBundle\Entity\Ticket;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;

class TicketType extends AbstractType {

    private $userManager;
    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'subject',
                method_exists(AbstractType::class, 'getBlockPrefix') ? TextType::class : 'text',
                [
                    'label' => 'LABEL_SUBJECT',
                ]
            )
            ->add(
                'messages',
                method_exists(AbstractType::class, 'getBlockPrefix') ? CollectionType::class : 'collection',
                [
                    method_exists(AbstractType::class, 'getBlockPrefix') ? 'entry_type' : 'type'       => method_exists(
                        AbstractType::class,
                        'getBlockPrefix'
                    ) ? TicketMessageType::class : new TicketMessageType($this->userManager),
                    method_exists(AbstractType::class, 'getBlockPrefix') ? 'entry_options' : 'options' => [
                        'new_ticket' => true,
                    ],
                    'label'                                                                            => false,
                    'allow_add'                                                                        => true,
                ]
            );
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Ticket::class,
            ]
        );
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }
    public function getBlockPrefix()
    {
        return 'ticket';
    }

}