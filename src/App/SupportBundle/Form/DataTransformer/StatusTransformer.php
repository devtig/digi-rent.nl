<?php

namespace App\SupportBundle\Form\DataTransformer;

use App\CoreBundle\Entity\Status;
use Symfony\Component\Form\DataTransformerInterface;

class StatusTransformer implements DataTransformerInterface
{
    /**
     * Transforms checkbox value into Ticket Message Status Closed.
     *
     * @param int $number
     *
     * @return int|null
     */
    public function transform($number)
    {
        if ($number == Status::STATUS_CLOSED) {
            return 1;
        }
        return;
    }
    /**
     * Transforms Ticket Message Status Closed into checkbox value checked.
     *
     * @param int $number
     *
     * @return int|null
     */
    public function reverseTransform($number)
    {
        if ($number == 1) {
            return Status::STATUS_CLOSED;
        }
        return;
    }
}