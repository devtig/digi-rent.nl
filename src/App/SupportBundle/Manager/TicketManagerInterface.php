<?php

namespace App\SupportBundle\Manager;

use App\SupportBundle\Entity\Ticket;

interface TicketManagerInterface {

    public function createTicket();
    public function createMessage(Ticket $ticket);
    public function updateTicket(Ticket $ticket);
    public function deleteTicket(Ticket $ticket);
    public function findTickets();
    public function findTicketsBy(array $criteria);

}