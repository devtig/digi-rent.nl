<?php

namespace App\SupportBundle\Manager;

use App\SupportBundle\Entity\Ticket;
use App\SupportBundle\Entity\TicketMessage;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class TicketManager implements TicketManagerInterface {

    private $tokenstorage;
    public function __construct(TokenStorage $tokenStorage, ObjectManager $om)
    {
        $this->tokenstorage = $tokenStorage;
        $this->ObjectManager = $om;
        $this->repository = $om->getRepository('SupportBundle:Ticket');
    }

    /**
     * Create a new instance of Ticket entity
     */
    public function createTicket()
    {
        $ticket = new Ticket($this->tokenstorage->getToken()->getUser());
        $ticket->addMessage($this->createMessage($ticket));
        return $ticket;
    }
    /**
     * Create a new instance of TicketMessage Entity
     */
    public function createMessage(Ticket $ticket)
    {
        return new TicketMessage($ticket);
    }

    /**
     * Update or Create a Ticket in the database
     * Update or Create a TicketMessage in the database
     *
     * @param Ticket $ticket
     * @param TicketMessage $message
     *
     * @return Ticket
     */
    public function updateTicket(Ticket $ticket, TicketMessage $message = null)
    {
        if (!\is_null($ticket)) {
            $this->ObjectManager->persist($ticket);
        }
        if (!\is_null($message)) {
            $this->ObjectManager->persist($message);
        }
        $this->ObjectManager->flush();
        return $ticket;
    }

    /**
     * Delete a ticket from the database
     *
     * @param Ticket $ticket
     */
    public function deleteTicket(Ticket $ticket)
    {
        $this->ObjectManager->remove($ticket);
        $this->ObjectManager->flush();
    }

    /**
     * Find all tickets in the database
     *
//     * @return array|\Hackzilla\Bundle\SupportBundle\Entity\Ticket[]
     * @return array
     */
    public function findTickets()
    {
        return $this->repository->findAll();
    }
    /**
     * Find ticket by criteria
     *
     * @param array $criteria
     * @return array
     */
    public function findTicketsBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

}