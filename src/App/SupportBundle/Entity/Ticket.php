<?php

namespace App\SupportBundle\Entity;

use App\CoreBundle\Entity\Priority;
use App\CoreBundle\Entity\Status;
use Shared\UserBundle\Entity\User;
use App\CoreBundle\Entity\Traits as Traits;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\UserInterface;
use JMS\Serializer\Annotation\AccessorOrder;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;


/**
 * Ticket
 * @AccessorOrder("custom", custom = {"id", "subject", "priority", "status", "assignedTo", "createdBy", "createdAt", "updatedAt", "messages" })
 * @Gedmo\Mapping\Annotation\Loggable(logEntryClass="TicketLogEntry")
 * @ORM\Table(name="ticket")
 * @ORM\Entity(repositoryClass="App\SupportBundle\Repository\TicketRepository")
 */
class Ticket {

    use Traits\TimeStampable,
        Traits\Priorable,
        Traits\Statusable,
        Traits\CreatedByAble;

    public function __construct(User $user)
    {
        $this->setCreatedBy($user);
        $this->setPriority(Priority::PRIORITY_MEDIUM);
        $this->setStatus(Status::STATUS_OPEN);
        $this->setCreatedAt(new \DateTime());
        $this->messages = new ArrayCollection();
    }

    public function __toString()
    {
        return str_pad($this->id, 3, "0", STR_PAD_LEFT);
    }

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type = "integer")
     * @ORM\GeneratedValue(strategy = "IDENTITY")
     */
    protected $id;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var User
     * @Gedmo\Mapping\Annotation\Versioned
     *
     * @ORM\ManyToOne(targetEntity="Shared\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="assigned_to_admin_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $assignedTo;

    /**
     * @ORM\OneToMany(targetEntity="App\SupportBundle\Entity\TicketMessage", mappedBy="ticket", cascade={"persist"})
     * @ORM\OrderBy({"created_at" = "ASC"})
     */
    protected $messages;

    /**
     * @var string
     *
     * @Gedmo\Mapping\Annotation\Versioned
     * @ORM\Column(name="subject", type="string", length=255, unique=false, nullable=false)
     */
    private $subject;

    /**
     * @return User
     */
    public function getAssignedTo()
    {
        return $this->assignedTo;
    }

    /**
     * @param User $assignedTo
     */
    public function setAssignedTo(User $assignedTo)
    {
        $this->assignedTo = $assignedTo;
    }

    /**
     * Add message
     *
     * @param TicketMessage $messages
     *
     * @return Ticket
     */
    public function addMessage(TicketMessage $messages)
    {
        $this->messages[] = $messages;
        return $this;
    }

    /**
     * Remove messages.
     *
     * @param TicketMessage $messages
     */
    public function removeMessage(TicketMessage $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }


    /**
     * Set subject.
     *
     * @param string $subject
     *
     * @return Ticket
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * Get subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

}