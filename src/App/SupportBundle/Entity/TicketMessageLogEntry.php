<?php

namespace App\SupportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;
use Gedmo\Loggable\Entity\MappedSuperclass\AbstractLogEntry;

/**
 * Domain
 *
 * @ORM\Table(name="ticket_message_log")
 * @ORM\Entity(repositoryClass="App\SupportBundle\Repository\TicketMessageLogEntryRepository")
 */
class TicketMessageLogEntry extends AbstractLogEntry
{

}

