<?php

namespace App\SupportBundle\Entity;

use App\CoreBundle\Entity\Priority;
use App\CoreBundle\Entity\Status;
use App\CoreBundle\Entity\Traits as Traits;
use Shared\UserBundle\Entity\User;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @Gedmo\Mapping\Annotation\Loggable(logEntryClass="TicketMessageLogEntry")
 * @ORM\Table(name="ticket_message")
 * @ORM\Entity(repositoryClass="App\SupportBundle\Repository\TicketMessageRepository")
 */
class TicketMessage {

    use Traits\Identifiable,
        Traits\TimeStampable,
        Traits\Priorable,
        Traits\Statusable;


    public function __construct(Ticket $ticket)
    {
        $this->setPriority(Priority::PRIORITY_MEDIUM);
        $this->setTicket($ticket);
        $this->setStatus(Status::STATUS_OPEN);
        $this->setCreatedBy($ticket->getCreatedBy());
        $this->setCreatedAt(new \DateTime());
    }

    public function __toString(){
        return $this->ticket->getSubject();
    }
    /**
     * @var Ticket
     * @Exclude()
     * @ORM\ManyToOne(targetEntity="Ticket", inversedBy="messages", cascade={"persist"})
     * @ORM\JoinColumn(name="ticket_id", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $ticket;

    /**
     * @var string
     *
     * @Gedmo\Mapping\Annotation\Versioned
     * @ORM\Column(name="message", type="text", nullable=true)
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @var boolean
     */
    private $didRead;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Shared\UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * Set created_by
     *
     * @param User $createdBy
     *
     * @return $this
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get created_by
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }


    /**
     * @return mixed
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * Set ticket.
     *
     * @param Ticket $ticket
     *
     * @return $this
     */
    public function setTicket(Ticket $ticket = null)
    {
        $this->ticket = $ticket;
        return $this;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return boolean
     */
    public function isDidRead()
    {
        return $this->didRead;
    }

    /**
     * @param boolean $didRead
     */
    public function setDidRead($didRead)
    {
        $this->didRead = $didRead;
    }

}