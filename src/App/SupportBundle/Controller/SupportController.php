<?php

namespace App\SupportBundle\Controller;

use App\SupportBundle\Entity\Ticket;
use App\SupportBundle\Entity\TicketMessage;
use App\SupportBundle\Form\Ticket\TicketMessageType;
use App\SupportBundle\Form\Ticket\TicketType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SupportController extends Controller {

    public function indexAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $faqs = $em->getRepository('SupportBundle:FAQ')->findAll();
        $tickets = $em->getRepository('SupportBundle:Ticket')->findBy(array('createdBy' => $this->getUser()), array("created_at" => "DESC"));

        $ticketManager = $this->get('dr_ticket.ticket_manager');

        $ticket = $ticketManager->createTicket();

        $form = $this->createForm(new TicketType(), $ticket);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->flush();

            // Ticket Event Dispatcher, send mail to client and to administrator
            $request->getSession()->getFlashBag()->add('success', 'Ticket is aangemaakt en ingediend.');

            return $this->redirect($this->generateUrl('app_account_support')."#index");
        }

        return $this->render('SupportBundle:Support:index.html.twig', array(
            'faqs' => $faqs,
            'ticket_form' => $form->createView(),
            'tickets' => $tickets
        ));
    }

    public function getTicketAction($id){
        $serializer = $this->container->get('jms_serializer');
        $em = $this->getDoctrine()->getManager();
        $ticket = $em->getRepository('SupportBundle:Ticket')->findOneBy(array('id' => $id));
        return new JsonResponse($serializer->serialize($ticket, 'json'));
    }

    public function detailsAction(Request $request, Ticket $ticketObject = null){

        if(!$ticketObject){
            return $this->redirect($this->generateUrl('app_account_support'));
        }else{
            // TODO Checkuserpermission service?
            if($ticketObject->getCreatedBy() != $this->getUser()){
                return $this->redirect($this->generateUrl('app_account_support'));
            }
        }

        $messageForm = $this->newTicketMessageForm($request, $ticketObject);

        if($messageForm->isValid()){
            return $this->redirect($this->generateUrl('app_account_support'));
        }

        return $this->render('SupportBundle:Support/Ticket:details.html.twig', array(
                'ticket' => $ticketObject,
                'message_form' => $messageForm->createView(),
        ));
    }

    public function newTicketForm(Request $request){

        $ticket = new Ticket($this->getUser());
        $message = new TicketMessage($ticket);
        $ticket->getMessages()->add($message);

        $form = $this->createForm(new TicketType(), $ticket);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($ticket);
            $em->persist($message);
            $em->flush();

            // TODO Ticket Event Dispatcher, send mail to client and to administrator
            $request->getSession()->getFlashBag()->add('success', 'Ticket is aangemaakt en ingediend.');

            return $form;
        }

        return $form;

    }

    public function newTicketMessageForm(Request $request, $ticket){
        $message = new TicketMessage($ticket);
        $ticket->getMessages()->add($message);

        $form = $this->createForm(new TicketMessageType(), $message);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();

            // TODO Ticket Event Dispatcher, send mail to client and to administrator
            $request->getSession()->getFlashBag()->add('success', 'Bericht is verzonden.');

           return $form;
        }

        return $form;
    }
}