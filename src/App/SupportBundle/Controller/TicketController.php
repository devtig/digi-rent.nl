<?php

namespace App\SupportBundle\Controller;

use App\CoreBundle\Entity\Status;
use App\SupportBundle\Entity\Ticket;
use App\SupportBundle\Entity\TicketMessage;
use App\SupportBundle\Event\TicketEvent;
use App\SupportBundle\Form\Type\TicketMessageType;
use App\SupportBundle\Form\Type\TicketType;
use App\SupportBundle\TicketEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class TicketController extends Controller
{
    /**
     * Lists all Ticket entities.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $translator = $this->get('translator');

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $ticketState = $request->get('state', $translator->trans('ticket.open'));

//        $ticketPriority = $request->get('priority', null);
        $repositoryTicket = $this->getDoctrine()->getRepository('SupportBundle:Ticket');
        $repositoryTicketMessage = $this->getDoctrine()->getRepository('SupportBundle:TicketMessage');

        $ticketState = 'STATUS_OPEN';
        $ticketPriority = 'PRIORITY_LOW';

        $query = $repositoryTicket->getTicketList($user, $repositoryTicketMessage->getTicketStatus($translator, $ticketState));

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query->getQuery(),
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('SupportBundle:Ticket:index.html.twig', array(
            'pagination' => $pagination,
            'ticketState' => $ticketState,
            'ticketPriority' => $ticketPriority,
        ));
    }

    /**
     * Creates a new Ticket entity.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');

        $ticketManager = $this->get('dr_ticket.ticket_manager');
        $ticket = $ticketManager->createTicket();
        $form = $this->createForm(new TicketType($userManager), $ticket);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $ticket->setClient($user);

            $message = $ticket->getMessages()->current();
            $message->setStatus(Status::STATUS_OPEN);
            $message->setTicket($ticket);
            $message->setSendBy($user);

            $ticketManager->updateTicket($ticket, $message);
            $event = new TicketEvent($ticket);
            $this->get('event_dispatcher')->dispatch(TicketEvents::TICKET_CREATE, $event);
            return $this->redirect($this->generateUrl('app_ticket_show', array('id' => $ticket->getId())));
        }
        return $this->render('SupportBundle:Ticket:new.html.twig', array(
            'entity' => $ticket,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Ticket entity.
     *
     */
    public function newAction()
    {
        $entity = new Ticket();
        $userManager = $this->get('fos_user.user_manager');
        $form = $this->createForm(new TicketType($userManager), $entity);
        return $this->render('SupportBundle:Ticket:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Ticket entity.
     *
     * @param Ticket $ticket
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Ticket $ticket=null)
    {
        if(!$ticket){
            return $this->redirect($this->generateUrl('app_ticket'));
        }
//        $userManager = $this->get('fos_user.user_manager');
//        $this->checkUserPermission($userManager->getCurrentUser(), $ticket);
        $data = array('ticket' => $ticket);

        $message = new TicketMessage();
        $message->setPriority($ticket->getPriority());
        $message->setStatus($ticket->getStatus());

//        if (Status::STATUS_CLOSED != $ticket->getStatus()) {
//            $data['form'] = $this->createForm(new TicketMessageType($userManager), $message)->createView();
//        }
//        if ($this->get('fos_user.user_manager')->isGranted($userManager->getCurrentUser(), 'ROLE_TICKET_ADMIN')) {
//            $data['delete_form'] = $this->createDeleteForm($ticket->getId())->createView();
//        }
        return $this->render('SupportBundle:Ticket:show.html.twig', $data);
    }

    /**
     * @param \FOS\UserBundle\Model\UserInterface|string $user
     * @param Ticket $ticket
     */
    private function checkUserPermission($user, Ticket $ticket)
    {
        if (!\is_object($user) || (!$this->get('fos_user.user_manager')->isGranted($user, 'ROLE_TICKET_ADMIN') && $ticket->getUserCreated() != $user->getId())) {
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(403);
        }
    }

    /**
     * Finds and displays a Ticket entity.
     *
     * @param Request $request
     * @param Ticket $ticket
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function replyAction(Request $request, Ticket $ticket)
    {
        $userManager = $this->get('fos_user.user_manager');
        $ticketManager = $this->get('dr_ticket.ticket_manager');
        $user = $userManager->getCurrentUser();
        $this->checkUserPermission($user, $ticket);
        $message = $ticketManager->createMessage();
        $message->setPriority($ticket->getPriority());
        $form = $this->createForm(new TicketMessageType($userManager), $message);
        $form->submit($request);
        if ($form->isValid()) {
            $message->setUser($user);
            $message->setTicket($ticket);
            $ticketManager->updateTicket($ticket, $message);

            $event = new TicketEvent($ticket);
            $this->get('event_dispatcher')->dispatch(TicketEvents::TICKET_UPDATE, $event);
            return $this->redirect($this->generateUrl('app_ticket_show', array('id' => $ticket->getId())));
        }
        return $this->showAction($ticket);
    }

    /**
     * Deletes a Ticket entity.
     *
     * @param Request $request
     * @param Ticket $ticket
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Ticket $ticket)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->getCurrentUser();
        if (!\is_object($user) || !$userManager->isGranted($user, 'ROLE_TICKET_ADMIN')) {
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(403);
        }
        $form = $this->createDeleteForm($ticket->getId());
        $form->submit($request);
        if ($form->isValid()) {
            if (!$ticket) {
                throw $this->createNotFoundException($this->get('translator')->trans('ERROR_FIND_TICKET_ENTITY'));
            }
            $ticketManager = $this->get('dr_ticket.ticket_manager');
            $ticketManager->deleteTicket($ticket);
            $event = new TicketEvent($ticket);
            $this->get('event_dispatcher')->dispatch(TicketEvents::TICKET_DELETE, $event);
        }
        return $this->redirect($this->generateUrl('app_ticket'));
    }

    /**
     * Creates a form to delete a Ticket entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
            ;
    }

}
