<?php

namespace App\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\ProductBundle\Entity\Brand;

class BrandType extends AbstractType {

    private $brands;

    public function __construct(Brand $brands)
    {
        $this->brands = $brands;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => $this->brands
        ));
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'brand_choice';
    }

}