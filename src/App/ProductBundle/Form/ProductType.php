<?php

namespace App\ProductBundle\Form;

use App\CategoryBundle\Repository\CategoryRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use App\ProductBundle\Entity\Product;
//use App\SettingsBundle\Entity\Division;

class ProductType extends AbstractType
{

    private $brands;
    private $em;
    private $product;

    public function __construct(EntityManager $em, Product $product, $brands)
    {
        $this->brands = $brands;
        $this->product = $product;
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $division = $this->division;
        $product = $this->product;

        $builder
            // Basic description tab
            ->add('name', 'text', array(
                    'label' => 'Naam:')
            )
            ->add('subtitle', 'text', array(
                    'label' => 'Subtitel:')
            )
            ->add('descrShort', 'text', array(
                    'label' => 'Korte omschrijving:')
            )
            ->add('specs', 'collection', array(
                    'type' => new ProductHasSpecificationType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'label' => false,
                    'by_reference' => true,
                    'options' => array('data_class' => 'App\ProductBundle\Entity\ProductHasSpecification'),
                )
            )
            ->add('brand', 'entity',  array(
                    'placeholder' => 'Geen merk',
                    'class' => 'ProductBundle:Brand',
                    'label' => 'Merk:',
                    'choices' => $this->brands,
                    'required' => false,
                )
            )
            ->add('warehouseLocation', 'entity',  array(
                    'placeholder' => 'Geen locatie',
                    'class' => 'WarehouseBundle:WarehouseLocation',
                    'label' => 'Magazijn locatie:',
                    'choices' => $this->getLocations(),
                    'required' => false,
                )
            )
            ->add('category', 'entity', array(
                    'required' => true,
                    'label' => 'Categorie:',
                    'class' => 'CategoryBundle:Category',
                    'placeholder' => false,
                    'property' => 'indentedTitle',
                    'multiple' => false,
                    'expanded' => false,
                    'query_builder' => function (CategoryRepository $r)
                        {
                            return $r->getCategories();
                        },
//                    'query_builder' => function (CategoryRepository $r) use ($division)
//                        {
//                            return $r->getCategories($division);
//                        }
                )
            )
            ->add('archived', 'checkbox', array(
                    'label' => 'Gearchiveerd:',
                    'required' => false,
                )
            )
            // Full description tab
//            ->add('descrFull', 'textarea', array(
//                    'label' => 'Volledige omschrijving:')
//            )
            // Media tab
            // Stock tab
//            ->add('physicalProducts', 'entity', array(
//                'placeholder' => 'Voer voorraad in',
//                'class' => 'ProductBundle:ProductPhysical',
//                'label' => 'Fysieke producten:',
//                'required' => false,
//
//                )
//            )
            // Price tab
            ->add('priceEx', 'money', array(
                    'label' => 'Prijs excl. BTW:',
                    'required' => true
                )
            )
            ->add('tax', 'integer', array(
                'label' => 'Belastingschaal:',
                'required' => true
                )
            )
            ->add('discount', 'percent', array(
                    'label' => 'Kortingspercentage:',
                    'required' => false,
                )
            )
            ->add('onsale', 'checkbox', array(
                    'label' => 'In de actie:',
                    'required' => false,
                )

            )
//             //Tags tab
//            ->add('tags', 'entity', array(
//                    'placeholder' => 'Selecteer tags',
//                    'class' => 'ProductBundle:ProductTag',
//                    'label' => 'Tags:',
//                     'required' => false,
//
//                 )
//            )
//            ->add('specs', 'entity', array(
//                    'placeholder' => 'Selecteer specs',
//                    'class' => 'ProductBundle:ProductSpecification',
//                    'label' => 'Specificaties:',
//                    'required' => false,
//                )
//            )
//            ->add('uniqness', 'collection', array(
//                    'class' => 'ProductBundle:ProductSpecification',
//                    'label' => 'Tags:',
//                    'required' => false,
//                )
//            )

            // Action section
            // TODO : Make this a formtype
            ->add('submit', 'submit', array(
                    'label' => 'Opslaan',
                    'icon' => '<i class="fa fa-save"></i>'
                )
            )
            ->add('cancel', 'submit', array(
                    'label' => 'Annuleer',
                    'icon' => '<i class="fa fa-times"></i>'
                )
            );

            if($product->isArchived()){
                $builder->add('delete', 'submit', array(
                        'label' => 'Verwijder',
                        'icon' => '<i class="fa fa-trash"></i>'
                    )
                );
            }else{
                $builder->add('archive', 'submit', array(
                        'label' => 'Archiveer',
                        'icon' => '<i class="fa fa-archive"></i>'
                    )
                );
            }
    }

    private function getLocations(){


        $warehouseLocations = $this->em->getRepository('WarehouseBundle:WarehouseLocation')->getRootNodes();
        $list = array();

        // Hoofdmagazijn
        foreach($warehouseLocations as $warehouseLocation){
            $list[$warehouseLocation->getName()] = array();

            // Gangen
            foreach($warehouseLocation->getChildren() as $index => $child){
                $list[$warehouseLocation->getName()][$child->getName() . '-' .$child->getCode()] = array();

                // Stellingen
                foreach($child->getChildren() as $index1 => $child1){
                    $list[$warehouseLocation->getName()][$child->getName() . '-' .$child->getCode()][$child1->getName() . '-' .$child1->getCode()] = array();

                    // Niveau
                    foreach($child1->getChildren() as $index2 => $child2){
                        $list[$warehouseLocation->getName()][$child->getName() . '-' .$child->getCode()][$child1->getName() . '-' .$child1->getCode()][$child2->getName()] = array();

                        // Vak
                        foreach($child2->getChildren() as $index3 => $child3){
                            $list[$warehouseLocation->getName()][$child->getName() . '-' .$child->getCode()][$child1->getName() . '-' .$child1->getCode()][$child2->getName()][$child3->getName() .'-'.$child3->getCode()] = $child3;

                        }
                    }
                }
            }
        }
//
//
//
//
//        foreach($warehouseLocations as $index => $warehouseLocation){
//            $list[$warehouseLocation->getName()] = array();
//            foreach($warehouseLocation->getChildren() as $index2 => $mainLocation){
//
//                $list[$warehouseLocation->getName()][$mainLocation->getName()] = $mainLocation;
////                foreach($mainLocation as $index3 => $warehouseLocation){
////                    $list[$index] = $warehouseLocation;
////                }
//            }
//        }


//
//        dump($list);
//        die();

        return $list;
    }

//    private function getWarehouseTree(){
//
////        // 0 and 1: entire category repo
//        $warehouseRepo = $this->em->getRepository('WarehouseBundle:WarehouseLocation');
//
//
////        var_dump($categoryRepo->childrenHierarchy());
////        die();
//        $treeReturn = [];
//
//        foreach($warehouseRepo->childrenHierarchy() as $warehouseTree){
//            $treeReturn[] =  self::getChildren($warehouseRepo, $warehouseTree);
//        }
//
//        dump($treeReturn);
//        die();
//
//        return $treeReturn;
//    }
//
//    public static function getChildren($repository, $warehouseTree){
//        $treeNode = new \stdClass();
//        $treeNode->text = $warehouseTree['name'];
//        $treeNode->archived = $warehouseTree['archived'];
////        var_dump($categoryTree);
////        die();
//        $treeNode->originalURL = $warehouseTree['archived'];
//
//        $treeNode->tags[] = $repository->childCount($repository->find($warehouseTree['id']));
//
//        foreach($treeNode->tags as $tag) {
//            foreach ($warehouseTree['__children'] as $child) {
//                $treeNode->nodes[] = self::getChildren($repository, $child);
//            }
//        }
//
////        if($controller->name == 'product'){
////            if($categoryTree['containsProducts']) {
////                $treeNode->href = $controller->generateUrl('app_catalog_product_new', ['category_id' => $categoryTree['id']]);
////            }
////        }else{
////            $treeNode->href = $controller->generateUrl('app_catalog_category_edit', ['id' => $categoryTree['id']]);
////        }
//
//        return $treeNode;
//    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\ProductBundle\Entity\Product',
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'uniq_backendbundle_productbundle_product';
    }
}
