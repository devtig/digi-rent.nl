<?php
namespace App\ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\ProductBundle\Entity\Brand;
use App\ProductBundle\Entity\ProductTag;
use App\ProductBundle\Form\BrandType;
use App\ProductBundle\Form\TagType;

class TagController extends Controller {

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager('dynamic');

//        $breadcrumbs = $this->get("white_october_breadcrumbs");
//        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("rental_dashboard"));
//        $breadcrumbs->addItem("Catalogus");
//        $breadcrumbs->addItem("Tags");
//        $breadcrumbs->addItem("Overzicht");

        //Division Form
        $tagEntity = new ProductTag();

        $tagForm = $this->createForm(new TagType(), $tagEntity);

        $tagForm->handleRequest($request);

        if ($tagForm->isValid()) {
            $em->persist($tagEntity);
            $em->flush();
            $this->addFlash(
                'success',
                'Tag: '.$tagEntity->getTitle() .' is succesvol toegevoegd'
            );

            return $this->redirect($this->generateUrl('tags_main'));
        }

        $tags = $em->getRepository('ProductBundle:ProductTag')->findBy(
            [], ['created' => 'DESC']
        );

        return $this->render('ProductBundle:Tag:index.html.twig', array(
            'tags' => $tags,
            'tagForm' => $tagForm->createView(),
        ));
    }

    private function createAddBrandForm(Brand $entity)
    {
        $form = $this->container
            ->get('form.factory')
            ->createNamedBuilder('formBrand', 'form', $entity, array(
                'action' => $this->generateUrl('brands_main'),
                'method' => 'POST',
            ))
            ->add('title', 'text',
                array('label' => 'Titel')
            )
            ->add('save', 'submit', array('label' => 'Toevoegen'));

        return $form;
    }

    private function createEditBrandForm(Brand $entity)
    {
        $form = $this->container
            ->get('form.factory')
            ->createNamedBuilder('formBrand', 'form', $entity, array(
                'action' => $this->generateUrl('brand_details', array('id' => $entity->getId())),
                'method' => 'POST',
            ))
            ->add('title', 'text',
                array('label' => 'Titel')
            )
            ->add('edit', 'submit', array('label' => 'Aanpassen'));

        return $form;
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     */
    public function detailsAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager('dynamic');

        $entity = $em->getRepository('ProductBundle:Brand')->find($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("rental_dashboard"));
        $breadcrumbs->addItem("Catalogus");
        $breadcrumbs->addItem("Merken", $this->get("router")->generate("brands_main"));
        $breadcrumbs->addItem($entity->getTitle());
        $breadcrumbs->addItem("details");

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Brand entity.');
        }

        //Division Form
        $formBuilderBrand = $this->createEditBrandForm($entity);

        $formBrand = $formBuilderBrand
            ->getForm()
            ->handleRequest($request);

        if ($formBrand->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->addFlash(
                'success',
                'Merk: '.$entity->getTitle() .' is succesvol gewijzigd'
            );

            return $this->redirect($this->generateUrl('brands_main'));
        }

        return $this->render('ProductBundle:Brand:details.html.twig', array(
            'entity'      => $entity,
            'formBrand'   => $formBrand->createView(),
//            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Creates a form to edit a Category entity.
     *
     * @param Brand $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Brand $entity)
    {
        $form = $this->createForm(new BrandType(), $entity, array(
            'action' => $this->generateUrl('brand_details'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Brand entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager('dynamic');

        $brand = $em->getRepository('ProductBundle:Brand')->find($id);

        if (!$brand) {
            throw $this->createNotFoundException('Unable to find Brand entity.');
        }

        return $this->render('ProductBundle:Brand:details.html.twig', array(
            'brand'      => $brand,
        ));
    }
    /**
     * Deletes a Brand entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager('dynamic');
            $entity = $em->getRepository('ProductBundle:Brand')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Brand entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('brands_main'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}