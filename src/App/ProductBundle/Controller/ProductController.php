<?php

namespace App\ProductBundle\Controller;

use App\CategoryBundle\Controller\CategoryController;
use App\CoreBundle\Entity\Tax;
use App\ProductBundle\Entity\Product;
use App\ProductBundle\Entity\ProductPhysical;
use App\ProductBundle\Form\ProductType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    public $name = 'product';

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager('dynamic');
        $products = $em->getRepository('ProductBundle:Product')->findBy(
            [], ['created_at' => 'DESC']
        );

        return $this->render('ProductBundle::index.html.twig', array(
            'categoryTree' => CategoryController::getCategoryTree($this),
            'products' => $products
        ));
    }

    /**
     * Displays a form to create a new Product entity.
     *
     */
    public function createAction($category_id)
    {
        $product = new Product();

        $em = $this->getDoctrine()->getManager('dynamic');

        $categoryRepo = $em->getRepository('CategoryBundle:Category');
        $focusCategory = $categoryRepo->find($category_id);

        $path = $categoryRepo->getPath($focusCategory);

        $SKUarray = [];

        foreach($path as $category){
            $SKUarray[] = $category->getPrefix();
        }

        $SKU = implode('-',$SKUarray);
        $productCount = $focusCategory->getProducts()->count();
        $productCount++;
        $productCount = sprintf("%03d",$productCount);
        // FINAL SKU
        $SKU = $SKU.'-'.$productCount;


        $product->setCategory($focusCategory);
        $product->setSku($SKU);

        $product = $this->productTemplate($em, $product);

        $em->persist($product);
        $em->flush();

        return $this->redirectToRoute('app_catalog_product_edit', array('id' => $product->getId()));
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     */
    public function editAction($id, Request $request)
    {
//        $division = str_replace('.dev', '',$request->getHost());

        $em = $this->getDoctrine()->getManager('dynamic');
//        $divisionRepo = $em->getRepository('SettingsBundle:Division');

//        $divisionEntity = $divisionRepo->findOneBy(array('title' => $division));

        $categoryRepo = $em->getRepository('CategoryBundle:Category');
        $brands = $em->getRepository('ProductBundle:Brand')->findAll();

        $productFormEntity = $em->getRepository('ProductBundle:Product')->findOneBy(array('id' => $id));

//        $breadcrumbs = $this->get("white_october_breadcrumbs");
//        $breadcrumbs->addItem("Catalogus");
//        $breadcrumbs->addItem("Producten", $this->get('router')->generate("products_main"));
//        $breadcrumbs->addItem($productFormEntity->getSku());
//        $breadcrumbs->addItem("details");

        //Division Form
        $productForm = $this->createForm(new ProductType($em, $productFormEntity, $brands), $productFormEntity);
        $productForm->handleRequest($request);

        if($request->getMethod() == 'POST'){
            if ($productForm->isValid()) {
                $em->persist($productFormEntity);
                $em->flush();
                $this->addFlash(
                    'success',
                    $productFormEntity->getName() .' is succesvol gewijzigd'
                );
                return $this->redirect($this->generateUrl('app_catalog_products_index'));
            }
        }
        if($productForm->get('cancel')->isClicked()){
            return $this->redirect($this->generateUrl('app_catalog_products_index'));
        }

        $physicalProducts = $productFormEntity->getPhysicalProducts();

        $physicalStock = array();
        $from = new \DateTime('now'); // We want to know if the physical product is available today
        $till    = new \DateTime('tomorrow'); // We want to know if the physical product is available today
        foreach($physicalProducts as $physicalProduct){
            $physicalStock[] = array(
                'entity' => $physicalProduct,
                'status' => PhysicalProductController::getStatusFromPhysicalProduct($physicalProduct, $from, $till),
            );
        }

        $availablePhysicalStock = 0;
        foreach($physicalStock as $physicalProduct){
            if($physicalProduct['status'] == "Beschikbaar"){
                $availablePhysicalStock++;
            }
        }

        if (!$productFormEntity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        return $this->render('ProductBundle::edit.html.twig', array(
            'productForm' => $productForm->createView(),
            'availablePhysicalStock' => $availablePhysicalStock,
            'physicalStock' => $physicalStock,
            'product' => $productFormEntity,
            'taxes' => Tax::$taxes,
            'brands' => $brands,
            'path' => $categoryRepo->getPath($productFormEntity->getCategory()),
        ));
    }

    private function productTemplate(EntityManager $em, Product $entity){

        $usr= $this->get('security.context')->getToken()->getUser();

        // TODO Change tax to what user wants? Of geowon ff naar kijken
        $product = array(
            'name' => 'Product aangemaakt door '.ucfirst($usr->getFirstname()),
            'subtitle' => '...',
            'descr_short' => '...',
            'descr_full' => '....',
            'price_ex' => 0.0,
            'tax' => Tax::NL_TAX_HIGH,
            'discount' => NULL,
            'video_url' => '', //nullable
            'archived' => false,
            'onsale' => false, //nullable
            'thumbnail' => "productPlaceholder.jpg", //nullable
            'created' => new \DateTime("NOW"),
            'modified' => new \DateTime("NOW"),
            'uniqness' => '{"uniq": ["Shoarma bakken", "Gekke shit","Niet normaal", "Biatch"]}', //nullable
        );

        $entity->setName($product['name']);
        $entity->setSubtitle($product['subtitle']);
        $entity->setDescrShort($product['descr_short']);
        $entity->setDescription($product['descr_full']);
        $entity->setPriceEx($product['price_ex']);
        $entity->setTax($product['tax']);
        $entity->setDiscount($product['discount']);
        $entity->setVideoUrl($product['video_url']);
        $entity->setArchived($product['archived']);
        $entity->setOnsale($product['onsale']);
        $entity->setImageName($product['thumbnail']);
        $entity->setCreatedAt($product['created']);
        $entity->setUniqness($product['uniqness']);
        $entity->setEnabled(false);
        $entity->setCreatedBy($usr);

        return $entity;
    }

    /**
     * Displays a form to create a new Product entity.
     *
     */
    public function createPhysicalAction($product_id, $amount)
    {
        // iterate through amount and create physical products via its functions
        $em = $this->getDoctrine()->getManager('dynamic');
        $product = $em->getRepository('ProductBundle:Product')->find($product_id);

        for($i = 0; $i < $amount; $i++){
            //create physical products
            $physicalProduct = new ProductPhysical();
            $physicalProduct->setProduct($product);
            $physicalProduct->setLocation(0);
            // Get current count of physical products, despite of a physical product is archived or not (total amount)
            $physicalProductCount = $product->getPhysicalProducts()->count();
            $physicalProductCount += $i;
            $physicalProductCount++;
            $physicalProduct->setBarcode($product->getSku().'-'.$physicalProductCount);

            $physicalProduct->setArchived(false);
            $em->persist($physicalProduct);
        }

        $em->flush();

//        for ($i = 0; $i < $amount - 1; $i++){
//            $stock = 4;
//
//            $randomLocation = rand(1, 99);
//
//            for ($j = 0; $j < $stock; $j++){
//                $physicalProduct = new ProductPhysical();
//                $physicalProduct->setProduct($products[$i]);
//                $physicalProduct->setLocation("2.".$randomLocation);
//                $physicalProduct->setBarcode($products[$i]->getSku().'-'.$j);
//                $physicalProduct->setArchived(false);
//
//                if($j < count($PhysicalProductActivities)){
//                    $activityCount = $j;
//                    $em->persist($PhysicalProductActivities[$activityCount]);
//                }
//
//                $physicalProduct->addProductActivity($PhysicalProductActivities[$activityCount]);
//                $products[$i]->addPhysicalProduct($physicalProduct);
//                $em->persist($physicalProduct);
//            }
//            $em->persist($products[$i]);
//        }

        return $this->redirectToRoute('app_catalog_product_edit', array('id' => $product->getId()));
    }

}
