<?php

namespace App\ProductBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\ProductBundle\Entity\ProductPhysical;

/**
 * Physical product controller.
 *
 */
class PhysicalProductController extends Controller
{

    public function indexAction()
    {

    }

    public function detailsAction($id)
    {
        $em = $this->getDoctrine()->getManager('dynamic');

        $Physicalproduct = $em->getRepository('ProductBundle:ProductPhysical')->findOneBy(array('id' => $id));

//        $breadcrumbs = $this->get("white_october_breadcrumbs");
//        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("rental_dashboard"));
//        $breadcrumbs->addItem("Catalogus");
//        $breadcrumbs->addItem("Producten", $this->get('router')->generate("products_main"));
//        $breadcrumbs->addItem($Physicalproduct->getProduct()->getSku());
//        $breadcrumbs->addItem("Wijzigen");

        if (!$Physicalproduct) {
            throw $this->createNotFoundException('Unable to find Physical product entity.');
        }

        return $this->render('ProductBundle:ProductPhysical:details.html.twig', array(
            'physicalProduct' => $Physicalproduct,
        ));
    }

    public static function getAvailablityFromPhysicalProduct(ProductPhysical $entity, \DateTime $date)
    {
        // TODO check if physical product is available on specific date

        foreach ($entity->getProductActivity() as $activity) {
            echo '<pre>';
            \Doctrine\Common\Util\Debug::dump($activity);
            \Doctrine\Common\Util\Debug::dump($activity->getStockLeaving());
            \Doctrine\Common\Util\Debug::dump($activity->getStockReceivingExpected());
            echo '</pre>';
        }
        return 4;
    }

    public static function getStatusFromPhysicalProduct(ProductPhysical $entity, \DateTime $from, \Datetime $till)
    {

        if ($from == null) {
            $beginDate = date_format(new \DateTime('now'), 'd/m/y');
        } else if ($till == null) {
            $endDate = date_format(new \DateTime('tomorrow'), 'd/m/y');
        }

        $beginDate = date_format($from, 'd/m/y');

        $toDate = date_format($till, 'd/m/y');

        foreach ($entity->getProductActivity() as $activity) {
//            if (!empty($activity)) {
//                \Doctrine\Common\Util\Debug::dump($activity->getStockLeaving());
//            } else {
//                // TODO check availability of product from specific date till specific date
//                \Doctrine\Common\Util\Debug::dump($entity->getProductActivity());
//                return "niks";


                if ($beginDate > date_format($activity->getStockLeaving(), 'd/m/y') && $beginDate < date_format($activity->getStockReceivingExpected(), 'd/m/y')) {
                    return 'Is op klus';
                } else if ($beginDate == date_format($activity->getStockLeaving(), 'd/m/y')) {
                    return 'Vandaag op klus';
                } else if ($beginDate == date_format($activity->getStockReceivingExpected(), 'd/m/y')) {
                    return 'Komt vandaag terug';
                } else if ($toDate == date_format($activity->getStockReceivingExpected(), 'd/m/y')) {
                    return 'Komt morgen terug';
                } else {
                    // Checken of hij echt beschikbaar is of het daadwerkelijk is terug gekomen
                    return 'Beschikbaar';
                }
//            }

//            }
        }
        return 'Beschikbaar';
    }

}