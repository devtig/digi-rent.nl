<?php
namespace App\ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\ProductBundle\Entity\Brand;
use App\ProductBundle\Entity\ProductSpecification;
use App\ProductBundle\Form\BrandType;
use App\ProductBundle\Form\ProductSpecificationType;

class SpecificationController extends Controller {

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager('uniq_rental');

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("rental_dashboard"));
        $breadcrumbs->addItem("Catalogus");
        $breadcrumbs->addItem("Merken");
        $breadcrumbs->addItem("Overzicht");

        //Division Form
        $specificationEntity = new ProductSpecification();
        $specificationForm = $this->createForm(new ProductSpecificationType(), $specificationEntity);
        $specificationForm->handleRequest($request);


        if ($specificationForm->isValid()) {
            $specificationEntity->setActive(true);
            $em->persist($specificationEntity);
            $em->flush();
            $this->addFlash(
                'success',
                'Specificatie: '.$specificationEntity->getTitle() .' is succesvol toegevoegd'
            );

            return $this->redirect($this->generateUrl('productSpecifications_main'));
        }

        $specs = $em->getRepository('ProductBundle:ProductSpecification')->findBy(
            [], ['created' => 'DESC']
        );

        return $this->render('ProductBundle:Specification:index.html.twig', array(
            'specs' => $specs,
            'specificationForm' => $specificationForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing Product entity.
     *
     */
    public function detailsAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager('uniq_rental');

        $entity = $em->getRepository('ProductBundle:ProductSpecification')->find($id);

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem("Dashboard", $this->get("router")->generate("rental_dashboard"));
        $breadcrumbs->addItem("Catalogus");
        $breadcrumbs->addItem("Specificaties", $this->get("router")->generate("productSpecifications_main"));
        $breadcrumbs->addItem($entity->getTitle());
        $breadcrumbs->addItem("details");

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find specification entity.');
        }

        $specificationForm = $this->createForm(new ProductSpecificationType(), $entity);
        $specificationForm->handleRequest($request);

        if ($specificationForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->addFlash(
                'success',
                'Specificatie: '.$entity->getTitle() .' is succesvol gewijzigd'
            );

            return $this->redirect($this->generateUrl('productSpecifications_main'));
        }

        return $this->render('ProductBundle:Specification:details.html.twig', array(
            'entity'      => $entity,
            'specificationForm'   => $specificationForm->createView(),
        ));
    }
    /**
     * Creates a form to edit a Category entity.
     *
     * @param Brand $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Brand $entity)
    {
        $form = $this->createForm(new BrandType(), $entity, array(
            'action' => $this->generateUrl('brand_details'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Brand entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager('uniq_rental');

        $brand = $em->getRepository('ProductBundle:Brand')->find($id);

        if (!$brand) {
            throw $this->createNotFoundException('Unable to find Brand entity.');
        }

        return $this->render('ProductBundle:Brand:details.html.twig', array(
            'brand'      => $brand,
        ));
    }
    /**
     * Deletes a Brand entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager('uniq_rental');
            $entity = $em->getRepository('ProductBundle:Brand')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Brand entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('brands_main'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}