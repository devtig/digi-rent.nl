<?php

namespace App\ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use App\CoreBundle\Entity\Traits as Traits;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProductActivityState
 *
 * @ORM\Table(name="product_activity_state")
 * @ORM\Entity
 */
class ProductActivityState
{

    use Traits\Identifiable,
        Traits\NameAble;

    /**
     * @ORM\OneToMany(targetEntity="ProductActivity", mappedBy="productActivityState")
     **/
    private $activities;

    public function __construct() {
        $this->activities = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add activities
     *
     * @param ProductActivity $activities
     * @return ProductActivityState
     */
    public function addActivity(ProductActivity $activities)
    {
        $this->activities[] = $activities;

        return $this;
    }

    /**
     * Remove activities
     *
     * @param ProductActivity $activities
     */
    public function removeActivity(ProductActivity $activities)
    {
        $this->activities->removeElement($activities);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActivities()
    {
        return $this->activities;
    }
}
