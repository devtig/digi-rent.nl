<?php

namespace App\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
//use App\SupplierBundle\Entity\Supplier;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * ProductExternal
 *
 * @ORM\Table(name="product_external")
 * @ORM\Entity
 */
class ProductExternal
{

    use Traits\Identifiable,
        Traits\Enableable,
        Traits\TimeStampable;

//    /**
//     * @ORM\ManyToOne(targetEntity="App\SupplierBundle\Entity\Supplier", inversedBy="externalProducts")
//     *
//     * @ORM\JoinColumn(name="supplier", referencedColumnName="id", nullable=false)
//     */
//    protected $supplier;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\Product", inversedBy="externalProducts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    private $productInternal;


    public function __construct()
    {
        $this->supplier = new ArrayCollection();
        $this->productInternal = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="supplier_sku", type="string", length=255)
     */
    private $supplierSku;

    /**
     * @var string
     *
     * @ORM\Column(name="supplier_price_ex", type="decimal")
     */
    private $supplierPriceEx;

    /**
     * @var integer
     *
     * @ORM\Column(name="supplier_stock", type="integer")
     */
    private $supplierStock;

//    /**
//     * Set supplier
//     *
//     * @param Supplier $supplier
//     * @return ProductExternal
//     */
//    public function setSupplier(Supplier $supplier)
//    {
//        $this->supplier = $supplier;
//
//        return $this;
//    }
//
//    /**
//     * Get supplier
//     *
//     * @return Supplier
//     */
//    public function getSupplier()
//    {
//        return $this->supplier;
//    }

    /**
     * Set supplierSku
     *
     * @param string $supplierSku
     * @return ProductExternal
     */
    public function setSupplierSku($supplierSku)
    {
        $this->supplierSku = $supplierSku;

        return $this;
    }

    /**
     * Get supplierSku
     *
     * @return string 
     */
    public function getSupplierSku()
    {
        return $this->supplierSku;
    }

    /**
     * Set supplierPriceEx
     *
     * @param string $supplierPriceEx
     * @return ProductExternal
     */
    public function setSupplierPriceEx($supplierPriceEx)
    {
        $this->supplierPriceEx = $supplierPriceEx;

        return $this;
    }

    /**
     * Get supplierPriceEx
     *
     * @return string 
     */
    public function getSupplierPriceEx()
    {
        return $this->supplierPriceEx;
    }

    /**
     * Set supplierStock
     *
     * @param integer $supplierStock
     * @return ProductExternal
     */
    public function setSupplierStock($supplierStock)
    {
        $this->supplierStock = $supplierStock;

        return $this;
    }

    /**
     * Get supplierStock
     *
     * @return integer 
     */
    public function getSupplierStock()
    {
        return $this->supplierStock;
    }

    /**
     * Set product
     *
     * @param Product $product
     * @return ProductExternal
     */
    public function setProduct(Product $product)
    {
        $this->productInternal = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->productInternal;
    }

}
