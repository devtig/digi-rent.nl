<?php

namespace App\ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * Brand
 *
 * @ORM\Table(name="brands")
 * @ORM\Entity
 */
class Brand
{

use Traits\Identifiable,
    Traits\NameAble,
    Traits\TimeStampable;

    public function __construct() {
        $this->products = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\Product", mappedBy="brand")
     */
    private $products;

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

}
