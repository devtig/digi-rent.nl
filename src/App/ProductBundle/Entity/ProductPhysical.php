<?php

namespace App\ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * ProductPhysical
 *
 * @ORM\Table(name="product_physical")
 * @ORM\Entity
 */
class ProductPhysical
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __construct() {
        $this->product_activity = new ArrayCollection();
    }

    public function __toString(){
        return $this->getBarcode();
    }

    /**
     * @ORM\ManyToMany(targetEntity="ProductActivity", inversedBy="product_physical")
     * @ORM\JoinTable(name="product_physical_has_activities")
     **/
    private $product_activity;

    /**
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="physicalProducts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     *
     **/
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", length=255, nullable=true, unique=true)
     */
    private $barcode;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $modified;

    /**
     * @var boolean
     *
     * @ORM\Column(name="archived", type="boolean", nullable=false, options={"default":false})
     */
    private $archived;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add product_activity
     *
     * @param ProductActivity $product_activity
     * @return ProductPhysical
     */
    public function addProductActivity(ProductActivity $product_activity)
    {
        $this->product_activity[] = $product_activity;

        return $this;
    }

    /**
     * Remove product_activity
     *
     * @param ProductActivity $product_activity
     */
    public function removeProductActivity(ProductActivity $product_activity)
    {
        $this->product_activity->removeElement($product_activity);
    }

    /**
     * Get productActivity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductActivity()
    {
        return $this->product_activity;
    }

    /**
     * Set product
     *
     * @param Product $product
     * @return ProductPhysical
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return ProductPhysical
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductExternal
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductExternal
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     * @return Product
     */
    public function setArchived($archived = false)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean
     */
    public function getArchived()
    {
        return $this->archived;
    }

}
