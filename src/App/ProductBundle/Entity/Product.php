<?php

namespace App\ProductBundle\Entity;

use App\WarehouseBundle\Entity\WarehouseLocation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;
use App\CategoryBundle\Entity\Category;
//use App\ProductBundle\Specification;
use App\CoreBundle\Entity\Traits as Traits;
use App\SharedBundle\Entity\Traits as SharedTraits;

/**
 * Product
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="App\ProductBundle\Repository\ProductRepository")
 * @Vich\Uploadable
 */
class Product
{
    // Non-referencing Traits
    use Traits\Identifiable,
        Traits\NameAble,
        Traits\Describable,
        Traits\Sluggable,
        Traits\Taxable,
        SharedTraits\CreatedByAble,
        Traits\TimeStampable,
        Traits\Archiveable,
        Traits\Enableable;


    public function __construct() {
        $this->alternatives = new ArrayCollection();
        $this->comboSuggestions = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->physicalProducts = new ArrayCollection();
        $this->productsInSet = new ArrayCollection();
        $this->productAsSet = new ArrayCollection();
        $this->specs = new ArrayCollection();
        $this->setArchived(false);
        $this->setEnabled(false);
    }

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $imageName;
    

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\CategoryBundle\Entity\Category", inversedBy="products")
     *
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    protected $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\WarehouseBundle\Entity\WarehouseLocation", inversedBy="products")
     *
     * @ORM\JoinColumn(name="warehouse_location_id", referencedColumnName="id", nullable=true)
     */
    protected $warehouseLocation;

    /**
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\ProductExternal", mappedBy="productInternal")
     */
    protected $externalProducts;

    /**
     * @ORM\ManyToMany(targetEntity="App\ProductBundle\Entity\ProductTag", inversedBy="products")
     *
     * @ORM\JoinTable(name="products_tags")
     **/
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\ProductHasSpecification", mappedBy="product")
     **/
    private $specs;

    /**
     * @ORM\ManyToMany(targetEntity="App\ProductBundle\Entity\Product")
     * @ORM\JoinTable(name="product_alternatives",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="product_alternative_id", referencedColumnName="id")}
     * )
     * @var ArrayCollection
     */
    protected $alternatives;

    /**
     * @ORM\ManyToMany(targetEntity="App\ProductBundle\Entity\Product")
     * @ORM\JoinTable(name="product_combo_suggestions",
     *     joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="product_combo_suggestion_id", referencedColumnName="id")}
     * )
     * @var ArrayCollection
     */
    protected $comboSuggestions;

    /**
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\ProductPhysical", mappedBy="product")
     **/
    private $physicalProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\ProductBundle\Entity\ProductsSet", mappedBy="productAsSet")
     **/
    private $productAsSet;

    /**
     * @ORM\ManyToMany(targetEntity="App\ProductBundle\Entity\ProductsSet", mappedBy="productsInSet")
     **/
    private $productsInSet;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\Brand", inversedBy="products")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=true)
     */
    private $brand;


    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=255, unique=true, nullable=false)
     */
    private $sku;


    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="text", nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="descr_short", type="text", nullable=true)
     */
    private $descrShort;


    /**
     * @var string
     *
     * @ORM\Column(name="price_ex", type="decimal", nullable=true)
     */
    private $priceEx;

    /**
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;

    /**
     * @var string
     *
     * @ORM\Column(name="video_url", type="text", nullable=true)
     */
    private $video_url;

    /**
     * @var boolean
     *
     * @ORM\Column(name="onsale", type="boolean", nullable=false, options={"default":false})
     */
    private $onsale;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="text", nullable=true)
     */
    private $thumbnail;

    /**
     * @var array
     *
     * @ORM\Column(name="uniqness", type="json_array", nullable=true)
     */
    private $uniqness;


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updated_at = new \DateTime('now');
        }

        return $this;
    }

    /**
     * Get ImageFile
     *
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get ImageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set category
     *
     * @param Category $category
     * @return Product
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set warehouseLocation
     *
     * @param WarehouseLocation $warehouseLocation
     * @return Product
     */
    public function setWarehouseLocation(WarehouseLocation $warehouseLocation)
    {
        $this->warehouseLocation = $warehouseLocation;

        return $this;
    }

    /**
     * Get warehouseLocation
     *
     * @return integer
     */
    public function getWarehouseLocation()
    {
        return $this->warehouseLocation;
    }

    public function addExternalProduct(ProductExternal $externalProduct)
    {
        if (!$this->externalProducts->contains($externalProduct)) {
            $this->externalProducts->add($externalProduct);
        }

        return $this;
    }

    public function removeExternalProduct(ProductExternal $externalProduct)
    {
        if ($this->externalProducts->contains($externalProduct)) {
            $this->externalProducts->removeElement($externalProduct);
        }

        return $this;
    }

    public function getExternalProducts()
    {
        return $this->externalProducts;
    }

    /**
     * Add spec
     *
     * @param ProductSpecification $specification
     * @return Product
     */
    public function addSpecs(ProductSpecification $specification)
    {
        if (!$this->specs->contains($specification)) {
            $this->specs[] = $specification;
        }

        return $this;
    }

    /**
     * Remove specification
     *
     * @param ProductSpecification $specification
     */
    public function removeSpecs(ProductSpecification $specification)
    {
        $this->specs->removeElement($specification);
    }

    /**
     * Get specs
     *
     * @return ArrayCollection
     */
    public function getSpecs()
    {
        return $this->specs;
    }

    /**
     * Add tags
     *
     * @param ProductTag $tags
     * @return Product
     */
    public function addTag(ProductTag $tags)
    {
        if (!$this->tags->contains($tags)) {
            $this->tags[] = $tags;
        }

        return $this;
    }

    /**
     * Remove tags
     *
     * @param ProductTag $tags
     */
    public function removeTag(ProductTag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param  Product $product
     * @return void
     */
    public function addAlternative(Product $product)
    {
        if (!$this->alternatives->contains($product)) {
            $this->alternatives->add($product);
        }
    }

    /**
     * @return Arraycollection
     */
    public function getAlternatives()
    {
        return $this->alternatives;
    }

    /**
     * @param  Product $product
     * @return void
     */
    public function removeAlternative(Product $product)
    {
        if ($this->alternatives->contains($product)) {
            $this->alternatives->removeElement($product);
        }
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }


    /**
     * Set subtitle
     *
     * @param string $subtitle
     * @return Product
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set descrShort
     *
     * @param string $descrShort
     * @return Product
     */
    public function setDescrShort($descrShort)
    {
        $this->descrShort = $descrShort;

        return $this;
    }

    /**
     * Get descrShort
     *
     * @return string
     */
    public function getDescrShort()
    {
        return $this->descrShort;
    }

    /**
     * Set priceEx
     *
     * @param string $priceEx
     * @return Product
     */
    public function setPriceEx($priceEx)
    {
        $this->priceEx = $priceEx;

        return $this;
    }

    /**
     * Get priceEx
     *
     * @return string
     */
    public function getPriceEx()
    {
        return $this->priceEx;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Product
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set video_url
     *
     * @param string $videoUrl
     * @return Product
     */
    public function setVideoUrl($videoUrl)
    {
        $this->video_url = $videoUrl;

        return $this;
    }

    /**
     * Get video_url
     *
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->video_url;
    }

    /**
     * Set onsale
     *
     * @param boolean $onsale
     * @return Product
     */
    public function setOnsale($onsale)
    {
        $this->onsale = $onsale;

        return $this;
    }

    /**
     * Get onsale
     *
     * @return boolean
     */
    public function getOnsale()
    {
        return $this->onsale;
    }

    /**
     * Set uniqness
     *
     * @param array $uniqness
     * @return Product
     */
    public function setUniqness($uniqness)
    {
        $this->uniqness = $uniqness;

        return $this;
    }

    /**
     * Get uniqness
     *
     * @return array
     */
    public function getUniqness()
    {
        return $this->uniqness;
    }

    /**
     * Add physicalProducts
     *
     * @param \App\ProductBundle\Entity\ProductPhysical $physicalProducts
     * @return Product
     */
    public function addPhysicalProduct(\App\ProductBundle\Entity\ProductPhysical $physicalProducts)
    {
        $this->physicalProducts[] = $physicalProducts;

        return $this;
    }

    /**
     * Remove physicalProducts
     *
     * @param \App\ProductBundle\Entity\ProductPhysical $physicalProducts
     */
    public function removePhysicalProduct(\App\ProductBundle\Entity\ProductPhysical $physicalProducts)
    {
        $this->physicalProducts->removeElement($physicalProducts);
    }

    /**
     * Get physicalProducts
     *
     * @return ArrayCollection
     */
    public function getPhysicalProducts()
    {
        return $this->physicalProducts;
    }

    public function addComboSuggestion(Product $comboSuggestion)
    {
        if (!$this->comboSuggestions->contains($comboSuggestion)) {
            $this->comboSuggestions[] = $comboSuggestion;
        }
    }
    public function getComboSuggestions(){
        return $this->comboSuggestions;
    }

    public function removeComboSuggestion(Product $comboSuggestion){
        if ($this->alternatives->contains($comboSuggestion)) {
            $this->alternatives->removeElement($comboSuggestion);
        }
    }

    /**
     * Add productAsSet
     *
     * @param ProductsSet $productAsSet
     * @return Product
     */
    public function addProductAsSet(ProductsSet $productAsSet)
    {
        $this->productAsSet[] = $productAsSet;

        return $this;
    }

    /**
     * Remove productAsSet
     *
     * @param ProductsSet $productAsSet
     */
    public function removeProductAsSet(ProductsSet $productAsSet)
    {
        $this->productAsSet->removeElement($productAsSet);
    }

    /**
     * Get productAsSet
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductAsSet()
    {
        return $this->productAsSet;
    }

    /**
     * @return mixed
     */
    public function getProductsInSet()
    {
        return $this->productsInSet;
    }

    /**
     * @param mixed $productsInSet
     */
    public function setProductsInSet($productsInSet)
    {
        $this->productsInSet = $productsInSet;
    }

    /**
     * Set brand
     *
     * @param Brand $brand
     * @return Product
     */
    public function setBrand(Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return integer
     */
    public function getBrand()
    {
        return $this->brand;
    }

}


