<?php

namespace App\ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\ProductBundle\Entity\ProductActivityState;
use App\ProjectBundle\Entity\Project;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ProductActivity
 *
 * @ORM\Table(name="product_activity")
 * @ORM\Entity
 */
class ProductActivity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="ProductPhysical", mappedBy="product_activity")
     **/
    private $product_physical;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\ProductActivityState", inversedBy="activities")
     * @ORM\JoinColumn(name="activity_state_id", referencedColumnName="id")
     **/
    private $productActivityState;

    public function __construct() {
        $this->created = new \DateTime('NOW');
        $this->product_physical = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_id", type="integer", nullable=true)
     */
    private $project;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stock_leaving", type="datetime")
     */
    private $stockLeaving;

    // TODO Stock leaving, relation with activity_state
    /**
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\ProductActivityState", inversedBy="activities")
     * @ORM\JoinColumn(name="activity_state_id", referencedColumnName="id")
     *
     * @ORM\Column(name="stock_leaving_state", type="string", length=255)
     */
    private $stockLeavingState;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_leaving_by", type="integer")
     */
    private $stockLeavingBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stock_receiving_expected", type="datetime")
     */
    private $stockReceivingExpected;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stock_receiving", type="datetime", nullable=true)
     */
    private $stockReceiving;

    // TODO Stock receiving, relation with activity_state
    /**
     * @var string
     *
     * @ORM\Column(name="stock_receiving_state", type="string", length=255, nullable=true)
     */
    private $stockReceivingState;

    // TODO user details from session should be saved here, no relation with user can been done here

    /**
     * @var integer
     *
     * @ORM\Column(name="stock_receiving_by", type="integer", nullable=true)
     */
    private $stockReceivingBy;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add product_physical
     *
     * @param ProductPhysical $productPhysical
     * @return ProductActivity
     */
    public function addProductPhysical(ProductPhysical $productPhysical)
    {
        $this->product_physical[] = $productPhysical;

        return $this;
    }

    /**
     * Remove product_physical
     *
     * @param ProductPhysical $productPhysical
     */
    public function removeProductPhysical(ProductPhysical $productPhysical)
    {
        $this->product_physical->removeElement($productPhysical);
    }

    /**
     * Get product_physical
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductPhysical()
    {
        return $this->product_physical;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ProductActivity
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->$title;
    }

    /**
     * Set project
     *
     * @param Project $project
     * @return ProductActivity
     */
    public function setProject(Project $project = Null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ProductActivity
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return ProductActivity
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set stockLeaving
     *
     * @param \DateTime $stockLeaving
     * @return ProductActivity
     */
    public function setStockLeaving($stockLeaving)
    {
        $this->stockLeaving = $stockLeaving;

        return $this;
    }

    /**
     * Get stockLeaving
     *
     * @return \DateTime 
     */
    public function getStockLeaving()
    {
        return $this->stockLeaving;
    }

    /**
     * Set stockLeavingState
     *
     * @param string $stockLeavingState
     * @return ProductActivity
     */
    public function setStockLeavingState($stockLeavingState)
    {
        $this->stockLeavingState = $stockLeavingState;

        return $this;
    }

    /**
     * Get stockLeavingState
     *
     * @return ProductActivityState
     */
    public function getStockLeavingState()
    {
        return $this->stockLeavingState;
    }


    /**
     * Set stockReceivingExpected
     *
     * @param \DateTime $stockReceivingExpected
     * @return ProductActivity
     */
    public function setStockReceivingExpected($stockReceivingExpected)
    {
        $this->stockReceivingExpected = $stockReceivingExpected;

        return $this;
    }

    /**
     * Get stockReceivingExpected
     *
     * @return \DateTime
     */
    public function getStockReceivingExpected()
    {
        return $this->stockReceivingExpected;
    }

    /**
     * Set stockReceiving
     *
     * @param \DateTime $stockReceiving
     * @return ProductActivity
     */
    public function setStockReceiving($stockReceiving)
    {
        $this->stockReceiving = $stockReceiving;

        return $this;
    }

    /**
     * Get stockReceiving
     *
     * @return \DateTime 
     */
    public function getStockReceiving()
    {
        return $this->stockReceiving;
    }

    /**
     * Set stockReceivingState
     *
     * @param string $stockReceivingState
     * @return ProductActivity
     */
    public function setStockReceivingState($stockReceivingState)
    {
        $this->stockReceivingState = $stockReceivingState;

        return $this;
    }

    /**
     * Get stockReceivingState
     *
     * @return ProductActivityState
     */
    public function getStockReceivingState()
    {
        return $this->stockReceivingState;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return ProductActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get stockLeavingBy
     *
     * @return integer
     */
    public function getStockLeavingBy()
    {
        return $this->stockLeavingBy;
    }

    /**
     * Set stockLeavingBy
     *
     * @param integer $stockLeavingBy
     * @return ProductActivity
     */
    public function setStockLeavingBy($stockLeavingBy)
    {
        $this->stockLeavingBy = $stockLeavingBy;
    }

    /**
     * @return integer
     */
    public function getStockReceivingBy()
    {
        return $this->stockReceivingBy;
    }

    /**
     * @param integer $stockReceivingBy
     */
    public function setStockReceivingBy($stockReceivingBy)
    {
        $this->stockReceivingBy = $stockReceivingBy;
    }

    /**
     * Set productActivityState
     *
     * @param ProductActivityState $productActivityState
     * @return ProductActivityState
     */
    public function setActivityState(ProductActivityState $productActivityState)
    {
        $this->productActivityState = $productActivityState;

        return $this;
    }

    /**
     * Get productActivityState
     *
     * @return ProductActivityState
     */
    public function getActivityState()
    {
        return $this->productActivityState;
    }

}
