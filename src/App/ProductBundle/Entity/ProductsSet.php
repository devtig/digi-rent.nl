<?php

namespace App\ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProductsSet
 *
 * @ORM\Table(name="product_sets")
 * @ORM\Entity
 */
class ProductsSet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    public function __construct() {
        $this->productsInSet = new ArrayCollection();
    }


    /**
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\Product", inversedBy="productAsSet")
     * @ORM\JoinColumn(name="set_id", referencedColumnName="id")
     **/
    private $productAsSet;


    /**
     * @ORM\ManyToMany(targetEntity="App\ProductBundle\Entity\Product", inversedBy="productsInSet")
     * @ORM\JoinTable(name="products_in_set")
     **/
    private $productsInSet;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return ProductsSet
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set set
     *
     * @param \App\ProductBundle\Entity\Product $set
     * @return ProductsSet
     */
    public function setSet(Product $set = null)
    {
        $this->productAsSet = $set;

        return $this;
    }

    /**
     * Get set
     *
     * @return \App\ProductBundle\Entity\Product 
     */
    public function getSet()
    {
        return $this->productAsSet;
    }

    /**
     * Add product
     *
     * @param Product $product
     * @return Product
     */
    public function addProduct(Product $product)
    {
        $this->productsInSet[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->productsInSet->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->productsInSet;
    }

}
