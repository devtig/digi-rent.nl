<?php

namespace App\ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * Tag
 *
 * @ORM\Table(name="product_tags")
 * @ORM\Entity
 */
class ProductTag
{

    use Traits\Identifiable,
        Traits\NameAble,
        Traits\TimeStampable;

    public function __construct() {
        $this->products = new ArrayCollection();
    }

    public function __toString(){
        return $this->getName();
    }


    /**
     * @ORM\ManyToMany(targetEntity="App\ProductBundle\Entity\Product", mappedBy="tags")
     **/
    private $products;


    /**
     * Add products
     *
     * @param \App\ProductBundle\Entity\Product $products
     * @return Tag
     */
    public function addProduct(\App\ProductBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \App\ProductBundle\Entity\Product $products
     */
    public function removeProduct(\App\ProductBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }
}
