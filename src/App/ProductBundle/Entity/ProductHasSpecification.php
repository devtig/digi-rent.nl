<?php

namespace App\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\CategoryBundle\Entity\Specification;

/**
 * ProductSpecification
 *
 * @ORM\Table(name="product_has_specs")
 * @ORM\Entity
 */
class ProductHasSpecification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\Product", inversedBy="specs")
     *k
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     **/
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\ProductSpecification")
     *
     * @ORM\JoinColumn(name="specification_id", referencedColumnName="id", nullable=false)
     **/
    private $specification;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param Product $product
     * @return ProductHasSpecification
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return integer
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set specification
     *
     * @param ProductSpecification $specification
     * @return ProductHasSpecification
     */
    public function setSpecType(ProductSpecification $specification)
    {
        $this->specification = $specification;

        return $this;
    }

    /**
     * Get specification
     *
     * @return ProductHasSpecification
     */
    public function getSpecification()
    {
        return $this->specification;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return ProductHasSpecification
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return ProductHasSpecification
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
}
