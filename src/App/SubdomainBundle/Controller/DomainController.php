<?php

namespace App\SubdomainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DomainController extends Controller
{
    public function disabledAction()
    {
        return $this->render('MainBundle:Domain:disabled.html.twig');
    }
}
