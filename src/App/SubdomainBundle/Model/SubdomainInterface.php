<?php

namespace App\SubdomainBundle\Model;


interface SubdomainInterface {
    public function getId();

    public function getName();
    public function setName($name);

    public function getDbName();
    public function setDbName($dbName);

    public function getUsername();
    public function setUsername($username);

    public function setPassword($password);
    public function getPassword();

    public function setPlainPassword($plainPassword);
    public function getPlainPassword();

    /**
    * @return string|null The salt
    */
    public function getSalt();

    public function setEnabled($enabled);
    public function isEnabled();

    /**
     * Removes sensitive data from the domain.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials();
}