<?php

namespace App\SubdomainBundle\Model;


interface SubdomainManagerInterface {

    /**
     * Updates a subdomain password if a plain password is set.
     *
     * @param SubdomainInterface $domain
     *
     * @return void
     */
    public function updatePassword(SubdomainInterface $domain);

}