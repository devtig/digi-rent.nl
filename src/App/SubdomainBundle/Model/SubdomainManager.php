<?php

namespace App\SubdomainBundle\Model;

use App\SubdomainBundle\Entity\Subdomain;
use App\CompanyBundle\Entity\Company;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class SubdomainManager implements SubdomainManagerInterface {

    protected $encoderFactory;
    protected $generator;
    protected $entityManager;
    protected $currentSubdomain;
    protected $subdomainRouteName;
    protected $subdomain;
    protected $credentials;

    /**
     * Constructor.
     *
     * @param EncoderFactoryInterface $encoderFactory
     * @param string                  $baseHost
     * @param EntityManager           $em
     */
    public function __construct(EncoderFactoryInterface $encoderFactory, $baseHost, EntityManager $em, ContainerInterface $container)
    {
        $this->encoderFactory = $encoderFactory;

        $this->entityManager = $em;
        $this->container = $container;

        $this->repository = $em->getRepository(Subdomain::class);
        $metadata = $em->getClassMetadata(Subdomain::class);
        $this->class = $metadata->getName();

        $this->baseHost = $baseHost;
        $this->host = $container->get('request')->getHost();
        $this->subdomainRouteName = str_replace("." . $this->baseHost, "", $this->host);
    }

    public function getCredentials(){
        return $this->credentials;
    }

    public function createSubdomain(Company $company){
        $class = $this->getClass();
        $subdomain = new $class;
        $passwordManager = $this->container->get('dr_core.password_manager');
        $subdomain = $passwordManager->secureSubdomain($subdomain);

        // At the point that the plainpassword gets set, the database needs to be created as well with the same credentials (unhashed)
        $subdomain->setCompanyId($company->getId());
        $subdomain->setName($company->getSubdomainName());
        $subdomain->setEnabled(true);

        $this->credentials = array(
            'dbName' => $subdomain->getDbName(),
            'username' => $subdomain->getUsername(),
            'password' => $subdomain->getPlainPassword(),
        );

        $this->updatePassword($subdomain);

        return $this->updateSubdomain($subdomain);
    }

    /**
     * Creates a subdomain
     *
     * @param SubdomainInterface $subdomain
     * @return boolean
     */
    public function updateSubdomain(SubdomainInterface $subdomain, $enabled = null)
    {
        try{
            $this->entityManager->persist($subdomain);
            $this->entityManager->flush();
            return true;
        }catch (Exception $e){
            return $e;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function updatePassword(SubdomainInterface $subdomain)
    {
        if (0 !== strlen($password = $subdomain->getPlainPassword())) {
            $passwordManager = $this->container->get('dr_core.password_manager');
            $encoder = $passwordManager->getEncoder($subdomain);

            if($subdomain->setPassword($encoder->encodePassword($password, $subdomain->getSalt()))){
                $subdomain->eraseCredentials();
            }
        }
    }

    /**
     * @return Subdomain
     */
    public function getCurrentSubdomain()
    {
        if(!is_null($this->subdomainRouteName)){
            // http://pastebin.com/kenVw9mv
            $this->container->get('router.request_context')->setParameter('subdomain', $this->subdomainRouteName);
            return $this->subdomain = $this->repository->findOneBy(array('name' => $this->subdomainRouteName));
        }else{
            return null;
        }
    }

    /**
     * @return Subdomain
     */
    public function getSubdomain($name)
    {
        return $this->repository->findOneBy(array('name' => $name));
    }

    /**
     * @param Subdomain $subdomain
     */
    public function setCurrentSubdomain(Subdomain $subdomain)
    {
        $this->currentSubdomain = $subdomain;
    }

    /**
     * {@inheritDoc}
     */
    public function getClass()
    {
        return $this->class;
    }
}