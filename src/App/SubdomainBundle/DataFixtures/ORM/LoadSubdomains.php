<?php

namespace App\SubdomainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\SubdomainBundle\Entity\Subdomain;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadSubdomains extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
//        // Get the connectionservice
//        $connectionService = $this->container->get('dr_database.connection_service');
//        $ngs =  $connectionService->getRepositoryFromDefault('dr_company',
//            array('where' => array(
//            'subdomain_name' => 'ngs'))
//        );
//        $uniq =  $connectionService->getRepositoryFromDefault('dr_company',
//            array('where' => array(
//            'subdomain_name' => 'uniq'))
//        );
//        $bsf =  $connectionService->getRepositoryFromDefault('dr_company',
//            array('where' => array(
//            'subdomain_name' => 'bspecial'))
//        );
//
//
//
//        $subdomains = array(
//            array(
//                'name' => 'uniq',
//                'company' => $uniq[0]['id'],
//            ),
//            array(
//                'name' => 'ngs',
//                'company' => $ngs[0]['id'],
//            ),
//            array(
//                'name' => 'bsf',
//                'company' => $bsf[0]['id'],
//            ),
//        );

//        $this->newSubdomains($subdomains, $manager);

    }

    private function newSubdomains($subdomains, ObjectManager $manager){
        foreach($subdomains as $subdomain){
            $subdomainObject = new Subdomain();
            $subdomainObject->setName($subdomain['name']);
            $subdomainObject->setDbName($subdomain['name']);
            $subdomainObject->setUsername($subdomain['name']);
            $subdomainObject->setPassword($subdomain['name']);
            $subdomainObject->setCompanyId($subdomain['company']);
            $subdomainObject->setEnabled(true);
            $manager->persist($subdomainObject);
            $this->addReference($subdomain['name'], $subdomainObject);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
}
