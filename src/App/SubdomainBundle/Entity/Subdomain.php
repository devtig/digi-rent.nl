<?php

namespace App\SubdomainBundle\Entity;

use App\SubdomainBundle\Model\SubdomainInterface;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * Domain
 *
 * @Gedmo\Mapping\Annotation\Loggable(logEntryClass="SubdomainLogEntry")
 * @ORM\Table(name="subdomain")
 * @ORM\Entity(repositoryClass="App\SubdomainBundle\Repository\subdomainRepository")
 */
class Subdomain implements SubdomainInterface
{

    use Traits\Identifiable;

    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->enabled = false;
    }

    /**
     * Removes sensitive data from the subdomain.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @var int
     * @ORM\Column(name="company_id", type="integer", nullable=false, unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $companyId;

    /**
     * @var string
     *
     * @Gedmo\Mapping\Annotation\Versioned
     * @ORM\Column(name="name", type="string", length=10, unique=true, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Mapping\Annotation\Versioned
     * @ORM\Column(name="db_name", type="string", length=64, unique=true, nullable=false)
     */
    private $dbName;

    /**
     * @var string
     *
     * @Gedmo\Mapping\Annotation\Versioned
     * @ORM\Column(name="username", type="string", length=64, unique=true, nullable=false)
     */
    private $username;

    /**
     * Encrypted password. Must be persisted.
     *
     * @Gedmo\Mapping\Annotation\Versioned
     * @var string
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    private $plainPassword;

    /**
     * The salt to use for hashing
     *
     * @var string
     * @ORM\Column(name="salt", type="string", length=31, nullable=false)
     */
    private $salt;

    /**
     * @var boolean
     *
     * @Gedmo\Mapping\Annotation\Versioned
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * Set name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Subdomain
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Set companyId
     *
     * @return string
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set companyId
     *
     * @param int $companyId
     * @return Subdomain
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * get dbName
     *
     * @return string
     */
    public function getDbName()
    {
        return $this->dbName;
    }

    /**
     * Set dbName
     *
     * @param string $dbName
     * @return Subdomain
     */
    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

    /**
     * get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Subdomain
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Set encrypted password.
     *
     * @param string $password
     * @return Subdomain
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get encrypted password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set plainPassword
     *
     * @param string $plainPassword
     * @return Subdomain
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * Get plainPassword
     *
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Subdomain
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

}

