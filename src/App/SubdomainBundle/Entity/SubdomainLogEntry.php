<?php

namespace App\SubdomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;
use Gedmo\Loggable\Entity\MappedSuperclass\AbstractLogEntry;

/**
 * Domain
 *
 * @ORM\Table(name="subdomain_log_entry")
 * @ORM\Entity(repositoryClass="App\SubdomainBundle\Repository\subdomainLogEntryRepository")
 */
class SubdomainLogEntry extends AbstractLogEntry
{

}

