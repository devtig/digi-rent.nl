<?php

namespace App\SubdomainBundle\EventListener;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CurrentSubdomainListener
{
    // Basic subdomain check
    private $em;
    private $baseHost;
    private $baseDBPrefix;
    private $router;
    private $container;
    private $host;

    // If all went well, connection details
    private $request;
    private $connection;

    // This class checks if the given subdomain exists or not, if it exists the url is generated
    public function __construct(EntityManager $em, Router $router, Connection $connection, ContainerInterface $container)
    {
        $this->em = $em;
        $this->router = $router;
        $this->connection = $connection;
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        // Check if subdomain wildcald exists in route
        if ($this->container->get('request')->attributes->get('subdomain')) {

            $subdomainManager = $this->container->get('dr_subdomain.subdomain_manager');
            $subdomain = $subdomainManager->getCurrentSubdomain();

            // Check if subdomain is valid
            if (!is_null($subdomain)) {
                // Subdomain exists
                if (!$subdomain->isEnabled()) {
                    //Subdomain exists but is not yet activated (through confirmation email & required company info)
                    // TODO Pass subdomain name in session
                    $url = $this->router->generate('web_subdomain_disabled', array('subdomainname' => $subdomain->getName()));
                    $response = new RedirectResponse($url);
                    $event->setResponse($response);
                }else{
                    // Check if user has been authenticated via login or via remember me cookie
                    if(!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY') ||
                       $this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')
                    ){
                        //Change the doctrine connection
                        $databaseManager = $this->container->get('dr_database.database_manager');
                        $databaseManager->prepareConnection($subdomain);

                    }else {
                        //TODO hier nog even naar kijken
                    }
                }
            }else{
                // TODO Maybe 302 moved ??? SEO
                // Redirect to web index
                $url = $this->router->generate('web_index');
                $response = new RedirectResponse($url);
                $event->setResponse($response);
            }
        }
    }
}