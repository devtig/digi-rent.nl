<?php

namespace App\SubdomainBundle\Command;

use Doctrine\DBAL\Exception\DriverException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateClientDatabaseCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('digirent:database:create')
            ->setDescription('Create subdomain database')
            ->setDefinition(array(
                new InputArgument('subdomain_name', InputArgument::REQUIRED, 'The subdomain name'),
                new InputArgument('username', InputArgument::REQUIRED, 'The subdomain username'),
                new InputArgument('password', InputArgument::REQUIRED, 'The subdomain password'),
            ))
            ->setHelp(<<<EOT
The <info>digirent:database:create</info> command creates a database for

  <info>php app/console digirent:database:create <subdomain_name></info>
EOT
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $errors = array();

        // Get doctrine SQL command
        $queryDatabase = $this->getApplication()->find('doctrine:query:sql');
        $dbName = $input->getArgument('subdomain_name');
        $dbUsername = $input->getArgument('username');
        $dbPassword = $input->getArgument('password');

        $dbCreateArguments = array(
            'command' => 'doctrine:query:sql',
            'sql' => 'CREATE DATABASE '.$dbName.';',
        );

        $userCreateArguments = array(
            'command' => 'doctrine:query:sql',
            'sql' => 'CREATE USER '.$dbUsername.'@localhost IDENTIFIED BY "'.$dbPassword.'";',
        );

        $privilegesGrantArguments = array(
            'command' => 'doctrine:query:sql',
            'sql' => 'GRANT ALL PRIVILEGES ON '.$dbName.'.* TO '.$dbUsername.'@localhost;',
        );

        $flushArgument = array(
            'command' => 'doctrine:query:sql',
            'sql' => 'FLUSH PRIVILEGES;',
        );
        
        // Try to create database
        try{
            $dbCreateInput = new ArrayInput($dbCreateArguments);
            $queryDatabase->run($dbCreateInput, $output);

        }catch(DriverException $e){
            $errors[] =
                array(
                    'code' => $e->getErrorCode(),
                    'message' => $e->getMessage()
                );
        }

        // Try to create user if database was created successfully
        if(empty($errors)){
            try{
                $userCreateInput = new ArrayInput($userCreateArguments);
                $queryDatabase->run($userCreateInput, $output);

            }catch(DriverException $e){
                $errors[] =
                    array(
                        'code' => $e->getErrorCode(),
                        'message' => $e->getMessage()
                    );
            }
        }

        // Try grant privileges to user if user was created successfully
        if(empty($errors)){
            try{
                $privilegesGrantInput = new ArrayInput($privilegesGrantArguments);
                $queryDatabase->run($privilegesGrantInput, $output);

            }catch(DriverException $e){
                $errors[] =
                    array(
                        'code' => $e->getErrorCode(),
                        'message' => $e->getMessage()
                    );
            }
        }
        // Try flush privileges if user was created was successfully granted privileges
        if(empty($errors)){
            try{
                $lushInput = new ArrayInput($flushArgument);
                $queryDatabase->run($lushInput, $output);
            }catch(DriverException $e){
                $errors[] =
                    array(
                        'code' => $e->getErrorCode(),
                        'message' => $e->getMessage()
                    );
            }
        }

        if(empty($errors)){
            return 'success';
        }else{
            /**
             * SQL error codes:
             * Error: 1006 SQLSTATE: HY000 (ER_CANT_CREATE_DB)
             * Error: 1007 SQLSTATE: HY000 (ER_DB_CREATE_EXISTS)
             */
            return $errors;
        }
    }

    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$this->getHelperSet()->has('question')) {
            $this->legacyInteract($input, $output);

            return;
        }

        if (!$input->getArgument('subdomain_name')) {
            $question = new Question('Please choose a subdomain name:');
            $question->setValidator(function($username) {
                if (empty($username)) {
                    throw new \Exception('Subdomain name can not be empty');
                }

                return $username;
            });
            $answer = $this->getHelper('question')->ask($input, $output, $question);

            $input->setArgument('subdomain_name', $answer);
        }
    }

    // BC for SF <2.5
    private function legacyInteract(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('subdomain_name')) {
            $username = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please enter subdomain name:',
                function($username) {
                    if (empty($username)) {
                        throw new \Exception('Subdomain name can not be empty');
                    }

                    return $username;
                }
            );
            $input->setArgument('username', $username);
        }
    }
}