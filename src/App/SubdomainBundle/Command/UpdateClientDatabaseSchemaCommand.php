<?php

namespace App\SubdomainBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\DoctrineCommandHelper;
use Doctrine\ORM\Tools\Console\Command\SchemaTool\UpdateCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class UpdateClientDatabaseSchemaCommand extends UpdateCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('digirent:schema:update')
            ->setDescription('Updates current schema from client')
            ->setDefinition(array(
                new InputArgument('subdomain_name', InputArgument::REQUIRED, 'The subdomain name'),
                new InputOption(
                    'complete', null, InputOption::VALUE_NONE,
                    'If defined, all assets of the database which are not relevant to the current metadata will be dropped.'
                ),

                new InputOption(
                    'dump-sql', null, InputOption::VALUE_NONE,
                    'Dumps the generated SQL statements to the screen (does not execute them).'
                ),
                new InputOption(
                    'force', 'f', InputOption::VALUE_NONE,
                    'Causes the generated SQL statements to be physically executed against your database.'
                ),
            ))
            ->setHelp(<<<EOT
The <info>digirent:schema:update</info> command updates a schema for

  <info>php app/console digirent:schema:update <subdomain></info>
EOT
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $connectionStatus = $this->getApplication()->getKernel()->getContainer()->get('dr_database.database_manager')->setConnectionForSubdomain($input->getArgument('subdomain_name'));
        if(is_null($connectionStatus)){
            DoctrineCommandHelper::setApplicationEntityManager($this->getApplication(), 'dynamic');

            return parent::execute($input, $output);
        }else{
            $output->writeln('Subdomain does not exist!');
        }
    }

    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$this->getHelperSet()->has('question')) {
            $this->legacyInteract($input, $output);

            return;
        }

        if (!$input->getArgument('subdomain_name')) {
            $question = new Question('Please choose a subdomain name:');
            $question->setValidator(function($username) {
                if (empty($username)) {
                    throw new \Exception('Subdomain name can not be empty');
                }

                return $username;
            });
            $answer = $this->getHelper('question')->ask($input, $output, $question);

            $input->setArgument('subdomain_name', $answer);
        }
    }

    // BC for SF <2.5
    private function legacyInteract(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('subdomain_name')) {
            $username = $this->getHelper('dialog')->askAndValidate(
                $output,
                'Please enter subdomain name:',
                function($username) {
                    if (empty($username)) {
                        throw new \Exception('Subdomain name can not be empty');
                    }

                    return $username;
                }
            );
            $input->setArgument('username', $username);
        }
    }
}