<?php

namespace App\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectActivity
 *
 * @ORM\Table(name="project_activities")
 * @ORM\Entity
 */
class ProjectActivity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer")
     */
    private $project;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_activity_state", type="integer")
     */
    private $projectActivityState;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProjectActivity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProjectActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set project
     *
     * @param integer $project
     * @return ProjectActivity
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set projectActivityState
     *
     * @param integer $projectActivityState
     * @return ProjectActivity
     */
    public function setProjectActivityState($projectActivityState)
    {
        $this->projectActivityState = $projectActivityState;

        return $this;
    }

    /**
     * Get projectActivityState
     *
     * @return integer 
     */
    public function getProjectActivityState()
    {
        return $this->projectActivityState;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Project
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}
