<?php

namespace App\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectInvoice
 *
 * @ORM\Table(name="project_invoices")
 * @ORM\Entity
 */
class ProjectInvoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __construct()
    {
        $this->created = new \DateTime('now');
        $this->projectListItems = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="App\ProjectBundle\Entity\ProjectListItem", mappedBy="invoice")
     **/
    private $projectListItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProjectBundle\Entity\Project", inversedBy="request")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     **/
    
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var integer
     *
     * @ORM\Column(name="quotation", type="integer")
     */
    private $quotation;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_history", type="integer")
     */
    private $paymentHistory;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_location", type="text")
     */
    private $pdfLocation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_status", type="boolean")
     */
    private $paymentStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_terms", type="integer")
     */
    private $paymentTerms;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param Project $project
     * @return ProjectInvoice
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return ProjectInvoice
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }


    /**
     * Set quotation
     *
     * @param integer $quotation
     * @return ProjectInvoice
     */
    public function setQuotation($quotation)
    {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * Get quotation
     *
     * @return integer 
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * Set paymentHistory
     *
     * @param integer $paymentHistory
     * @return ProjectInvoice
     */
    public function setPaymentHistory($paymentHistory)
    {
        $this->paymentHistory = $paymentHistory;

        return $this;
    }

    /**
     * Get paymentHistory
     *
     * @return integer 
     */
    public function getPaymentHistory()
    {
        return $this->paymentHistory;
    }

    /**
     * Set pdfLocation
     *
     * @param string $pdfLocation
     * @return ProjectInvoice
     */
    public function setPdfLocation($pdfLocation)
    {
        $this->pdfLocation = $pdfLocation;

        return $this;
    }

    /**
     * Get pdfLocation
     *
     * @return string 
     */
    public function getPdfLocation()
    {
        return $this->pdfLocation;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set paymentStatus
     *
     * @param boolean $paymentStatus
     * @return ProjectInvoice
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return boolean 
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Set paymentTerms
     *
     * @param integer $paymentTerms
     * @return ProjectInvoice
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;

        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return integer 
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }
}
