<?php

namespace App\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * Project
 *
 * @ORM\Table(name="projects")
 * @ORM\Entity
 */
class Project
{

    use Traits\Identifiable,
        Traits\NameAble,
        Traits\Describable,
        Traits\TimeStampable;

    function __construct()
    {
        $this->request = new ArrayCollection();
        $this->quotation = new ArrayCollection();
        $this->invoice = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="App\ProjectBundle\Entity\ProjectRequest", mappedBy="project")
     **/
    private $request;

    /**
     * @ORM\OneToMany(targetEntity="App\ProjectBundle\Entity\ProjectQuotation", mappedBy="project")
     **/
    private $quotation;

    /**
     * @ORM\OneToMany(targetEntity="App\ProjectBundle\Entity\ProjectInvoice", mappedBy="project")
     **/
    private $invoice;


    /**
     * @var integer
     *
     * @ORM\Column(name="customer", type="integer")
     */
    private $customer;

    /**
     * @var integer
     *
     * @ORM\Column(name="manager", type="integer")
     */
    private $manager;

    /**
     * Add request
     *
     * @param ProjectRequest $request
     * @return Project
     */
    public function addRequest(ProjectRequest $request)
    {
        $this->request[] = $request;

        return $this;
    }

    /**
     * Remove request
     *
     * @param ProjectRequest $request
     */
    public function removeRequest(ProjectRequest $request)
    {
        $this->request->removeElement($request);
    }

    /**
     * Get request
     *
     * @return ArrayCollection
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Add quotation
     *
     * @param ProjectQuotation $quotation
     * @return Project
     */
    public function addQuotation(ProjectQuotation $quotation)
    {
        $this->quotation[] = $quotation;

        return $this;
    }

    /**
     * Remove quotation
     *
     * @param ProjectQuotation $quotation
     */
    public function removeQuotation(ProjectQuotation $quotation)
    {
        $this->quotation->removeElement($quotation);
    }

    /**
     * Get quotation
     *
     * @return ArrayCollection
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * Add invoice
     *
     * @param ProjectInvoice $invoice
     * @return Project
     */
    public function addInvoice(ProjectInvoice $invoice)
    {
        $this->invoice[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice
     *
     * @param ProjectInvoice $invoice
     */
    public function removeInvoice(ProjectInvoice $invoice)
    {
        $this->invoice->removeElement($invoice);
    }

    /**
     * Get invoice
     *
     * @return ArrayCollection
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set customer
     *
     * @param integer $customer
     * @return Project
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return integer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set manager
     *
     * @param integer $manager
     * @return Project
     */
    public function setManager($manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return integer 
     */
    public function getManager()
    {
        return $this->manager;
    }

}
