<?php

namespace App\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * ProjectQuotation
 *
 * @ORM\Table(name="project_quotations")
 * @ORM\Entity
 */
class ProjectQuotation
{

    use Traits\Identifiable,
        Traits\Statusable;

    public function __construct()
    {
        $this->projectListItems = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="App\ProjectBundle\Entity\ProjectListItem", mappedBy="quotation")
     **/
    private $projectListItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProjectBundle\Entity\Project", inversedBy="request")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false)
     **/
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, unique=true, nullable=false)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="remark", type="text")
     */
    private $remark;

    /**
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer")
     */
    private $discount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accept_date", type="datetime")
     */
    private $acceptDate;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_location", type="text")
     */
    private $pdfLocation;


    /**
     * Set project
     *
     * @param Project $project
     * @return ProjectQuotation
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }


    /**
     * Set number
     *
     * @param string $number
     * @return ProjectQuotation
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return ProjectQuotation
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return ProjectQuotation
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set acceptDate
     *
     * @param \DateTime $acceptDate
     * @return ProjectQuotation
     */
    public function setAcceptDate($acceptDate)
    {
        $this->acceptDate = $acceptDate;

        return $this;
    }

    /**
     * Get acceptDate
     *
     * @return \DateTime 
     */
    public function getAcceptDate()
    {
        return $this->acceptDate;
    }


    /**
     * Set pdfLocation
     *
     * @param string $pdfLocation
     * @return ProjectQuotation
     */
    public function setPdfLocation($pdfLocation)
    {
        $this->pdfLocation = $pdfLocation;

        return $this;
    }

    /**
     * Get pdfLocation
     *
     * @return string
     */
    public function getPdfLocation()
    {
        return $this->pdfLocation;
    }
}
