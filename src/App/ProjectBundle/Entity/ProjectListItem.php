<?php

namespace App\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\ProductBundle\Entity\Product;
use App\CoreBundle\Entity\Traits as Traits;

/**
 * ProjectListItem
 *
 * @ORM\Table(name="project_list_items")
 * @ORM\Entity
 */
class ProjectListItem
{

    use Traits\Identifiable,
        Traits\Taxable;


    /**
     * @ORM\ManyToOne(targetEntity="App\ProjectBundle\Entity\ProjectRequest", inversedBy="projectListItems")
     * @ORM\JoinColumn(name="request_id", referencedColumnName="id", nullable=true)
     **/
    private $request;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProjectBundle\Entity\ProjectQuotation", inversedBy="projectListItems")
     * @ORM\JoinColumn(name="quotation_id", referencedColumnName="id", nullable=true)
     **/
    private $quotation;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProjectBundle\Entity\ProjectInvoice", inversedBy="projectListItems")
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     **/
    private $invoice;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProductBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true)
     **/
    private $product;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="price_ex", type="decimal")
     */
    private $priceEx;

    /**
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer")
     */
    private $discount;

    /**
     * Set quotation
     *
     * @param ProjectQuotation $quotation
     * @return ProjectListItem
     */
    public function setQuotation(ProjectQuotation $quotation)
    {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * Get quotation
     *
     * @return ProjectQuotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * Set request
     *
     * @param ProjectRequest $request
     * @return ProjectListItem
     */
    public function setRequest(ProjectRequest $request = NULL)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return ProjectRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set invoice
     *
     * @param ProjectInvoice $invoice
     * @return ProjectListItem
     */
    public function setInvoice(ProjectInvoice $invoice = NULL)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return ProjectInvoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set product
     *
     * @param Product $product
     * @return ProjectListItem
     */
    public function setProduct(Product $product = NULL)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return ProjectListItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set priceEx
     *
     * @param string $priceEx
     * @return ProjectListItem
     */
    public function setPriceEx($priceEx)
    {
        $this->priceEx = $priceEx;

        return $this;
    }

    /**
     * Get priceEx
     *
     * @return string 
     */
    public function getPriceEx()
    {
        return $this->priceEx;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return ProjectListItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }
}
