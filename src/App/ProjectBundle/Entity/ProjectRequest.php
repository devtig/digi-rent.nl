<?php

namespace App\ProjectBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectRequest
 *
 * @ORM\Table(name="project_requests")
 * @ORM\Entity
 */
class ProjectRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    public function __construct()
    {
        $this->created = new \DateTime('now');
        $this->projectListItems = new ArrayCollection();
    }

    /**
     * @ORM\OneToMany(targetEntity="App\ProjectBundle\Entity\ProjectListItem", mappedBy="request")
     **/
    private $projectListItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\ProjectBundle\Entity\Project", inversedBy="request")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     **/
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(name="remark", type="text")
     */
    private $remark;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add projectListItem
     *
     * @param ProjectListItem $projectListItem
     * @return ProjectRequest
     */
    public function addProjectListItem(ProjectListItem $projectListItem)
    {
        if ($this->projectListItems->contains($projectListItem)) {
            $this->projectListItems->add($projectListItem);
        }
        return $this;
    }

    /**
     * Remove projectListItem
     *
     * @param ProjectListItem $projectListItem
     * @return ProjectRequest
     */
    public function removeProjectListItem(ProjectListItem $projectListItem)
    {
        if ($this->projectListItems->contains($projectListItem)) {
            $this->projectListItems->removeElement($projectListItem);
        }
        return $this;
    }

    /**
     * Get projectListItem
     *
     * @return ArrayCollection
     */
    public function getProjectListItems()
    {
        return $this->projectListItems;
    }

    /**
     * Set project
     *
     * @param Project $project
     * @return ProjectRequest
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set remark
     *
     * @param string $remark
     * @return Project
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }
}
