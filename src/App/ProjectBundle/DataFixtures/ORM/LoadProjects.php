<?php

namespace App\ProjectBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\ProjectBundle\Entity\Project;

class LoadProjects extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $peter = $userManager->findUserBy(array('email' => 'peter@uniq-group.nl'))->getId();
        $tim = $userManager->findUserBy(array('email' => 'tim@tp.nl'))->getId();
        $jeroen = $userManager->findUserBy(array('email' => 'jeroen@uniq-group.nl'))->getId();

        if($peter == null || $jeroen == null){
            echo "Peter of Jeroen bestaat niet in de database";
            die();
        }

        $projects = array(
            array(
                'title' => 'Bruiloft',
                'description' => 'Wij gaan trouwen op 12 july in de brabantse hal',
                'customer' => $jeroen,
                'manager' => $peter
            ),
            array(
                'title' => 'Summervibes',
                'description' => 'dikke fissa hoor!',
                'customer' => $tim,
                'manager' => $peter
            )
        );

        $this->addProjects($projects, $manager);
    }

    private function addProjects($projects, ObjectManager $manager){

        foreach($projects as $projects_array){
            $project = new Project();
            $project->setTitle($projects_array['title']);
            $project->setDescription($projects_array['description']);
            $project->setCustomer($projects_array['customer']);
            $project->setManager($projects_array['manager']);
            $manager->persist($project);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10; // the order in which fixtures will be loaded
    }
}
