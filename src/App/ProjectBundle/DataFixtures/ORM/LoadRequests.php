<?php

namespace App\ProjectBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\ProjectBundle\Entity\ProjectRequest;

class LoadRequests extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $em = $this->container->get('doctrine')->getEntityManager('dynamic');
        $repo = $em->getRepository('ProjectBundle:Project');
        $summervibes = $repo->findOneBy(array('title' => 'Summervibes'));
        $bruiloft = $repo->findOneBy(array('title' => 'Bruiloft'));

        $requests = array(
            array(
                'remark' => 'Ik wil wel graag trouwen met een dikke hardcore beat als het kan!',
                'project' => $bruiloft,
            ),
            array(
                'remark' => 'het mag niks kosten maar moet wel dikke fissa worden',
                'project' => $summervibes,
            )
        );

        $this->addRequests($requests, $manager);
    }

    private function addRequests($requests, ObjectManager $manager){

        foreach($requests as $requests_array){
            $request = new ProjectRequest();
            $request->setRemark($requests_array['remark']);
            $request->setProject($requests_array['project']);
            $manager->persist($request);
        }
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11; // the order in which fixtures will be loaded
    }
}
