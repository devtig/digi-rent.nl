<?php

namespace Web\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class BetaRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text', array(
                'attr' => array(
                    'placeholder' => 'Voornaam',
                    'pattern'     => '.{2,}' //minlength
                )
            ))
            ->add('lastname', 'text', array(
                'attr' => array(
                    'placeholder' => 'Achternaam',
                    'pattern'     => '.{2,}' //minlength
                )
            ))
            ->add('email', 'email', array(
                'attr' => array(
                    'placeholder' => 'E-mail'
                )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $collectionConstraint = new Collection(array(
            'firstname' => array(
                new NotBlank(array('message' => 'Uw voornaam mag niet leeg zijn.')),
                new Length(array('min' => 2))
            ),
            'lastname' => array(
                new NotBlank(array('message' => 'Uw achternaam mag niet leeg zijn.')),
                new Length(array('min' => 2))
            ),
            'email' => array(
                new NotBlank(array('message' => 'Uw email mag niet leeg zijn.')),
                new Email(array('message' => 'Ongeldig e-mail adres.'))
            ),
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }
    public function getName()
    {
        return 'betaRegister';
    }
}