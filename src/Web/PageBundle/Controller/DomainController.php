<?php

namespace Web\PageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DomainController extends Controller {

    public function disabledAction($subdomainname = null){
        return $this->render('PageBundle:Domain:disabled.html.twig', array(
            'subdomain' => $subdomainname
        ));
    }
}