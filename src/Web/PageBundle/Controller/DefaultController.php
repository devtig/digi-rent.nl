<?php

namespace Web\PageBundle\Controller;


use App\CoreBundle\Event\DREvents;
use Shared\UserBundle\Entity\User;
use Shared\UserBundle\Mailer\TwigMailer;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Web\PageBundle\Form\Type\BetaRegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Web\PageBundle\Form\Type\NewsletterType;

class DefaultController extends Controller
{
    /**
     * Let's the user register via a form
     */
    public function indexAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->addRole('ROLE_BETA');
        $passwordManager = new ComputerPasswordGenerator();
        $user->setPlainPassword($passwordManager->generatePassword());

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm(array('type' => 'beta'));
        $form->setData($user);

        if ($request->isXmlHttpRequest()) {
            if ($request->isMethod('POST')){

                $form->submit($request);
                $response['firstname'] = $user->getFirstname();

                if ($form->isValid()){

                    $event = new FormEvent($form, $request);
                    $userManager->updateUser($user);

                    if (null === $eventResponse = $event->getResponse()) {
                        $url = $this->generateUrl('fos_user_registration_confirmed');
                        $eventResponse = new RedirectResponse($url);
                    }

                    $dispatcher->dispatch(DREvents::BETA_REGISTRATION_SUCCESS, $event);

                    $response['success'] = true;
                    $response['msg'] = 'bevestig uw e-mail adres. Zodra de BETA live is wordt u direct op de hoogte gesteld!';
                    return new JsonResponse($response);

                }else{
                    $response['success'] = false;
                    return new JsonResponse($response);

                }
            }
        }

        return $this->render('PageBundle:Default:index.html.twig', array(
            'betaForm' => $form->createView(),
        ));
    }

}
