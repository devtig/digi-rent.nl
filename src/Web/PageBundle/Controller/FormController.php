<?php

namespace Web\PageBundle\Controller;
use App\CoreBundle\Model\PasswordManager;
use Shared\UserBundle\Entity\User;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Web\PageBundle\Form\Type\BetaRegisterType;


class FormController extends Controller
{
    /**
     * @Template()
     */
    public function BetaPostAction(Request $request) { // renamed to postAction for consistency. Took out request as parameter (not sure if this method of injection is still supported)


        if ($request->isMethod('POST')){

            $betaForm->submit($request);
            $errorMsg = "er is een fout opgetreden, probeer het later opnieuw.";

            if ($betaForm->isValid()){
                $data = $betaForm->getData();

                // TODO persist data into database
                // Register ajax call to controller (submitting data)
                // Response to webpage success and sent email to customer & webmaster (success)

                // check if user already exists from email
                $succesfullyRegistered = $this->register($request, $betaForm, $data['email'],$data['firstname'],$data['lastname']);
                if($succesfullyRegistered){
                    $response['success'] = true;
                    $response['firstname'] = $data['firstname'];
                    $response['msg'] = 'bevestig uw e-mail adres. Zodra de BETA live is wordt u direct op de hoogte gesteld!';

                    // Sent email with confirmation
                }else{
                    $response['success'] = false;
                    $response['msg'] = $errorMsg;
                }

            }else{
                $response['success'] = false;
                $response['msg'] = $errorMsg;
            }

            return new JsonResponse( $response );
        }

        return array(
            'betaForm' => $betaForm->createView( )
        );
    }

    /**
     * This method registers an user in the database manually.
     *
     * @return boolean User registered / not registered
     **/
    private function register($request, $form, $email,$firstname,$lastname){
        $userManager = $this->get('fos_user.user_manager');

        // Check if the user exists to prevent Integrity constraint violation error in the insertion
        if($userManager->findUserByEmail($email)){
            return false;
        }

        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();

        $event = new FormEvent($form, $request);

        $user->addRole('ROLE_BETA');
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setUsername($email);
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        $user->setLocked(0);
        $user->setEnabled(0);
        $passwordManager = new ComputerPasswordGenerator();
        $user->setPlainPassword($passwordManager->generatePassword());

        try {
            $userManager->updateUser($user);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
        }catch (Exception $e){
            echo($e->getMessage());
        }

        return true;
    }

}