<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),

            new EWZ\Bundle\RecaptchaBundle\EWZRecaptchaBundle(), // reCAPTCHA
            new Hip\MandrillBundle\HipMandrillBundle(), // Mandril API
            new Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle(), // Bootstrap
            new Bmatzner\FontAwesomeBundle\BmatznerFontAwesomeBundle(), // Fontawesome
            new Knp\Bundle\MenuBundle\KnpMenuBundle(), // Menu's
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(), // Pagination for data collections
            new Mablae\DatetimepickerBundle\MablaeDatetimepickerBundle(), // jQuery datetime picker
            new Sfk\EmailTemplateBundle\SfkEmailTemplateBundle(), // Email templating

            new JMS\AopBundle\JMSAopBundle(), // Don't know
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(), // Don't know
            new JMS\DiExtraBundle\JMSDiExtraBundle($this), // Don't know
            new JMS\SerializerBundle\JMSSerializerBundle(),
//            new JMS\I18nRoutingBundle\JMSI18nRoutingBundle(),
//            new JMS\TranslationBundle\JMSTranslationBundle(),

            new FOS\JsRoutingBundle\FOSJsRoutingBundle(), // For javascript route calls with controllers in frontend
            new Endroid\Bundle\QrCodeBundle\EndroidQrCodeBundle(), // QRcode generation bundle
            new Vich\UploaderBundle\VichUploaderBundle(), // Image uploading
            new Hackzilla\Bundle\PasswordGeneratorBundle\HackzillaPasswordGeneratorBundle(), // Password generator
            new Simpleweb\SaaSBundle\SimplewebSaaSBundle(), // Subscriptions
            new Lexik\Bundle\CurrencyBundle\LexikCurrencyBundle(),

            // Web bundles
            new Web\PageBundle\PageBundle(),

            // App bundles
            new App\CoreBundle\CoreBundle(),
            new App\SubdomainBundle\SubdomainBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Shared\UserBundle\UserBundle(),
            new App\CompanyBundle\CompanyBundle(),
            new App\DashboardBundle\DashboardBundle(),
            new App\CategoryBundle\CategoryBundle(),
            new App\SettingsBundle\SettingsBundle(),
            new App\ProductBundle\ProductBundle(),
            new App\WarehouseBundle\WarehouseBundle(),
            new App\SupportBundle\SupportBundle(),
            new App\DatabaseBundle\DatabaseBundle(),
            new App\SharedBundle\SharedBundle(),
            new App\AdministrationBundle\AdministrationBundle(),
            new App\CatalogBundle\CatalogBundle(),
            new App\ProjectBundle\ProjectBundle(),
            new App\RelationBundle\RelationBundle(),
            new Shared\EmailBundle\EmailBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
            $bundles[] = new Elao\WebProfilerExtraBundle\WebProfilerExtraBundle(); // Extra debug info in DEV environment
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
