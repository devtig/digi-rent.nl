$(document).ready(function(e) {

    /* ======= Blog Featured Post Slideshow - Flexslider ======= */
    //$('.projects-slider').flexslider({
    //    animation: "fade",
    //    slideshowSpeed: 8000
    //});

    /* ======= Blog page masonry ======= */
    /* Ref: http://desandro.github.io/masonry/index.html */
    var $container = $('#projects-mansonry');
    $container.imagesLoaded(function(){
        $container.masonry({
            itemSelector : '.project'
        });
    });


    //$('.filter').each(function(){var $filter=$(this),$selected=$('.selected',this),filterText=$selected.text(),dontFilterText=$selected.attr('data-dont-select');$filter.on('change',function(){$selected.text(dontFilterText);if($selected.is(':selected')){$selected.text(filterText);}
    //    $('#filter-projects').submit();});});$('#filter-projects').on('submit',function(event){console.log("ERROR");event.preventDefault();var oldItems=$('.work-block',$container);$.ajax({type:'POST',data:$(this).serialize(),dataType:'html',success:function(data){console.log(data);var items=$('.work-block',data);$container.isotope('remove',oldItems);$container.isotope('insert',items);}});return false;});$('body').on('click',".btn-pagenation",function(event){event.preventDefault();var $btn=$(this),href=$btn.get(0).href;$.ajax({url:href,type:'GET',dataType:'html',success:function(data){var items=$('.work-block',data);$container.isotope('remove',$btn.parents('.btn-pagenation-container'));$container.isotope('insert',items);}});return;})
    //
    //if($('.submitOnLoad').length){}
    //
    //if($('.filter').length&&$('.filter').val()!=''){$('#filter-projects').submit();}

    var $cs=$('.filter').customSelect();


    var $grid = $('.project-list').isotope({
        // options
        itemSelector: '.project-item',
        layoutMode: 'fitRows'
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
        }
    };
    // bind filter on select change
    $('.filter').on( 'change', function() {
        // get filter value from option value
        var filterValue = this.value;
        // use filterFn if matches value
        filterValue = filterFns[ filterValue ] || filterValue;
        $grid.isotope({ filter: filterValue });
    });

});



